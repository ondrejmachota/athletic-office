﻿using System;
using System.Collections.Generic;
using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.Domain.Enums;
using AtletickaKancelar.WPF.Infrastructure.Score;
using AtletickaKancelar.WPF.Models;
using Xunit;

namespace AtletickaKancelar.Tests.WPF
{
    public class PripravkaScoreTester
    {
        private readonly ScoreCalc _scoreCalc;
        private readonly Dictionary<string, Discipline> _disciplinesDictionary;
        private readonly List<SpojkaMainModel> _competitors;


        public PripravkaScoreTester()
        {
            _scoreCalc = new ScoreCalc();
            _disciplinesDictionary = new Dictionary<string, Discipline>
            {
                {"50", new Discipline(1, "50 m", "Přípravka", DisciplineType.Sprint, PhaseType.None, TimeSpan.Zero)},
                {"400", new Discipline(1, "400 m", "Přípravka", DisciplineType.MidRun, PhaseType.None, TimeSpan.Zero)},
                {"LongJump", new Discipline(1, "Dálka", "Přípravka", DisciplineType.LongJump, PhaseType.None, TimeSpan.Zero)},
                {"Throw", new Discipline(1, "Hod", "Přípravka", DisciplineType.Throw, PhaseType.None, TimeSpan.Zero)},

            };
            _competitors = new List<SpojkaMainModel>
            {
                new SpojkaMainModel(new SpojkaModel(0, null, null, null, PenalizationType.None)),
                new SpojkaMainModel(new SpojkaModel(0, null, null, null, PenalizationType.None)),
                new SpojkaMainModel(new SpojkaModel(0, null, null, null, PenalizationType.None))
            };
        }

        [Fact]
        public void CalculatePripravkaRun50()
        {
            _competitors[0].Spojka.Result = 8.21f;
            _competitors[1].Spojka.Result = 9.09f;
            _competitors[2].Spojka.Result = 7.49f;  
            _scoreCalc.CalculatePripravka(_disciplinesDictionary["50"], _competitors);
            Assert.Equal(613, _competitors[0].Spojka.Points);
            Assert.Equal(423, _competitors[1].Spojka.Points);
            Assert.Equal(789, _competitors[2].Spojka.Points);
        }
        
        [Fact]
        public void CalculatePripravkaRun400()
        {
            _competitors[0].Spojka.Result = 77.63f;
            _competitors[1].Spojka.Result = 82.73f;
            _competitors[2].Spojka.Result = 71.9f;  
            _scoreCalc.CalculatePripravka(_disciplinesDictionary["400"], _competitors);
            Assert.Equal(579, _competitors[0].Spojka.Points);
            Assert.Equal(470, _competitors[1].Spojka.Points);
            Assert.Equal(711, _competitors[2].Spojka.Points);
        }
        
        [Fact]
        public void CalculatePripravkaTechLongJump()
        {
            _competitors[0].Spojka.Result = 408;
            _competitors[1].Spojka.Result = 323;
            _competitors[2].Spojka.Result = 467;  
            _scoreCalc.CalculatePripravka(_disciplinesDictionary["LongJump"], _competitors);
            Assert.Equal(687, _competitors[0].Spojka.Points);
            Assert.Equal(357, _competitors[1].Spojka.Points);
            Assert.Equal(948, _competitors[2].Spojka.Points);
        }
        
        [Fact]
        public void CalculatePripravkaTechThrow()
        {
            _competitors[0].Spojka.Result = 34.86f;
            _competitors[1].Spojka.Result = 20.34f;
            _competitors[2].Spojka.Result = 36.29f;  
            _scoreCalc.CalculatePripravka(_disciplinesDictionary["Throw"], _competitors);
            Assert.Equal(517, _competitors[0].Spojka.Points);
            Assert.Equal(248, _competitors[1].Spojka.Points);
            Assert.Equal(544, _competitors[2].Spojka.Points);
        }
    }
}