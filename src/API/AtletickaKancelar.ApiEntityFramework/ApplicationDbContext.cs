﻿using AtletickaKancelar.Domain.EFEntities.ApiEntities;
using AtletickaKancelar.Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace AtletickaKancelar.ApiEntityFramework
{
	public class ApplicationDbContext : DbContext
	{
		#region MyRegion

		public DbSet<ApiRace> Races { get; set; }

		public DbSet<ApiClubPoint> ClubPoints { get; set; }

		public DbSet<ApiAthlete> Athletes { get; set; }

		public DbSet<ApiClub> Clubs { get; set; }

		public DbSet<ApiDiscipline> Disciplines { get; set; }

		public DbSet<ApiCompetitor> Competitors { get; set; }

		#endregion DbSets

		public ApplicationDbContext(DbContextOptions options) : base(options)
		{
		}

		public ApplicationDbContext()
		{
		}

		/*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseMySql("server=127.0.0.1;user id=PC;password=1234;database=webathleticofficedb;persistsecurityinfo=True");
		}*/

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);
			builder.Entity<ApiRace>(entity =>
			{
				entity.Property(x => x.Title).IsRequired().HasMaxLength(125);
				entity.Property(x => x.Date).IsRequired();
				entity.Property(x => x.Place).IsRequired().HasMaxLength(125);
				entity.Property(x => x.Note).HasMaxLength(750).IsRequired(false);
			});
			EnumToNumberConverter<DisciplineType, int> disciplineTypeConverter = new EnumToNumberConverter<DisciplineType, int>();
			EnumToNumberConverter<PhaseType, int> phaseTypeConverter = new EnumToNumberConverter<PhaseType, int>();
			builder.Entity<ApiDiscipline>(entity =>
			{
				entity.HasOne(r => r.Race).WithMany(d => d.Disciplines).HasForeignKey(k => k.RaceId);
				entity.Property(x => x.MainId).IsRequired(false);
				entity.HasMany(x => x.ChildDisciplies).WithOne(x => x.MainDiscipline).HasForeignKey(k => k.MainId).OnDelete(DeleteBehavior.SetNull);
				entity.Property(x => x.Name).HasMaxLength(50).IsRequired();
				entity.Property(x => x.Category).HasMaxLength(50).IsRequired();
				entity.Property(x => x.DisciplineType).HasConversion(disciplineTypeConverter).IsRequired();
				entity.Property(x => x.PhaseType).HasConversion(phaseTypeConverter).IsRequired().HasDefaultValue(PhaseType.None);
				entity.Property(x => x.Start).IsRequired();
				entity.Property(x => x.CameraId).IsRequired(false).HasDefaultValue();
				entity.Property(x => x.Order).IsRequired(false).HasDefaultValue();
				entity.Property(x => x.IsRegistrationsPrinted).HasDefaultValue(false);
				entity.Property(x => x.IsResultsPrinted).HasDefaultValue(false);
			});
			builder.Entity<ApiClub>(entity =>
			{
				entity.Property(x => x.Name).HasMaxLength(100).IsRequired();
				entity.Property(x => x.Abbreviation).HasMaxLength(10).IsRequired();
				entity.Property(x => x.Password).HasMaxLength(300).IsRequired();
				entity.Property(x => x.Email).HasMaxLength(125).IsRequired();
			});
			builder.Entity<ApiAthlete>(entity =>
			{
				entity.HasOne(c => c.Club).WithMany(c => c.Athletes).HasForeignKey(k => k.ClubId);
				entity.Property(x => x.FirstName).HasMaxLength(40).IsRequired();
				entity.Property(x => x.LastName).HasMaxLength(60).IsRequired();
				entity.Property(x => x.BirthDate).IsRequired();
				entity.Property(x => x.Email).HasMaxLength(125).IsRequired();
				entity.Property(x => x.Password).HasMaxLength(300);
			});
			EnumToNumberConverter<PenalizationType, int> penalizationTypeConverter = new EnumToNumberConverter<PenalizationType, int>();
			builder.Entity<ApiCompetitor>(entity =>
			{
				entity.HasOne(c => c.Athlete).WithMany(c => c.Competitors).HasForeignKey(k => k.AthleteId);
				entity.HasOne(c => c.Discipline).WithMany(c => c.Competitors).HasForeignKey(k => k.DisciplineId);
				entity.Property(x => x.Track).IsRequired(false).HasDefaultValue();
				entity.Property(x => x.Bib).IsRequired(false).HasDefaultValue();
				entity.Property(x => x.Result).IsRequired(false).HasDefaultValue();
				entity.Property(x => x.Order).IsRequired(false).HasDefaultValue();
				entity.Property(x => x.SecondOrder).IsRequired(false).HasDefaultValue(0);
				entity.Property(x => x.Points).IsRequired(false).HasDefaultValue();
				entity.Property(x => x.PenalizationType).HasConversion(penalizationTypeConverter).IsRequired().HasDefaultValue(PenalizationType.None);
			});
			builder.Entity<ApiClubPoint>(entity =>
			{
				entity.HasOne(x => x.Club).WithMany(x => x.ClubPoints).HasForeignKey(k => k.ClubId);
				entity.HasOne(x => x.Race).WithMany(x => x.ClubPoints).HasForeignKey(k => k.RaceId);
			});
		}
	}
}
