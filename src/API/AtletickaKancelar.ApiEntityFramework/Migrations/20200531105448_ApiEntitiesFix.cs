﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AtletickaKancelar.ApiEntityFramework.Migrations
{
    public partial class ApiEntitiesFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClubPoints_Race_RaceId1",
                table: "ClubPoints");

            migrationBuilder.DropForeignKey(
                name: "FK_Disciplines_Races_ApiRaceId",
                table: "Disciplines");

            migrationBuilder.DropForeignKey(
                name: "FK_Disciplines_Race_RaceId",
                table: "Disciplines");

            migrationBuilder.DropTable(
                name: "Race");

            migrationBuilder.DropIndex(
                name: "IX_Disciplines_ApiRaceId",
                table: "Disciplines");

            migrationBuilder.DropIndex(
                name: "IX_ClubPoints_RaceId1",
                table: "ClubPoints");

            migrationBuilder.DropColumn(
                name: "ApiRaceId",
                table: "Disciplines");

            migrationBuilder.DropColumn(
                name: "RaceId1",
                table: "ClubPoints");

            migrationBuilder.AddForeignKey(
                name: "FK_Disciplines_Races_RaceId",
                table: "Disciplines",
                column: "RaceId",
                principalTable: "Races",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Disciplines_Races_RaceId",
                table: "Disciplines");

            migrationBuilder.AddColumn<int>(
                name: "ApiRaceId",
                table: "Disciplines",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RaceId1",
                table: "ClubPoints",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Race",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    Note = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Place = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Title = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Race", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Disciplines_ApiRaceId",
                table: "Disciplines",
                column: "ApiRaceId");

            migrationBuilder.CreateIndex(
                name: "IX_ClubPoints_RaceId1",
                table: "ClubPoints",
                column: "RaceId1");

            migrationBuilder.AddForeignKey(
                name: "FK_ClubPoints_Race_RaceId1",
                table: "ClubPoints",
                column: "RaceId1",
                principalTable: "Race",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Disciplines_Races_ApiRaceId",
                table: "Disciplines",
                column: "ApiRaceId",
                principalTable: "Races",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Disciplines_Race_RaceId",
                table: "Disciplines",
                column: "RaceId",
                principalTable: "Race",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
