﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AtletickaKancelar.ApiEntityFramework.Migrations
{
    public partial class ApiClubFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Athletes_Clubs_ApiClubId",
                table: "Athletes");

            migrationBuilder.DropForeignKey(
                name: "FK_Athletes_Club_ClubId",
                table: "Athletes");

            migrationBuilder.DropForeignKey(
                name: "FK_ClubPoints_Club_ClubId",
                table: "ClubPoints");

            migrationBuilder.DropTable(
                name: "Club");

            migrationBuilder.DropIndex(
                name: "IX_Athletes_ApiClubId",
                table: "Athletes");

            migrationBuilder.DropColumn(
                name: "ApiClubId",
                table: "Athletes");

            migrationBuilder.AlterColumn<string>(
                name: "Password",
                table: "Clubs",
                maxLength: 400,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Clubs",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Abbreviation",
                table: "Clubs",
                maxLength: 10,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Athletes_Clubs_ClubId",
                table: "Athletes",
                column: "ClubId",
                principalTable: "Clubs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClubPoints_Clubs_ClubId",
                table: "ClubPoints",
                column: "ClubId",
                principalTable: "Clubs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Athletes_Clubs_ClubId",
                table: "Athletes");

            migrationBuilder.DropForeignKey(
                name: "FK_ClubPoints_Clubs_ClubId",
                table: "ClubPoints");

            migrationBuilder.AlterColumn<string>(
                name: "Password",
                table: "Clubs",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 400);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Clubs",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<string>(
                name: "Abbreviation",
                table: "Clubs",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 10);

            migrationBuilder.AddColumn<int>(
                name: "ApiClubId",
                table: "Athletes",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Club",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Abbreviation = table.Column<string>(type: "varchar(10) CHARACTER SET utf8mb4", maxLength: 10, nullable: false),
                    Name = table.Column<string>(type: "varchar(100) CHARACTER SET utf8mb4", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Club", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Athletes_ApiClubId",
                table: "Athletes",
                column: "ApiClubId");

            migrationBuilder.AddForeignKey(
                name: "FK_Athletes_Clubs_ApiClubId",
                table: "Athletes",
                column: "ApiClubId",
                principalTable: "Clubs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Athletes_Club_ClubId",
                table: "Athletes",
                column: "ClubId",
                principalTable: "Club",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClubPoints_Club_ClubId",
                table: "ClubPoints",
                column: "ClubId",
                principalTable: "Club",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
