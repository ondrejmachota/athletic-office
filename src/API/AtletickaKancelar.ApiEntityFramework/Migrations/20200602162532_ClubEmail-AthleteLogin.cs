﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AtletickaKancelar.ApiEntityFramework.Migrations
{
    public partial class ClubEmailAthleteLogin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Password",
                table: "Clubs",
                maxLength: 125,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(400) CHARACTER SET utf8mb4",
                oldMaxLength: 400);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Clubs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Athletes",
                maxLength: 125,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "Athletes",
                maxLength: 300,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "Clubs");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Athletes");

            migrationBuilder.DropColumn(
                name: "Password",
                table: "Athletes");

            migrationBuilder.AlterColumn<string>(
                name: "Password",
                table: "Clubs",
                type: "varchar(400) CHARACTER SET utf8mb4",
                maxLength: 400,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 125);
        }
    }
}
