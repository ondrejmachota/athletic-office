﻿using System;
using System.Security.Cryptography;
using System.Text;
using AkAPI.Services.IServices;

namespace AkAPI.Services
{
	public class SecurityService : ISecurityService
	{
		public string GetSha256Hash(string input)
		{
			using (var hashAlgorithm = SHA256.Create())
			{
				var byteValue = Encoding.UTF8.GetBytes(input);
				var byteHash = hashAlgorithm.ComputeHash(byteValue);
				return Convert.ToBase64String(byteHash);
			}
		}
	}
}
