﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AkAPI.Services.IServices;
using AtletickaKancelar.ApiEntityFramework;
using AtletickaKancelar.Domain.EFEntities.ApiEntities;
using Microsoft.EntityFrameworkCore;

namespace AkAPI.Services
{
	public class DisciplineService : IDisciplineService
	{
		private readonly ApplicationDbContext _context;

		public DisciplineService(ApplicationDbContext context)
		{
			_context = context;
		}

		public async Task<IEnumerable<ApiDiscipline>> GetDisciplinesByRaceId(int raceId)
		{
			return await _context.Disciplines.Where(x => x.RaceId == raceId).ToListAsync();
		}
	}
}
