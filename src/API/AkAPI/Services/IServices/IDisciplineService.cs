﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AtletickaKancelar.Domain.EFEntities.ApiEntities;

namespace AkAPI.Services.IServices
{
	public interface IDisciplineService
	{
		Task<IEnumerable<ApiDiscipline>> GetDisciplinesByRaceId(int raceId);
	}
}
