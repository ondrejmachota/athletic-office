﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AtletickaKancelar.Domain.EFEntities.ApiEntities;

namespace AkAPI.Services.IServices
{
	public interface IRaceService
	{
		Task<IEnumerable<ApiRace>> GetRacesForLogin();
		Task<IEnumerable<ApiRace>> GetRacesWithResults();
	}
}
