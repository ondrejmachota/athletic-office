﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AkAPI.Services.IServices
{
	public interface IGenericDataService<T>
	{
		Task<IEnumerable<T>> GetAll();
		Task<T?> Get(int id);
		Task<T> Create(T entity);
		Task<T> Update(int id, T entity);
		Task<bool> Delete(int id);
	}
}
