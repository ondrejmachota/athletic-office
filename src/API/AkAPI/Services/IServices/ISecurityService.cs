﻿namespace AkAPI.Services.IServices
{
	public interface ISecurityService
	{
		string GetSha256Hash(string input);
	}
}
