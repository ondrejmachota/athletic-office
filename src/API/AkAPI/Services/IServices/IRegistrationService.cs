﻿using System.Threading.Tasks;
using AtletickaKancelar.Domain.ApiReqResp;
using AtletickaKancelar.Domain.EFEntities.ApiEntities;

namespace AkAPI.Services.IServices
{
	public interface IRegistrationService
	{
		Task<ApiAthlete> Register(RegistrationModel registrationModel);
	}
}
