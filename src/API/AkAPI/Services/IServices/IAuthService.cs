﻿using System.Threading.Tasks;
using AtletickaKancelar.Domain.ApiReqResp;

namespace AkAPI.Services.IServices
{
	public interface IAuthService
	{
		Task<AuthClubResponse?> AuthenticateClub(string clubName, string password);
		Task<AuthAthleteResponse?> AuthenticateAthlete(string email, string password);
	}
}
