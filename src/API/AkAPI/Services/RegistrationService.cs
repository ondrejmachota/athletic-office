﻿using System;
using System.Threading.Tasks;
using AkAPI.Services.IServices;
using AtletickaKancelar.Domain.ApiReqResp;
using AtletickaKancelar.Domain.EFEntities.ApiEntities;
using AtletickaKancelar.Domain.Enums;

namespace AkAPI.Services
{
	public class RegistrationService : IRegistrationService
	{
		private readonly IGenericDataService<ApiAthlete> _genericDataService;
		private readonly ISecurityService _securityService;


		public RegistrationService(IGenericDataService<ApiAthlete> genericDataService, ISecurityService securityService)
		{
			_genericDataService = genericDataService;
			_securityService = securityService;
		}

		public async Task<ApiAthlete> Register(RegistrationModel registrationModel)
		{
			var athlete = new ApiAthlete
			{
				FirstName = registrationModel.FirstName,
				LastName = registrationModel.LastName,
				BirthDate = DateTime.Parse(registrationModel.BirthDate),
				ClubId = registrationModel.ClubId,
				Email = registrationModel.Email,
				Password = _securityService.GetSha256Hash(registrationModel.Password),
				Sex = registrationModel.Sex ? Sex.Woman : Sex.Man
			};

			return await _genericDataService.Create(athlete);
		}
	}
}
