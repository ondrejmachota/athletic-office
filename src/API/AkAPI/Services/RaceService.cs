﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AkAPI.Services.IServices;
using AtletickaKancelar.ApiEntityFramework;
using AtletickaKancelar.Domain.EFEntities.ApiEntities;
using Microsoft.EntityFrameworkCore;

namespace AkAPI.Services
{
	public class RaceService : IRaceService
	{
		private readonly ApplicationDbContext _context;

		public RaceService(ApplicationDbContext context)
		{
			_context = context;
		}

		public async Task<IEnumerable<ApiRace>> GetRacesForLogin()
		{
			var races =  await _context.Races.Where(x => x.Date > DateTime.Today).ToListAsync().ConfigureAwait(false);

			for (int i = 0; i < races.Count; i++)
			{
				races[i].CanUserLoginToRace = true;
			}

			return races;
		}

		public async Task<IEnumerable<ApiRace>> GetRacesWithResults()
		{
			var races = await _context.Races.Where(x => x.Date <= DateTime.Today).ToListAsync();

			for (int i = 0; i < races.Count; i++)
			{
				races[i].CanUserLoginToRace = false;
			}

			return races;
		}
	}
}
