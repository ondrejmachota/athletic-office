﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AkAPI.Services.IServices;
using AtletickaKancelar.ApiEntityFramework;
using AtletickaKancelar.Domain.EFEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace AkAPI.Services
{
	public class GenericDataService<T> : IGenericDataService<T> where T : IdObject
	{
		private readonly ApplicationDbContext _context;

		public GenericDataService(ApplicationDbContext context)
		{
			_context = context;
		}

		public async Task<IEnumerable<T>> GetAll()
		{
			return await _context.Set<T>().ToListAsync().ConfigureAwait(false);
		}

		public async Task<T?> Get(int id)
		{
			return await _context.Set<T>().FirstOrDefaultAsync(e => e.Id == id).ConfigureAwait(false);
		}

		public async Task<T> Create(T entity)
		{
			EntityEntry<T> createdEntity = await _context.Set<T>().AddAsync(entity).ConfigureAwait(false);
			await _context.SaveChangesAsync().ConfigureAwait(false);

			return createdEntity.Entity;
		}

		public async Task<T> Update(int id, T entity)
		{
			entity.Id = id;
			_context.Set<T>().Update(entity);
			await _context.SaveChangesAsync().ConfigureAwait(false);

			return entity;
		}

		public async Task<bool> Delete(int id)
		{
			T entity = await _context.Set<T>().FirstOrDefaultAsync(e => e.Id == id).ConfigureAwait(false);
			_context.Set<T>().Remove(entity);
			await _context.SaveChangesAsync().ConfigureAwait(false);

			return true;
		}
	}
}
