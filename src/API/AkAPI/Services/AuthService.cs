﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AkAPI.Services.IServices;
using AtletickaKancelar.ApiEntityFramework;
using AtletickaKancelar.Domain.ApiReqResp;
using AtletickaKancelar.Domain.EFEntities.ApiEntities;
using AtletickaKancelar.Domain.Extensions;
using AtletickaKancelar.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace AkAPI.Services
{
	public class AuthService : IAuthService
	{
		private readonly ApplicationDbContext _context;
		private readonly ISecurityService _securityService;
		private readonly ApplicationSettings _appSettings;

		public AuthService(ApplicationDbContext context, ISecurityService securityService, IOptions<ApplicationSettings> appSettings)
		{
			_context = context;
			_securityService = securityService;
			_appSettings = appSettings.Value;
		}

		public async Task<AuthClubResponse?> AuthenticateClub(string clubName, string password)
		{
			if (string.IsNullOrEmpty(clubName))
				return null;
			if (string.IsNullOrEmpty(password))
				return null;

			var club = await _context.Clubs.SingleOrDefaultAsync(x => x.Name == clubName);

			if (club == null)
				return null;

			var passwordHash = _securityService.GetSha256Hash(password);

			if (passwordHash != club.Password)
				return null;

			return CreateToken(club);
		}

		private AuthClubResponse CreateToken(ApiClub club)
		{
			var tokenHandler = new JwtSecurityTokenHandler();
			if (_appSettings.JwtSecret != null)
			{
				var key = Encoding.ASCII.GetBytes(_appSettings.JwtSecret);
				var tokenDescriptor = new SecurityTokenDescriptor
				{
					Subject = new ClaimsIdentity(new[]
					{
						new Claim("Club_ID", club.Id.ToString()),
						new Claim(ClaimTypes.Role, Role.Club)
					}),
					Expires = DateTime.UtcNow.AddDays(1),
					SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
				};
				var token = tokenHandler.CreateToken(tokenDescriptor);
				return new AuthClubResponse(club, tokenHandler.WriteToken(token));
			}

			return null!;
		}

		public async Task<AuthAthleteResponse?> AuthenticateAthlete(string email, string password)
		{
			if (string.IsNullOrEmpty(email))
				return null;
			if (string.IsNullOrEmpty(password))
				return null;

			var athlete = await _context.Athletes.SingleOrDefaultAsync(x => x.Email == email);

			if (athlete == null)
				return null;

			var passwordHash = _securityService.GetSha256Hash(password);

			if (passwordHash != athlete.Password)
				return null;

			return CreateToken(athlete);
		}

		private AuthAthleteResponse CreateToken(ApiAthlete athlete)
		{
			var tokenHandler = new JwtSecurityTokenHandler();
			if (_appSettings.JwtSecret != null)
			{
				var key = Encoding.ASCII.GetBytes(_appSettings.JwtSecret);
				var tokenDescriptor = new SecurityTokenDescriptor
				{
					Subject = new ClaimsIdentity(new[]
					{
						new Claim("Athlete_ID", athlete.Id.ToString()),
						new Claim(ClaimTypes.Role, Role.Athlete)
					}),
					Expires = DateTime.UtcNow.AddDays(1),
					SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
				};
				var token = tokenHandler.CreateToken(tokenDescriptor);
				return new AuthAthleteResponse(athlete, tokenHandler.WriteToken(token));
			}

			return null!;
		}
	}
}
