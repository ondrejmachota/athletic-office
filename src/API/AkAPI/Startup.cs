using System;
using System.Text;
using AkAPI.Services;
using AkAPI.Services.IServices;
using AtletickaKancelar.ApiEntityFramework;
using AtletickaKancelar.Domain.EFEntities.ApiEntities;
using AtletickaKancelar.Domain.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;

namespace AkAPI
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));

			services.AddDbContext<ApplicationDbContext>(option =>
			{
				option.UseMySql(Configuration.GetConnectionString("DefaultConnection"));
			});

			services.AddScoped<ISecurityService, SecurityService>();
			services.AddScoped<IGenericDataService<ApiRace>, GenericDataService<ApiRace>>();
			services.AddScoped<IGenericDataService<ApiClub>, GenericDataService<ApiClub>>();
			services.AddScoped<IGenericDataService<ApiAthlete>, GenericDataService<ApiAthlete>>();
			services.AddScoped<IRaceService, RaceService>();
			services.AddScoped<IDisciplineService, DisciplineService>();
			services.AddScoped<IAuthService, AuthService>();
			services.AddScoped<IClubService, ClubService>();
			services.AddScoped<IRegistrationService, RegistrationService>();

			services.AddCors(options =>
			{
				options.AddPolicy("CorsPolicy",
					builder => builder
					           .WithOrigins("https://localhost:5001")
					           .AllowAnyOrigin()
					           .AllowAnyMethod()
					           .AllowAnyHeader());
			});

			services.Configure<IdentityOptions>(options =>
			{
				options.Password.RequireDigit = false;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequireLowercase = false;
				options.Password.RequireUppercase = false;
				options.Password.RequiredLength = 4;
			});

			//Jwt Authentication
			var key = Encoding.UTF8.GetBytes(Configuration["ApplicationSettings:JwtSecret"]);

			services.AddAuthentication(x =>
			{
				x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
				x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
			}).AddJwtBearer(x =>
			{
				x.RequireHttpsMetadata = false;
				x.SaveToken = false;
				x.TokenValidationParameters = new TokenValidationParameters
				{
					ValidateIssuerSigningKey = true,
					IssuerSigningKey = new SymmetricSecurityKey(key),
					ValidateIssuer = false,
					ValidateAudience = false,
					ClockSkew = TimeSpan.Zero
				};
			});

			services.AddControllers().AddNewtonsoftJson(options =>
				options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
			);
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseCors("CorsPolicy");

			app.UseAuthentication();

			app.UseAuthorization();

			app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
		}
	}
}
