﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtletickaKancelar.Domain.EFEntities.ApiEntities;

namespace AkAPI.Utilities
{
	public static class Extensions
	{
		public static IEnumerable<ApiClub> WithoutPasswords(this IEnumerable<ApiClub> clubs)
		{
			return clubs.Select(WithoutPassword);
		}

		public static ApiClub WithoutPassword(this ApiClub club)
		{
			club.Password = null;
			return club;
		}


		public static IEnumerable<ApiRace> ValidateRaces(this IEnumerable<ApiRace> races)
		{
			return races.Select(CheckRaceUserCanLogin);
		}

		public static ApiRace CheckRaceUserCanLogin(this ApiRace? race)
		{
			if (race != null)
			{
				race.CanUserLoginToRace ??= race.Date > DateTime.UtcNow;
			}

			return race!;
		}
	}
}
