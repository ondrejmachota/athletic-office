﻿using System.Threading.Tasks;
using AkAPI.Services.IServices;
using AtletickaKancelar.Domain.ApiReqResp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AkAPI.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class RegistrationController : ControllerBase
	{
		private readonly IRegistrationService _registrationService;

		public RegistrationController(IRegistrationService registrationService)
		{
			_registrationService = registrationService;
		}

		// GET: Registration/
		[AllowAnonymous]
		[HttpPost("")]
		public async Task<IActionResult> Register([FromBody] RegistrationModel registrationModel)
		{
			var athlete = await _registrationService.Register(registrationModel);

			athlete.Password = "";

			return Ok(athlete);
		}
	}
}
