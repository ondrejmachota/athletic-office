﻿using System.Threading.Tasks;
using AkAPI.Services.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AkAPI.Controllers
{
	[Authorize]
	[ApiController]
	[Route("[controller]")]
	public class DisciplineController : ControllerBase
	{
		private readonly IDisciplineService _disciplineService;

		public DisciplineController(IDisciplineService disciplineService)
		{
			_disciplineService = disciplineService;
		}

		// GET: Discipline/1
		[HttpGet("{raceId}")]
		public async Task<IActionResult> GetDisciplines([FromRoute]int raceId)
		{
			return Ok(await _disciplineService.GetDisciplinesByRaceId(raceId));
		}
	}
}
