﻿using System.Threading.Tasks;
using AkAPI.Services.IServices;
using AtletickaKancelar.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AkAPI.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class AuthController : ControllerBase
	{
		private readonly IAuthService _authService;

		public AuthController(IAuthService authService)
		{
			_authService = authService;
		}

		[AllowAnonymous]
		[HttpPost("[action]")]
		public async Task<IActionResult> ClubLogin([FromBody]AuthModel model)
		{
			var club = await _authService.AuthenticateClub(model.UserName, model.Password);

			if (club == null)
			{
				return Unauthorized(new { message = "Bad username or password" });
			}

			return Ok(club);
		}

		[AllowAnonymous]
		[HttpPost("[action]")]
		public async Task<IActionResult> AthleteLogin([FromBody]AuthModel model)
		{
			var athlete = await _authService.AuthenticateAthlete(model.UserName, model.Password);

			if (athlete == null)
			{
				return Unauthorized(new { message = "Bad username or password" });
			}

			return Ok(athlete);
		}
	}
}
