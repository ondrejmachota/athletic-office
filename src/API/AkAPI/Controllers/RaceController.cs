﻿using System.Threading.Tasks;
using AkAPI.Services.IServices;
using AtletickaKancelar.Domain.EFEntities.ApiEntities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AkAPI.Controllers
{
	[Authorize]
	[ApiController]
	[Route("[controller]")]
	public class RaceController : ControllerBase
	{
		private readonly IGenericDataService<ApiRace> _genericDataService;
		private readonly IRaceService _raceService;

		public RaceController(IGenericDataService<ApiRace> genericDataService, IRaceService raceService)
		{
			_genericDataService = genericDataService;
			_raceService = raceService;
		}

		// GET: Race/
		[HttpGet("")]
		public async Task<IActionResult> GetRaces()
		{
			return Ok(await _genericDataService.GetAll());
		}

		// GET : Race/5
		[HttpGet("{raceId}")]
		public async Task<IActionResult> GetRaceById([FromRoute]int raceId)
		{
			var race = await _genericDataService.Get(raceId);

			if (race == null)
			{
				return NotFound(new { message = "Závod nebyl nalezen" });
			}

			return Ok(race);
		}

		// GET: Race/ForLogin
		[HttpGet("ForLogin")]
		public async Task<IActionResult> GetRacesForLogin()
		{
			return Ok(await _raceService.GetRacesForLogin());
		}

		// GET: Race/WithResults
		[HttpGet("WithResults")]
		public async Task<IActionResult> GetRacesWithResults()
		{
			return Ok(await _raceService.GetRacesWithResults());
		}
	}
}
