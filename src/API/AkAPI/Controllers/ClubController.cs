﻿using System.Linq;
using System.Threading.Tasks;
using AkAPI.Services.IServices;
using AtletickaKancelar.Domain.ApiReqResp;
using AtletickaKancelar.Domain.EFEntities.ApiEntities;
using AtletickaKancelar.Domain.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AkAPI.Controllers
{
	[Authorize]
	[ApiController]
	[Route("[controller]")]
	public class ClubController : ControllerBase
	{
		private readonly IGenericDataService<ApiClub> _genericDataService;

		public ClubController(IGenericDataService<ApiClub> genericDataService)
		{
			_genericDataService = genericDataService;
		}

		[Authorize(Roles = Role.Club)]
		[HttpGet("")]
		public async Task<IActionResult> GetClubs()
		{
			var clubs = await _genericDataService.GetAll();

			return Ok(clubs.Select(x => x.Password = null));
		}

		[AllowAnonymous]
		[HttpGet("ForRegistration")]
		public async Task<IActionResult> GetClubsForRegistration()
		{
			var clubs = await _genericDataService.GetAll().ConfigureAwait(false);

			return Ok(clubs.Select(x => new RegistrationClub(x)));
		}
	}
}
