﻿using System;
using System.Collections.Generic;
using AtletickaKancelar.Domain.EFEntities.LocalEntities;
using AtletickaKancelar.Domain.Enums;
using AtletickaKancelar.Domain.Interfaces;

namespace AtletickaKancelar.Domain.EFEntities.Entities
{
	public class Athlete : IdObject, IAthlete
	{
		public int ClubId { get; set; }

		public Club Club { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public Sex Sex { get; set; }

		public DateTime BirthDate { get; set; }

		public ICollection<Competitor> Competitors { get; set; }
	}
}
