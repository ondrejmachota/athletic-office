﻿using AtletickaKancelar.Domain.Enums;
using AtletickaKancelar.Domain.Interfaces;

namespace AtletickaKancelar.Domain.EFEntities.Entities
{
	public class Competitor : IdObject, ICompetitor
	{
		public int AthleteId { get; set; }

		public virtual Athlete Athlete { get; set; }

		public int DisciplineId { get; set; }

		public virtual Discipline Discipline { get; set; }

		public int? Track { get; set; }

		public int? Bib { get; set; }

		public float? Result { get; set; }

		public int? Order { get; set; }

		public int? SecondOrder { get; set; }

		public int? Points { get; set; }

		public PenalizationType PenalizationType { get; set; }
	}
}
