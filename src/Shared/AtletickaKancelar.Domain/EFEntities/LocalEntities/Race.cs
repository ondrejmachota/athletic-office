﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using AtletickaKancelar.Domain.EFEntities.Entities;
using AtletickaKancelar.Domain.Interfaces;

namespace AtletickaKancelar.Domain.EFEntities.LocalEntities
{
	public class Race : IdObject, IRace
	{
		public virtual ICollection<Discipline> Disciplines { get; set; }

		public virtual ICollection<ClubPoint> ClubPoints { get; set; }

		public string Title { get; set; }

		[NotMapped] public bool IsActive { get; set; }

		public string Place { get; set; }

		public DateTime Date { get; set; }

		public string Note { get; set; }
	}
}
