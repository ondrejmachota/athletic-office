﻿using System;
using System.Collections.Generic;
using AtletickaKancelar.Domain.EFEntities.LocalEntities;
using AtletickaKancelar.Domain.Enums;
using AtletickaKancelar.Domain.Interfaces;

namespace AtletickaKancelar.Domain.EFEntities.Entities
{
	public class Discipline : IdObject, IDiscipline
	{
		public int RaceId { get; set; }

		public Race Race { get; set; }

		public string Name { get; set; }

		public string Category { get; set; }

		public DisciplineType DisciplineType { get; set; }

		public PhaseType PhaseType { get; set; }

		public int? MainId { get; set; }

		public virtual Discipline MainDiscipline { get; set; }

		public virtual ICollection<Discipline> ChildDisciplies { get; set; }

		public virtual ICollection<Competitor> Competitors { get; set; }

		public TimeSpan Start { get; set; }

		public int? CameraId { get; set; }

		public int? Order { get; set; }

		public bool IsRegistrationsPrinted { get; set; }

		public bool IsResultsPrinted { get; set; }
	}
}
