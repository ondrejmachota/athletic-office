﻿using System.Collections.Generic;
using AtletickaKancelar.Domain.EFEntities.Entities;
using AtletickaKancelar.Domain.Interfaces;

namespace AtletickaKancelar.Domain.EFEntities.LocalEntities
{
	public class Club : IdObject, IClub
	{
		public string Name { get; set; }

		public string Abbreviation { get; set; }

		public ICollection<Athlete> Athletes { get; set; }

		public ICollection<ClubPoint> ClubPoints { get; set; }

		public override string ToString()
		{
			return Name;
		}
	}
}
