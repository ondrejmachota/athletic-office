﻿using AtletickaKancelar.Domain.Interfaces;

namespace AtletickaKancelar.Domain.EFEntities.LocalEntities
{
	public class ClubPoint : IdObject, IClubPoint
	{
		public int ClubId { get; set; }

		public Club Club { get; set; }

		public int RaceId { get; set; }

		public Race Race { get; set; }

		public string ClubName { get; set; }

		public int Order { get; set; }

		public float? Points { get; set; }
	}
}
