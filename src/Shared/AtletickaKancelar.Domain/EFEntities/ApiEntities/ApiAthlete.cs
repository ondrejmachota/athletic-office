﻿using AtletickaKancelar.Domain.Enums;
using AtletickaKancelar.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace AtletickaKancelar.Domain.EFEntities.ApiEntities
{
	public class ApiAthlete : IdObject, IAthlete
	{
		public int ClubId { get; set; }

		public ApiClub Club { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public Sex Sex { get; set; }

		public DateTime BirthDate { get; set; }

		public string Email { get; set; }
		
		public string Password { get; set; }

		public ICollection<ApiCompetitor> Competitors { get; set; }
	}
}
