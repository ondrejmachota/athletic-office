﻿using AtletickaKancelar.Domain.Enums;
using AtletickaKancelar.Domain.Interfaces;

namespace AtletickaKancelar.Domain.EFEntities.ApiEntities
{
    public class ApiCompetitor : IdObject, ICompetitor
	{
		public int AthleteId { get; set; }

		public virtual ApiAthlete Athlete { get; set; }

		public int DisciplineId { get; set; }

		public virtual ApiDiscipline Discipline { get; set; }

		public int? Track { get; set; }

		public int? Bib { get; set; }

		public float? Result { get; set; }

		public int? Order { get; set; }

		public int? SecondOrder { get; set; }

		public int? Points { get; set; }

		public PenalizationType PenalizationType { get; set; }
	}
}
