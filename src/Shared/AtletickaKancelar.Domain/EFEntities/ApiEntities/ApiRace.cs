﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using AtletickaKancelar.Domain.Interfaces;

namespace AtletickaKancelar.Domain.EFEntities.ApiEntities
{
    public class ApiRace : IdObject, IRace
    {
        public virtual ICollection<ApiDiscipline> Disciplines { get; set; }

        public virtual ICollection<ApiClubPoint> ClubPoints { get; set; }
        public string Title { get; set; }
        public string Place { get; set; }
        public DateTime Date { get; set; }
        public string Note { get; set; }
        [NotMapped] public bool? CanUserLoginToRace { get; set; }
    }
}
