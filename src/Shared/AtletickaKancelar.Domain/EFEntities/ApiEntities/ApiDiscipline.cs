﻿using AtletickaKancelar.Domain.Enums;
using AtletickaKancelar.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace AtletickaKancelar.Domain.EFEntities.ApiEntities
{
    public class ApiDiscipline : IdObject, IDiscipline
    {
		public int RaceId { get; set; }

		public ApiRace Race { get; set; }

		public string Name { get; set; }

		public string Category { get; set; }

		public DisciplineType DisciplineType { get; set; }

		public PhaseType PhaseType { get; set; }

		public int? MainId { get; set; }

		public virtual ApiDiscipline MainDiscipline { get; set; }

		public virtual ICollection<ApiDiscipline> ChildDisciplies { get; set; }

		public virtual ICollection<ApiCompetitor> Competitors { get; set; }

		public TimeSpan Start { get; set; }

		public int? CameraId { get; set; }

		public int? Order { get; set; }

		public bool IsRegistrationsPrinted { get; set; }

		public bool IsResultsPrinted { get; set; }
	}
}
