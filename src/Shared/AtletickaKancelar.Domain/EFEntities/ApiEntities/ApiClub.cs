﻿using System.Collections.Generic;
using AtletickaKancelar.Domain.Interfaces;

namespace AtletickaKancelar.Domain.EFEntities.ApiEntities
{
	public class ApiClub : IdObject, IClub
	{
		public string Name { get; set; }

		public string Abbreviation { get; set; }

		public string Password { get; set; }

		public string Email { get; set; }

		public ICollection<ApiAthlete> Athletes { get; set; }

		public ICollection<ApiClubPoint> ClubPoints { get; set; }
	}
}
