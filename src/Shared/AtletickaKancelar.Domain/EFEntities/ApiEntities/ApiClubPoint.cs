﻿using System.ComponentModel.DataAnnotations.Schema;
using AtletickaKancelar.Domain.Interfaces;

namespace AtletickaKancelar.Domain.EFEntities.ApiEntities
{
	public class ApiClubPoint : IdObject, IClubPoint
	{
		public int ClubId { get; set; }

		public ApiClub Club { get; set; }

		public int RaceId { get; set; }

		public ApiRace Race { get; set; }

		[NotMapped] public string ClubName { get; set; }

		public int Order { get; set; }

		public float? Points { get; set; }
	}
}
