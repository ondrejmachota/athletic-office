﻿namespace AtletickaKancelar.Domain.Models
{
    public class CameraResult
    {
        private int _place;
        public readonly int Lane;
        public readonly float Time;

        public CameraResult(int place, int lane, float time)
        {
            _place = place;
            Lane = lane;
            Time = time;
        }
    }
}
