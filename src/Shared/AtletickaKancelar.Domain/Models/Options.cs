﻿
namespace AtletickaKancelar.Domain.Models
{
	public class Options
	{
		public int Rovinka { get; set; }

		public int Oval { get; set; }

		// public ScoreType ScoreType { get; set; }

		public string IpAddress { get; set; }
		
		public bool IsDark { get; set; }

		public string GrpcServer { get; set; }

		public Options()
		{
				
		}

		public Options(int rovinka, int oval, string ipAddress, bool isDark, string grpcServer)
		{
			Rovinka = rovinka;
			Oval = oval;
			IpAddress = ipAddress;
			IsDark = isDark;
			GrpcServer = grpcServer;
		}
	}
}