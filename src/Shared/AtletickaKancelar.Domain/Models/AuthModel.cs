﻿using System.ComponentModel.DataAnnotations;

namespace AtletickaKancelar.Domain.Models
{
	public class AuthModel
	{
		[Required] public string UserName { get; set; }
		[Required] public string Password { get; set; }
	}
}
