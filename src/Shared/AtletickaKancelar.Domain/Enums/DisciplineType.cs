﻿namespace AtletickaKancelar.Domain.Enums
{
	public enum DisciplineType
	{
		Sprint,
		MidRun,
		LongRun,
		Throw,
		LongJump,
		HighJump,
		Relay,
		Other
	}
}
