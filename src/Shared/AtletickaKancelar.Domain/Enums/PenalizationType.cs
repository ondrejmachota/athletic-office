﻿namespace AtletickaKancelar.Domain.Enums
{
	public enum PenalizationType // TODO : -1 ??
	{
		None = -1,
		DNS,
		DNF,
		DQ,
		NM
	}
}