﻿namespace AtletickaKancelar.Domain.Enums
{
    public enum PhaseType
    {
        None,
        Rozběh,
        Běh,
        Finále
    }
}
