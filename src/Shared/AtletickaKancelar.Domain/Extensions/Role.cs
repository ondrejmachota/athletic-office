﻿namespace AtletickaKancelar.Domain.Extensions
{
	public static class Role
	{
		public const string Admin = "Admin";
		public const string Athlete = "Athlete";
		public const string Club = "Club";
		public const string Application = "Application";
	}
}
