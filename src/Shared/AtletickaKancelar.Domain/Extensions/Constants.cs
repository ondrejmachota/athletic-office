﻿using System;
using System.IO;
using AtletickaKancelar.Domain.Models;

namespace AtletickaKancelar.Domain.Extensions
{
	public static class Constants
	{
		public static readonly Options Options = new Options(8, 6, "127.0.0.1", false, "https://localhost:5001");

		public static readonly string WorkingDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
			"Ondřej Machota", "AtletickaKancelar");
	}
}