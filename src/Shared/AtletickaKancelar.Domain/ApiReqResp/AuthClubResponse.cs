﻿using AtletickaKancelar.Domain.EFEntities.ApiEntities;

namespace AtletickaKancelar.Domain.ApiReqResp
{
    public class AuthClubResponse
    {
        public int Id { get; }

        public string Name { get; }

        public string Abbreviation { get; }

        public string Token { get; }

        public AuthClubResponse(ApiClub club, string token)
        {
            Id = club.Id;
            Name = club.Name;
            Abbreviation = club.Abbreviation;
            Token = token;
        }
    }
}
