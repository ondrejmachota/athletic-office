﻿using System.ComponentModel.DataAnnotations;

namespace AtletickaKancelar.Domain.ApiReqResp
{
	public class RegistrationModel
	{
		[Required] public string FirstName { get; set; }
		[Required] public string LastName { get; set; }
		[Required] public string Email { get; set; }
		[Required] public string Password { get; set; }
		[Required] public string BirthDate { get; set; }
		[Required] public bool Sex { get; set; }
		[Required] public int ClubId { get; set; }

	}
}
