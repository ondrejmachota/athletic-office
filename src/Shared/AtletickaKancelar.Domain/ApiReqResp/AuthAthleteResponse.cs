﻿using AtletickaKancelar.Domain.EFEntities.ApiEntities;

namespace AtletickaKancelar.Domain.ApiReqResp
{
	public class AuthAthleteResponse
	{
		public int Id { get; }

		public string FullName { get; }

		public string Token { get; }

		public AuthAthleteResponse(ApiAthlete athlete, string token)
		{
			Id = athlete.Id;
			FullName = $"{athlete.FirstName} {athlete.LastName}";
			Token = token;
		}
	}
}
