﻿using AtletickaKancelar.Domain.EFEntities.ApiEntities;
using AtletickaKancelar.Domain.Interfaces;

namespace AtletickaKancelar.Domain.ApiReqResp
{
	public class RegistrationClub : IWithId
	{
		public RegistrationClub(ApiClub club)
		{
			Id = club.Id;
			Name = club.Name;
			Abbreviation = club.Abbreviation;
		}

		public int Id { get; set; }
		public string Name { get; }
		public string Abbreviation { get; }
	}
}
