﻿using System;
using AtletickaKancelar.Domain.Enums;

namespace AtletickaKancelar.Domain.Interfaces
{
	public interface IAthlete : IWithId
	{
		int ClubId { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public Sex Sex { get; set; }

		public DateTime BirthDate { get; set; }
	}
}