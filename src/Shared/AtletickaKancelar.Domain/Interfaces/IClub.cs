﻿namespace AtletickaKancelar.Domain.Interfaces
{
	public interface IClub : IWithId
	{
		string Name { get; set; }
		
		string Abbreviation { get; set; }
	}
}