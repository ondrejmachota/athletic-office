﻿using AtletickaKancelar.Domain.Enums;

namespace AtletickaKancelar.Domain.Interfaces
{
	public interface ICompetitor : IWithId
	{
		int AthleteId { get; set; }
		
		int DisciplineId { get; set; }
		
		int? Track { get; set; }
		
		int? Bib { get; set; }
		
		float? Result { get; set; }
		
		int? Order { get; set; }
		
		int? SecondOrder { get; set; }
		
		int? Points { get; set; }
		
		PenalizationType PenalizationType { get; set; }
	}
}