﻿namespace AtletickaKancelar.Domain.Interfaces
{
	public interface IWithId
	{
		int Id { get; set; }
	}
}