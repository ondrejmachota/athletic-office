﻿using System;

namespace AtletickaKancelar.Domain.Interfaces
{
	public interface IRace : IWithId
	{
		string Title { get; set; }

		string Place { get; set; }

		DateTime Date { get; set; }

		string Note { get; set; }
	}
}
