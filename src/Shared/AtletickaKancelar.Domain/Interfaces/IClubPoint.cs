﻿namespace AtletickaKancelar.Domain.Interfaces
{
	public interface IClubPoint : IWithId
	{
		int ClubId { get; set; }

		int RaceId { get; set; }

		string ClubName { get; set; }

		int Order { get; set; }

		float? Points { get; set; }
	}
}
