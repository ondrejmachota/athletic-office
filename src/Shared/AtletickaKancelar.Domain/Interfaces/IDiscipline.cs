﻿using System;
using AtletickaKancelar.Domain.Enums;

namespace AtletickaKancelar.Domain.Interfaces
{
	public interface IDiscipline : IWithId
	{
		int RaceId { get; set; }

		string Name { get; set; }

		string Category { get; set; }

		DisciplineType DisciplineType { get; set; }

		PhaseType PhaseType { get; set; }

		int? MainId { get; set; }

		TimeSpan Start { get; set; }

		int? CameraId { get; set; }

		int? Order { get; set; }

		bool IsRegistrationsPrinted { get; set; }

		bool IsResultsPrinted { get; set; }
	}
}
