import {Component, OnInit} from '@angular/core';
import {first} from 'rxjs/operators';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ClubService} from '../../Services/club.service';
import {RegistrationService} from '../../Services/registration.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registrationForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  clubs = [];

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private clubService: ClubService,
    private registrationService: RegistrationService
  ) {
    // redirect to home if already logged in
    if (localStorage.getItem('token') != null) {
      this.router.navigateByUrl('/');
    }
    clubService.getClubs().toPromise().then(x => this.clubs = x);
  }

  ngOnInit() {

    // TODO : fetch clubs
    this.clubs = ['TEST', 'TEST'];
    this.registrationForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.required],
      day: ['', Validators.required],
      month: ['', Validators.required],
      year: ['', Validators.required],
      sex: ['', Validators.required],
      club: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registrationForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    const controls = this.f;

    // stop here if form is invalid
    if (this.registrationForm.invalid) {
      return;
    }

    if (controls.password.value !== controls.confirmPassword.value) {
      this.error = 'Hesla se neshodují';
      return;
    }

    this.loading = true;

    // tslint:disable-next-line:max-line-length
    this.registrationService.registrerTest(controls.firstname.value, controls.lastname.value, controls.email.value, controls.password.value, controls.day.value, controls.month.value, controls.year.value, JSON.parse(controls.sex.value), Number(controls.club.value))
      .pipe(first())
      .subscribe(() => {
        {
          this.router.navigate([this.returnUrl]);
          this.loading = false;
        }
      }, error => {
        this.error = error;
        this.loading = false;
      });
  }

  Cancel() {
    this.router.navigateByUrl('/');
  }

}
