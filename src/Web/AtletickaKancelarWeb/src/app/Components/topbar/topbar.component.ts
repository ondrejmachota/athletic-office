import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {AuthenticationService} from '../../Services/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  public IsSignedIn: Observable<boolean>;
  public UserName: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public IsNotSignedIn: Observable<boolean>;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router) {
  }

  ngOnInit(): void {
    this.IsSignedIn = this.authenticationService.isLoggedIn;
    this.parseUser(this.authenticationService.userType);
  }

  parseUser(userType: string) {
    const user = JSON.parse(localStorage.getItem('current' + userType));
    if (user !== null) {
      if (userType === 'Athlete') {
        this.UserName.next(user.fullName);
      } else {
        this.UserName.next(user.name);
      }
    }
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/userlogin']);
    this.IsSignedIn.subscribe(x => false);
  }

}
