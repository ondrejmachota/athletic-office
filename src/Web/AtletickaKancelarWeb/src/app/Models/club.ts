﻿export interface Club {
  // tslint:disable-next-line:variable-name
  Club_Id: number;
  Name: string;
  Abbreviation: string;
  Token: string;
}
