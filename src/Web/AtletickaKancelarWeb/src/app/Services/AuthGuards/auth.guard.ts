import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {AthleteAuthService} from '../athlete-auth.service';
import {ClubAuthService} from '../club-auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private athleteAuthServiceuth: AthleteAuthService,
    private clubAuthService: ClubAuthService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    const currentAthlete = this.athleteAuthServiceuth.currentAthleteValue;
    const currentClub = this.clubAuthService.currentClubValue;
    if (currentAthlete) {
      return true;
    } else {
      if (currentClub) {
        return true;
      }
    }

    this.router.navigate(['/login'], {
      queryParams: {returnUrl: state.url}
    });

    return false;
  }

}
