import { Injectable } from '@angular/core';
import {Club} from '../Models/club';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClubService {

  constructor(private http: HttpClient) { }

  getClubs(): Observable<Club[]> {
    const httpHeaders = new HttpHeaders()
      .set('Accept', ' application/json');
    const options = {
      headers: httpHeaders
    };
    return this.http.get<Club[]>(environment.apiUrl + '/Club/ForRegistration', options);
  }
}
