import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Club} from '../Models/club';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import * as jwt_decode from 'jwt-decode';
// import {AthleteAuthService} from './athlete-auth.service';
// import {ClubAuthService} from './club-auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<Club>;
  public currentUser: Observable<Club>;
  private loggedIn: BehaviorSubject<boolean>;
  // private athleteAuthService: AthleteAuthService;
  // private clubAuthService: ClubAuthService;
  private readonly token: string;
  public userType: string;

  constructor(
    private http: HttpClient
  ) {
    this.token = localStorage.getItem('token');
    if (this.token != null) {
      const decoded = jwt_decode(this.token);
      this.userType = decoded.role;
      // if (this.userType === 'Athlete') {
      //   this.athleteAuthService = new AthleteAuthService();
      // }
      this.currentUserSubject = new BehaviorSubject<Club>(JSON.parse(this.token));
      this.loggedIn = new BehaviorSubject<boolean>(true);
    } else {
      this.currentUserSubject = new BehaviorSubject<Club>(null);
      this.loggedIn = new BehaviorSubject<boolean>(false);
    }
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): Club {
    return this.currentUserSubject.value;
  }

  public get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  login(username: string, password: string, type: boolean) {
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json');
    const options = {
      headers: httpHeaders
    };
    const address = type ? 'AthleteLogin' : 'ClubLogin';
    return this.http.post<any>(`${environment.apiUrl}/Auth/${address}`, JSON.stringify({username, password}), options)
      .pipe(map(res => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        if (address === 'AthleteLogin') {
          localStorage.setItem('currentAthlete', JSON.stringify(res));
        } else {
          localStorage.setItem('currentClub', JSON.stringify(res));
        }
        localStorage.setItem('token', JSON.stringify(res.token));
        const decoded = jwt_decode(res.token);
        this.userType = decoded.role;
        this.currentUserSubject.next(res);
        this.loggedIn.next(true);
        return res;
      }));
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('currentClub');
    localStorage.removeItem('currentAthlete');
    this.currentUserSubject.next(null);
    this.loggedIn.next(false);
  }
}
