import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http: HttpClient) { }

  // tslint:disable-next-line:max-line-length
  registrerTest(firstName: string, lastName: string, email: string, password: string, day: string, month: string, year: string, sex: boolean, clubId: number) {
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json');
    const options = {
      headers: httpHeaders
    };
    const birthDate = `${day}.${month}.${year}`;
    // tslint:disable-next-line:max-line-length
    return this.http.post<any>(`${environment.apiUrl}/Registration`, JSON.stringify({firstName, lastName, email, password, birthDate, sex, clubId}), options)
      .pipe(map(res => {
        return res;
      }));
  }
}
