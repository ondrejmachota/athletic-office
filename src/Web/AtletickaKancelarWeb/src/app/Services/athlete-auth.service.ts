import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Athlete} from '../Models/athlete';
import {HttpClient} from '@angular/common/http';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AthleteAuthService {
  private currentAthleteSubject: BehaviorSubject<Athlete>;
  private readonly currentAthlete: Observable<Athlete>;
  private isLoggedIn: BehaviorSubject<boolean>;
  private readonly token: string;

  constructor() {
    this.token = localStorage.getItem('token');
    if (this.token != null) {
      const decoded = jwt_decode(this.token);
      if (decoded.role === 'Athlete') {
        this.currentAthleteSubject = new BehaviorSubject<Athlete>(JSON.parse(localStorage.getItem('currentAthlete')));
        this.currentAthlete = this.currentAthleteSubject.asObservable();
        this.isLoggedIn = new BehaviorSubject<boolean>(true);
      } else {
        this.notAthlete();
      }
    } else {
      this.notAthlete();
    }
  }

  private notAthlete() {
    this.currentAthleteSubject = new BehaviorSubject<Athlete>(null);
    this.isLoggedIn = new BehaviorSubject<boolean>(false);
  }

  public get currentAthleteValue(): Athlete {
    return this.currentAthleteSubject.value;
  }
}
