import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Club} from '../Models/club';
import {Athlete} from '../Models/athlete';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class ClubAuthService {
  private currentClubSubject: BehaviorSubject<Club>;
  private readonly currentClub: Observable<Club>;
  private isLoggedIn: BehaviorSubject<boolean>;
  private readonly token: string;

  constructor() {
    this.token = localStorage.getItem('token');
    if (this.token != null) {
      const decoded = jwt_decode(this.token);
      if (decoded.role === 'Club') {
        this.currentClubSubject = new BehaviorSubject<Club>(JSON.parse(localStorage.getItem('currentClub')));
        this.currentClub = this.currentClubSubject.asObservable();
        this.isLoggedIn = new BehaviorSubject<boolean>(true);
      } else {
        this.notAthlete();
      }
    } else {
      this.notAthlete();
    }
  }

  private notAthlete() {
    this.currentClubSubject = new BehaviorSubject<Club>(null);
    this.isLoggedIn = new BehaviorSubject<boolean>(false);
  }

  public get currentClubValue(): Club {
    return this.currentClubSubject.value;
  }
}
