﻿using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.Domain.Settings;
using AtletickaKancelar.EntityFramework;
using AtletickaKancelar.EntityFramework.Services;
using AtletickaKancelar.EntityFramework.Services.IServices;
using GrpcServer.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace GrpcServer
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGrpc();
            services.AddSingleton<ISettingsStore, SettingsStore>();

            services.AddSingleton(s => new DbContextFactory(s.GetRequiredService<ISettingsStore>()));
            // Races
            services.AddSingleton<IGenericDataService<Race>, GenericDataService<Race>>();
            services.AddSingleton<IRaceService, RaceService>();
            // Disicplines
            services.AddSingleton<IGenericDataService<Discipline>, GenericDataService<Discipline>>();
            services.AddSingleton<IDisciplineService, DisciplineService>();
            // Clubs
            services.AddSingleton<IGenericDataService<Club>, GenericDataService<Club>>();
            services.AddSingleton<IClubService, ClubService>();
            // Competitors
            services.AddSingleton<IGenericDataService<Competitor>, GenericDataService<Competitor>>();
            // Spojky
            services.AddSingleton<ISpojkaService, SpojkaService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<RaceGrpcService>();
                endpoints.MapGrpcService<DisciplineGrpcService>();
                endpoints.MapGrpcService<ClubGrpcService>();
                endpoints.MapGrpcService<CompetitorGrpcService>();
                endpoints.MapGrpcService<SpojkaGrpcService>();

                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
            });
        }
    }
}
