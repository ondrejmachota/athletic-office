﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.EntityFramework.Services.IServices;
using Grpc.Core;

namespace GrpcServer.Services
{
	public class SpojkaGrpcService : SpojkaGrpcProtoService.SpojkaGrpcProtoServiceBase
	{
		private readonly ISpojkaService _spojkaService;

		public SpojkaGrpcService(ISpojkaService spojkaService)
		{
			_spojkaService = spojkaService;
		}

		public override async Task GetSpojkaStream(EmptySpojkaRequest request, IServerStreamWriter<SpojkaDto> responseStream,
		                                           ServerCallContext context)
		{
			var spojky = await _spojkaService.GetAll();

			await SendSpojkaStream(spojky, responseStream);
		}

		public override async Task CreateSpojkaStream(IAsyncStreamReader<SpojkaDto> requestStream, IServerStreamWriter<SpojkaDto> responseStream,
		                                              ServerCallContext context)
		{
			var spojky = new List<Spojka>();

			while (await requestStream.MoveNext())
			{
				var request = requestStream.Current;
				spojky.Add(request.ToSpojka());
			}

			await _spojkaService.Add(spojky);

			var newSpojky = await _spojkaService.GetSpojkyByDisciplineId(spojky[0].DisciplineId);

			await SendSpojkaStream(newSpojky, responseStream);
		}

		public override async Task GetSpojkaByDisciplineIdSteam(DisciplineIdRequest request, IServerStreamWriter<SpojkaDto> responseStream,
		                                                        ServerCallContext context)
		{
			var spojky = await _spojkaService.GetSpojkyByDisciplineId(request.Id);

			await SendSpojkaStream(spojky, responseStream);
		}

		public override async Task GetSpojkyByMainId(DisciplineIdRequest request, IServerStreamWriter<SpojkaDto> responseStream,
		                                             ServerCallContext context)
		{
			var spojky = await _spojkaService.GetSpojkyByMainId(request.Id);

			await SendSpojkaStream(spojky, responseStream);
		}

		public override async Task GetSpojkyByMainIdWithoudDisicplineId(DisciplineIdRequestWithoudDisicplineId request,
		                                                                IServerStreamWriter<SpojkaDto> responseStream, ServerCallContext context)
		{
			var spojky = await _spojkaService.GetSpojkyByMainIdWithouDisciplineId(request.MaindId, request.DisciplineId);

			await SendSpojkaStream(spojky, responseStream);
		}

		private async Task SendSpojkaStream(IEnumerable<Spojka> spojky, IServerStreamWriter<SpojkaDto> responseStream)
		{
			foreach (var spojka in spojky)
			{
				await responseStream.WriteAsync(spojka.ToSpojkaDto());
			}
		}

		public override async Task<EmptySpojkaRequest> UpdateSpojkaStream(IAsyncStreamReader<SpojkaDto> requestStream, ServerCallContext context)
		{
			var spojky = new List<Spojka>();

			while (await requestStream.MoveNext())
			{
				var request = requestStream.Current;
				spojky.Add(request.ToSpojka());
			}

			await _spojkaService.Update(spojky);

			return await Task.FromResult(new EmptySpojkaRequest());
		}

		public override async Task<EmptySpojkaRequest> DeleteSpojka(SpojkaDto requestStream, ServerCallContext context)
		{
			var spojka = requestStream.ToSpojka();

			await _spojkaService.Remove(spojka);
			
			return await Task.FromResult(new EmptySpojkaRequest());
		}

		public override async Task<EmptySpojkaRequest> UpdateSpojka(SpojkaDto request, ServerCallContext context)
		{
			await _spojkaService.UpdateOne(request.ToSpojka());
			
			return await Task.FromResult(new EmptySpojkaRequest());
		}
	}
}