﻿using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.EntityFramework.Services.IServices;
using Grpc.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GrpcServer.Services
{
    public class ClubGrpcService : ClubGrpcProtoService.ClubGrpcProtoServiceBase
    {
        private readonly IGenericDataService<Club> _genericDataService;
        private readonly IClubService _clubService;
        private readonly ISpojkaService _spojkaService;

        public ClubGrpcService(IGenericDataService<Club> genericDataService, IClubService clubService, ISpojkaService spojkaService)
        {
            _genericDataService = genericDataService;
            _clubService = clubService;
            _spojkaService = spojkaService;
        }

        public override async Task GetClubStream(EmptyClubRequest request, IServerStreamWriter<ClubDto> responseStream, ServerCallContext context)
        {
            IEnumerable<Club> clubs = await _genericDataService.GetAll();

            await SendClubStream(clubs, responseStream);
        }

        public override async Task<ClubDto> CreateClub(ClubDto request, ServerCallContext context)
        {
            var club = await _genericDataService.Create(request.ToClub());

            return club.ToClubDto();
        }

        public override async Task<EmptyClubRequest> EditClub(ClubDto request, ServerCallContext context)
        {
            var club = request.ToClub();
            await _genericDataService.Update(club.Id, club);

            return new EmptyClubRequest();
        }

        public override async Task<ClubDto> UpdateClub(ClubDto request, ServerCallContext context)
        {
            var club = request.ToClub();
            var newClub = await _genericDataService.Update(club.Id, club);

            return newClub.ToClubDto();
        }

        public override async Task<IsUniqueResponse> IsUnique(IsUniqueRequest request, ServerCallContext context)
        {
            bool isUnique = await _clubService.IsUnique(request.Name, request.Abbreviation);

            return await Task.FromResult(new IsUniqueResponse { IsUnique = isUnique });
        }

        public override async Task GetActiveClubIdByDisciplineIdStream(GetActiveClubIdDto request, IServerStreamWriter<GetActiveClubIdDto> responseStream, ServerCallContext context)
        {
            var ids = await _clubService.GetActiveClubIdByDisciplineId(request.Value);

            foreach (var id in ids)
            {
                await responseStream.WriteAsync(new GetActiveClubIdDto { Value = id });
            }

        }

        public override async Task GetActiveClubIdByMainIdStream(GetActiveClubIdDto request, IServerStreamWriter<GetActiveClubIdDto> responseStream, ServerCallContext context)
        {
            var ids = await _clubService.GetActiveClubIdByMainId(request.Value);

            foreach (var id in ids)
            {
                await responseStream.WriteAsync(new GetActiveClubIdDto { Value = id });
            }
        }

        public override async Task GetPoints(GetPointsRequest request, IServerStreamWriter<GetPointsResponse> responseStream, ServerCallContext context)
        {
            await foreach (var clubPoints in _spojkaService.GetPoints(request.RaceId))
            {
                await responseStream.WriteAsync(clubPoints.ToGetPointsResponse());
            }
        }

        private async Task SendClubStream(IEnumerable<Club> clubs, IServerStreamWriter<ClubDto> responseStream)
        {
            foreach (var club in clubs)
            {
                await responseStream.WriteAsync(club.ToClubDto());
            }
        }
    }
}