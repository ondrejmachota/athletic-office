﻿using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.EntityFramework.Services.IServices;
using Grpc.Core;
using System.Threading.Tasks;

namespace GrpcServer.Services
{
	public class CompetitorGrpcService : CompetitorGrpcProtoService.CompetitorGrpcProtoServiceBase
	{
		private readonly IGenericDataService<Competitor> _genericDataService;

		public CompetitorGrpcService(IGenericDataService<Competitor> genericDataService)
		{
			_genericDataService = genericDataService;
		}

		public override async Task GetCompetitorStream(EmptyCompetitorRequest request,
		                                               IServerStreamWriter<CompetitorDto> responseStream,
		                                               ServerCallContext context)
		{
			var competitors = await _genericDataService.GetAll();

			foreach (var competitor in competitors)
			{
				await responseStream.WriteAsync(competitor.ToCompetitorDto());
			}
		}

		public override async Task<CompetitorDto> CreateCompetitor(CompetitorDto request, ServerCallContext context)
		{
			var competitor = await _genericDataService.Create(request.ToCompetitor());
			return competitor.ToCompetitorDto();
		}

		public override async Task<CompetitorDto> UpdateCompetitor(CompetitorDto request, ServerCallContext context)
		{
			var oldCompetitor = request.ToCompetitor();
			var competitor = await _genericDataService.Update(oldCompetitor.Id, oldCompetitor);
			return competitor.ToCompetitorDto();
		}

		public override async Task<CompetitorDto> GetCompetitor(IdCompetitorRequest request, ServerCallContext context)
		{
			var competitor = await _genericDataService.Get(request.Id);
			return competitor.ToCompetitorDto();
		}
	}
}