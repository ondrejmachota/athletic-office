﻿﻿﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.Domain.Enums;
using AtletickaKancelar.EntityFramework.Services.IServices;
using Grpc.Core;

namespace GrpcServer.Services
{
	public class DisciplineGrpcService : DisciplineGrpcProtoService.DisciplineGrpcProtoServiceBase
	{
		private readonly IDisciplineService _disciplineService;
		private readonly IRaceService _raceService;

		public DisciplineGrpcService(IDisciplineService disciplineService,
		                             IRaceService raceService)
		{
			_disciplineService = disciplineService;
			_raceService = raceService;
		}

		public override async Task GetDisciplineStream(EmptyDisciplineRequest request,
		                                               IServerStreamWriter<DisciplineDto> responseStream,
		                                               ServerCallContext context)
		{
			var disicplines = await _disciplineService.GetAll();
			foreach (var discipline in disicplines)
			{
				await responseStream.WriteAsync(discipline.ToDisciplineDto());
			}
		}

		public override async Task<DisciplineDto> CreateDiscipline(
			CreateDisciplineGrpcModel request, ServerCallContext context)
		{
			var activeRace = await _raceService.GetActiveRace();
			request.DisciplineModel.RaceId = activeRace.Id;
			var discipline = await _disciplineService.Create(request.DisciplineModel.ToDiscipline(), request.AssignMainId);
			return await Task.FromResult(discipline.ToDisciplineDto());
		}

		public override async Task<DisciplineDto> CreateChildDiscipline(
			CreateChildDisciplineGrpcModel request, ServerCallContext context)
		{
			var discipline = await _disciplineService.CreateChildDiscipline(request.DisciplineModel.ToDiscipline(),
				request.CameraId, request.Order);
			return await Task.FromResult(discipline.ToDisciplineDto());
		}

		public override async Task<EmptyDisciplineRequest> EditDiscipline(
			DisciplineDto request, ServerCallContext context)
		{
			await _disciplineService.Update(request.ToDiscipline());
			return new EmptyDisciplineRequest();
		}

		public override async Task<DisciplineDto> UpdateDiscipline(
			DisciplineDto request, ServerCallContext context)
		{
			await _disciplineService.Update(request.ToDiscipline());
			return new DisciplineDto(); // TODO : not finished
		}

		public override async Task<EmptyDisciplineRequest> DeleteDiscipline(
			DisciplineDto request, ServerCallContext context)
		{
			await _disciplineService.Remove(request.ToDiscipline());
			return new EmptyDisciplineRequest();
		}

		public override async Task<DisciplineDto> GetSimilarDiscipline(
			GetSimilarDisciplineGrpcModel request, ServerCallContext context)
		{
			var discipline = await _disciplineService.GetSimilar(request.Name, request.Category,
				(PhaseType) request.PhaseType, request.Start.ToTimeSpan());
			if (discipline == null)
				return await Task.FromResult(new DisciplineDto());
			return await Task.FromResult(discipline.ToDisciplineDto());
		}

		public override async Task GetChildDisciplinesOrderStream(ChildDisciplineOrderModel request,
		                                                          IServerStreamWriter<ChildDisciplineOrderModel>
			                                                          responseStream,
		                                                          ServerCallContext context)
		{
			var activeRace = await _raceService.GetActiveRace();
			var orders = await _disciplineService.GetChildDisciplinesOrders(request.Value, activeRace.Id);

			foreach (var order in orders)
			{
				var orderModel = new ChildDisciplineOrderModel {Value = order};
				await responseStream.WriteAsync(orderModel);
			}
		}

		public override async Task<EmptyDisciplineRequest> UpdateDisciplineStream(IAsyncStreamReader<DisciplineDto> requestStream, ServerCallContext context)
		{
			var disciplines = new List<Discipline>();

			while (await requestStream.MoveNext())
			{
				disciplines.Add(requestStream.Current.ToDiscipline());
			}

			await _disciplineService.UpdateList(disciplines);

			return await Task.FromResult(new EmptyDisciplineRequest());
		}

		public override async Task<ChildDisciplineOrderModel> GetChildDisciplinesCount(ChildDisciplineOrderModel request, ServerCallContext context)
		{
			int? count = await _disciplineService.ChildDisciplinesCount(request.Value);

			return await Task.FromResult(new ChildDisciplineOrderModel {Value = count});
		}
	}
}