﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace AtletickaKancelar.WPF.Controls
{
	public partial class NavigationMenu : UserControl
	{
		public NavigationMenu()
		{
			InitializeComponent();
		}

		private void GridMenu_MouseLeave(object sender, MouseEventArgs e)
		{
			if (GridMenu.Width > 60 && IsClosing == false)
			{
				IsClosing = true;
				var sb = (Storyboard)Resources["CloseMenu"]; // Report if missing
				var animationFrame = (DoubleAnimationUsingKeyFrames)sb.Children[0];
				var keyFrame = (EasingDoubleKeyFrame)animationFrame.KeyFrames[0];
				if (Math.Abs(GridMenu.Width - 200) < 0.05)
				{
					keyFrame.Value = 200;
				}
				else
				{
					keyFrame.Value = GridMenu.Width;
				}

				sb.Begin();
				keyFrame.Value = 200;
			}

			MenuToggleButton.IsChecked = false;
		}

		private bool IsClosing { get; set; }

		private void MenuToggleButton_OnClick(object sender, RoutedEventArgs e)
		{
			if (MenuToggleButton.IsChecked == true)
			{
				var sb = (Storyboard)Resources["OpenMenu"];
				sb?.Begin();
			}
			else
			{
				IsClosing = true;
				var sb = (Storyboard)Resources["CloseMenu"];
				sb?.Begin();
			}

			IsClosing = false;
		}

		private async Task KontrolaOtevreniGridMenu()
		{
			if (GridMenu.Width > 60)
			{
				IsClosing = true;
				var sb = (Storyboard)Resources["CloseMenu"];
				sb?.Begin();
				MenuToggleButton.IsChecked = false;
				await Task.Delay(300);
			}

			IsClosing = false;
		}

		private async void ButtonBase_OnClick(object sender, RoutedEventArgs e)
		{
			await KontrolaOtevreniGridMenu();
		}
	}
}
