﻿using System.Windows;
using System.Windows.Input;

namespace AtletickaKancelar.WPF
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow(object dataContext)
		{
			InitializeComponent();
			
			DataContext = dataContext;
		}
		
		protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			base.OnMouseLeftButtonDown(e);
			// Begin dragging the window
			try
			{
				DragMove();
			}
			catch
			{
				// ignored
			}
		}
	}
}