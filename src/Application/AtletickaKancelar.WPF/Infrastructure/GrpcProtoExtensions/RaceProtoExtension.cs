﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using AtletickaKancelar.WPF.Converters;
using AtletickaKancelar.WPF.Models;
using Grpc.Core;
using GrpcServer;

namespace AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions
{
	public static class RaceProtoExtension
	{
		public static RaceGrpcProtoService.RaceGrpcProtoServiceClient Create()
		{
			var channel = GrpcChannelFactory.CreateChannel();
			return new RaceGrpcProtoService.RaceGrpcProtoServiceClient(channel);
		}
		
		public static async Task<ObservableCollection<RaceFv>> DownloadRaces(AsyncServerStreamingCall<RaceDto> call)
		{
			List<RaceFv> races = new List<RaceFv>();
					
			while (await call.ResponseStream.MoveNext().ConfigureAwait(false))
			{
				var currentRace = call.ResponseStream.Current;
				RaceFv race = currentRace.ToRaceFv();
				races.Add(race);
			}

			return new ObservableCollection<RaceFv>(races);
		}
		
		public static async IAsyncEnumerable<RaceFv> DownloadRacesAsync(AsyncServerStreamingCall<RaceDto> call)
		{
			while (await call.ResponseStream.MoveNext().ConfigureAwait(false))
			{
				var currentRace = call.ResponseStream.Current;
				RaceFv race = currentRace.ToRaceFv();
				await Task.Delay(2);
				yield return race;
			}
		}

		public static async Task<RaceFv> GetActiveRace()
		{
			var client = Create();
			var race = await client.GetActiveRaceAsync(new EmptyRequest());
			return race.ToRaceFv();
		}
	}
}