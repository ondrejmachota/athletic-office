﻿﻿using System.Net.Http;
 using AtletickaKancelar.Domain.Settings;
using Grpc.Net.Client;

namespace AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions
{
	public static class GrpcChannelFactory
	{
		public static GrpcChannel CreateChannel()
		{
			// var cert = new X509Certificate2("cert_fleName", "cert_password");

			var httpClientHandler = new HttpClientHandler
			{
				ServerCertificateCustomValidationCallback =
					HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
			};
			// httpClientHandler.ClientCertificates.Add(cert);
			// Return `true` to allow certificates that are untrusted/invalid
			var client = new HttpClient(httpClientHandler);
			var opt = new GrpcChannelOptions { HttpClient = client };

			ISettingsStore settingsStore = new SettingsStore();
			var options = settingsStore.Load();
			return GrpcChannel.ForAddress(options.GrpcServer, opt); // TODO : fetch from the settings file
		}
	}
}
