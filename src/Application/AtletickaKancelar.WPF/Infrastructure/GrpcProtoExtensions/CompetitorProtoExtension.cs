﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using AtletickaKancelar.WPF.Converters;
using AtletickaKancelar.WPF.Models;
using Grpc.Core;
using GrpcServer;

namespace AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions
{
	public static class CompetitorProtoExtension
	{
		public static CompetitorGrpcProtoService.CompetitorGrpcProtoServiceClient Create()
		{
			var channel = GrpcChannelFactory.CreateChannel();
			return new CompetitorGrpcProtoService.CompetitorGrpcProtoServiceClient(channel);
		}

		public static async Task<ObservableCollection<CompetitorMainModel>> DownloadCompetitors(AsyncServerStreamingCall<CompetitorDto> call)
		{
			List<CompetitorMainModel> competitors = new List<CompetitorMainModel>();

			while (await call.ResponseStream.MoveNext().ConfigureAwait(false))
			{
				var currentCompetitor = call.ResponseStream.Current;
				CompetitorModel competitor = currentCompetitor.ToCompetitorModel();
				competitors.Add(new CompetitorMainModel(competitor));
			}

			return new ObservableCollection<CompetitorMainModel>(competitors);
		}

		public static async IAsyncEnumerable<CompetitorMainModel> DownloadCompetitorsAsync(AsyncServerStreamingCall<CompetitorDto> call)
		{
			while (await call.ResponseStream.MoveNext().ConfigureAwait(false))
			{
				var currentCompetitor = call.ResponseStream.Current;
				var competitor = new CompetitorMainModel(currentCompetitor.ToCompetitorModel());
				await Task.Delay(2).ConfigureAwait(false);
				yield return competitor;
			}
		}

		public static async Task UpdateCompetitor(CompetitorMainModel competitor)
		{
			await Create().UpdateCompetitorAsync(competitor.Competitor.ToCompetitorDto());
		}

		public static async Task<CompetitorMainModel> CreateCompetitor(CompetitorMainModel competitor)
		{
			var response = await Create().CreateCompetitorAsync(competitor.Competitor.ToCompetitorDto());
			return new CompetitorMainModel(response.ToCompetitorModel());
		}
	}
}
