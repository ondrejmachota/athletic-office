﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.WPF.Converters;
using Grpc.Core;
using GrpcServer;

namespace AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions
{
	public static class ClubProtoExtension
	{
		private static ClubGrpcProtoService.ClubGrpcProtoServiceClient Create()
		{
			var channel = GrpcChannelFactory.CreateChannel();
			return new ClubGrpcProtoService.ClubGrpcProtoServiceClient(channel);
		}

		public static async Task<List<int>> GetActiveClubsId(bool isRun, int id)
		{
			var client = Create();

			if (isRun)
			{
				using var call = client.GetActiveClubIdByMainIdStream(new GetActiveClubIdDto {Value = id});
				return await DownloadClubIds(call);
			}
			else
			{
				using var call = client.GetActiveClubIdByDisciplineIdStream(new GetActiveClubIdDto {Value = id});
				return await DownloadClubIds(call);
			}
		}
		
		public static async Task<List<Club>> DownloadClubs()
		{
			var clubs = new List<Club>();
			
			using var call = Create().GetClubStream(new EmptyClubRequest());
			while (await call.ResponseStream.MoveNext())
			{
				clubs.Add(call.ResponseStream.Current.ToClub());
			}

			return clubs;
		}

		private static async Task<List<int>> DownloadClubIds(AsyncServerStreamingCall<GetActiveClubIdDto> call)
		{
			var ids = new List<int>();
			
			while (await call.ResponseStream.MoveNext())
			{
				ids.Add(call.ResponseStream.Current.Value);
			}

			return ids;
		}
		
		public static async Task<Club> CreateClub(Club club)
		{
			var response = await Create().CreateClubAsync(club.ToClubDto());
			return response.ToClub();
		}

		public static async Task<bool> IsUnique(string name, string abbreviation)
		{
			var response = await Create().IsUniqueAsync(new IsUniqueRequest {Abbreviation = abbreviation, Name = name});
			return response.IsUnique;
		}

		public static async Task<List<ClubPoints>> GetPoints(int raceId)
        {
	        var points = new List<ClubPoints>();
	        
	        using var call = Create().GetPoints(new GetPointsRequest {RaceId = raceId});
	        while (await call.ResponseStream.MoveNext())
	        {
		        points.Add(call.ResponseStream.Current.ToClubPoints());
	        }

	        return points;
        }
	}
}