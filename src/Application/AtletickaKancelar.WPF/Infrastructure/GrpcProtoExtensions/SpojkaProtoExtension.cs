﻿﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.WPF.Converters;
using AtletickaKancelar.WPF.Models;
using Grpc.Core;
using GrpcServer;

namespace AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions
{
	public static class SpojkaProtoExtension
	{
		public static SpojkaGrpcProtoService.SpojkaGrpcProtoServiceClient Create()
		{
			var channel = GrpcChannelFactory.CreateChannel();
			return new SpojkaGrpcProtoService.SpojkaGrpcProtoServiceClient(channel);
		}

		public static async Task<List<SpojkaModel>> GetAll()
		{
			List<SpojkaModel> spojky = new List<SpojkaModel>();

			using var call = Create().GetSpojkaStream(new EmptySpojkaRequest());
			while (await call.ResponseStream.MoveNext().ConfigureAwait(false))
			{
				var currentSpojka = call.ResponseStream.Current;
				spojky.Add(currentSpojka.ToSpojkaModel());
			}

			return spojky;
		}

		public static async Task<List<SpojkaModel>> DownloadSpojkyByMainIdWithoudDiscipline(int maindId, int disciplineId)
		{
			List<SpojkaModel> spojky = new List<SpojkaModel>();

			using var call = Create().GetSpojkyByMainIdWithoudDisicplineId(new DisciplineIdRequestWithoudDisicplineId { MaindId = maindId, DisciplineId = disciplineId});
			while (await call.ResponseStream.MoveNext())
			{
				var currentSpojka = call.ResponseStream.Current;
				spojky.Add(currentSpojka.ToSpojkaModel());
			}

			return spojky;
		}

		public static async Task<List<SpojkaModel>> DownloadSpojkyByDisciplineId(int id)
		{
			List<SpojkaModel> spojky = new List<SpojkaModel>();

			using var call = Create().GetSpojkaByDisciplineIdSteam(new DisciplineIdRequest { Id = id });
			while (await call.ResponseStream.MoveNext().ConfigureAwait(false))
			{
				var currentSpojka = call.ResponseStream.Current;
				spojky.Add(currentSpojka.ToSpojkaModel());
			}

			return spojky;
		}

		public static async Task<ObservableCollection<SpojkaMainModel>> DownloadSpojkyAsObservable(AsyncServerStreamingCall<SpojkaDto> call)
		{
			List<SpojkaMainModel> spojky = new List<SpojkaMainModel>();

			while (await call.ResponseStream.MoveNext())
			{
				var currentSpojka = call.ResponseStream.Current;
				SpojkaModel spojka = currentSpojka.ToSpojkaModel();
				spojky.Add(new SpojkaMainModel(spojka));
			}

			return new ObservableCollection<SpojkaMainModel>(spojky.OrderByDescending(x => x.Spojka.Order.HasValue).ThenBy(x => x.Spojka.Order));
		}

		public static async Task<List<SpojkaMainModel>> DownloadSpojky(AsyncServerStreamingCall<SpojkaDto> call)
		{
			List<SpojkaMainModel> spojky = new List<SpojkaMainModel>();

			while (await call.ResponseStream.MoveNext())
			{
				var currentSpojka = call.ResponseStream.Current;
				SpojkaModel spojka = currentSpojka.ToSpojkaModel();
				spojky.Add(new SpojkaMainModel(spojka));
			}

			return spojky;
		}

		public static async IAsyncEnumerable<SpojkaMainModel> DownloadSpojkyAsync(AsyncServerStreamingCall<SpojkaDto> call)
		{
			while (await call.ResponseStream.MoveNext().ConfigureAwait(false))
			{
				var currentSpojka = call.ResponseStream.Current;
				var spojka = new SpojkaMainModel(currentSpojka.ToSpojkaModel());
				yield return spojka;
			}
		}

		public static async Task<List<SpojkaMainModel>> Add(IEnumerable<SpojkaDto> spojkyGrpc)
		{
			using var call = Create().CreateSpojkaStream();
			foreach (var spojkaGrpc in spojkyGrpc)
			{
				await call.RequestStream.WriteAsync(spojkaGrpc);
			}

			await call.RequestStream.CompleteAsync();

			List<SpojkaMainModel> spojky = new List<SpojkaMainModel>();

			// TODO : download only created spojky

			while (await call.ResponseStream.MoveNext().ConfigureAwait(false))
			{
				var currentSpojka = call.ResponseStream.Current;
				SpojkaModel spojka = currentSpojka.ToSpojkaModel();
				spojky.Add(new SpojkaMainModel(spojka));
			}

			return spojky;
		}

		public static async Task<List<SpojkaDto>> CheckSpojky(List<CompetitorMainModel> competitors, int disciplineId, bool isRun, int? mainId)
		{
			List<SpojkaMainModel> intertalSearchSpojky = await GetSpojky(isRun, disciplineId, mainId);
			List<SpojkaDto> spojky = new List<SpojkaDto>();
			for (var i = 0; i < competitors.Count; i++)
			{
				var isCompetitorRegistered = false;
				if (isRun)
				{
					for (var j = 0; j < intertalSearchSpojky.Count; j++) // Look for the competitor in other runs
					{
						if (intertalSearchSpojky[j].Spojka.CompetitorId == competitors[i].Competitor.Id)
						{
							isCompetitorRegistered = true;
							break;
						}
					}
				}
				else
				{
					var spojkaMainModels = intertalSearchSpojky.Where(x => x.Spojka.DisciplineId == disciplineId).ToList();
					for (int j = 0; j < spojkaMainModels.Count; j++)
					{
						if (spojkaMainModels[j].Spojka.CompetitorId == competitors[i].Competitor.Id)
						{
							isCompetitorRegistered = true;
							break;
						}
					}
				}

				if (isCompetitorRegistered == false)
				{
					Spojka spojka = new Spojka
					{
						CompetitorId = competitors[i].Competitor.Id,
						DisciplineId = disciplineId,
						MainId = mainId,
						ClubId = competitors[i].Competitor.ClubId,
						ClubAbbreviation = competitors[i].Competitor.ClubAbbreviation,
						FullName = competitors[i].FullName,
						BirthDate = competitors[i].Competitor.BirthDate.ToString(CultureInfo.CurrentCulture),
						PenalizationType = 0
					};
					spojky.Add(spojka.ToSpojkaDto());
				}
			}

			return spojky;
		}

		private static async Task<List<SpojkaMainModel>> GetSpojky(bool isRun, int id, int? mainId)
		{
			List<SpojkaMainModel> intertalSearchSpojky;
			var client = Create();

			if (isRun)
			{
				using (var call = client.GetSpojkyByMainId(new DisciplineIdRequest {Id = Convert.ToInt32(mainId)}))
				{
					intertalSearchSpojky = await DownloadSpojky(call);
				}
			}
			else
			{
				using (var call = client.GetSpojkaByDisciplineIdSteam(new DisciplineIdRequest {Id = id}))
				{
					intertalSearchSpojky = await DownloadSpojky(call);
				}
			}

			return intertalSearchSpojky;
		}

		/// <summary>
		/// Gets competitors by DisciplineId or MainId. Depends on competitorsByMainId
		/// </summary>
		/// <param name="competitorsByMainId">True = by MainId; False = by disciplineId</param>
		/// <param name="id">Discipline.MainId or Discipline.Id</param>
		/// <returns></returns>
		public static async Task<List<SpojkaMainModel>> GetSpojkyById(bool competitorsByMainId, int id)
		{
			var client = Create();

			if (competitorsByMainId) // True - by mainId
			{
				using var call = client.GetSpojkyByMainId(new DisciplineIdRequest {Id = id});
				return await DownloadSpojky(call);
			}
			else
			{
				using var call = client.GetSpojkaByDisciplineIdSteam(new DisciplineIdRequest {Id = id});
				return await DownloadSpojky(call);
			}
		}

		public static async Task UpdateSpojky(List<SpojkaMainModel> spojky)
		{
			var client = Create();
			using var call = client.UpdateSpojkaStream();
			var spojkyDto = spojky.Select(x => x.Spojka.ToSpojkaDto());
			foreach (var spojkaGrpc in spojkyDto)
			{
				await call.RequestStream.WriteAsync(spojkaGrpc);
			}

			await call.RequestStream.CompleteAsync();

			await call.ResponseAsync;
		}

		public static async Task UpdateSpojka(SpojkaMainModel spojka)
		{
			await Create().UpdateSpojkaAsync(spojka.Spojka.ToSpojkaDto());
		}

		public static async Task DeleteSpojka(SpojkaMainModel spojka)
		{
			await Create().DeleteSpojkaAsync(spojka.Spojka.ToSpojkaDto());
		}
	}
}
