﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AtletickaKancelar.WPF.Converters;
using AtletickaKancelar.WPF.Models;
using Grpc.Core;
using GrpcServer;

namespace AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions
{
	public static class ScheduleProtoExtension
	{
		public static DisciplineGrpcProtoService.DisciplineGrpcProtoServiceClient Create()
		{
			var channel = GrpcChannelFactory.CreateChannel();
			return new DisciplineGrpcProtoService.DisciplineGrpcProtoServiceClient(channel);
		}

		public static async IAsyncEnumerable<DisciplineMainModel> DownloadDisciplinesAsync(AsyncServerStreamingCall<DisciplineDto> call)
		{
			while (await call.ResponseStream.MoveNext().ConfigureAwait(false))
			{
				var currentDiscipline = call.ResponseStream.Current;
				var discipline = new DisciplineMainModel(currentDiscipline.ToDisciplineModel());
				await Task.Delay(2).ConfigureAwait(false);
				yield return discipline;
			}
		}

		public static async Task UpdateListDisciplines(List<DisciplineModel> disciplines)
		{
			using var call = Create().UpdateDisciplineStream();
			foreach (var discipline in disciplines)
			{
				await call.RequestStream.WriteAsync(discipline.ToDisciplineDto());
			}

			await call.RequestStream.CompleteAsync();

			await call.ResponseAsync;
		}

		public static async Task<int?> ChildDisciplinesCount(int? mainId)
		{
			var count = await Create().GetChildDisciplinesCountAsync(new ChildDisciplineOrderModel {Value = mainId});
			return count.Value;
		}
	}
}
