﻿using AtletickaKancelar.Domain.Enums;

namespace AtletickaKancelar.WPF.Infrastructure
{
    public class DisciplineConstant
    {
        public string Name { get; }

        public DisciplineType DisciplineType { get; }

        public DisciplineConstant(string name, DisciplineType disciplineType)
        {
            Name = name;
            DisciplineType = disciplineType;
        }

        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
