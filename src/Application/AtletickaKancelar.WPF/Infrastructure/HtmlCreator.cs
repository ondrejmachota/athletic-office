﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions;
using AtletickaKancelar.WPF.Models;

namespace AtletickaKancelar.WPF.Infrastructure
{
	public static class HtmlCreator
	{
		#region Shared

		private static void CreateUvod(RaceFv race, string header, PrintMode printMode)
		{
			StringBuilder sb = new StringBuilder($@"<!DOCTYPE html>
<html lang='cs'>
<head>
    <meta charset='UTF-8'>
    <link href='Css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='Css/style.css' rel='stylesheet' type='text/css'>
    <title>{header}</title>
</head>
<body>
<div class='page-footer'>
    Ondřej Machota a organizační tým
</div>
<div class='page-header' style='text-align: center'>
    <div>
        <h3 class='nadpis'>{race.Title}</h3>
        <h4 class='nadpis'>{race.Place}  -  {race.Date.ToShortDateString()}</h4>
    </div>
    <div class='jumbotron-custom'>
        <h5 class='nadpis'>{header.ToUpper()}</h5>
    </div>
</div>
<div id='content'>
    <!-- Automaticky generované -->
    <table>
        <thead id='header'>
        <tr>
            <td>
                <!--place holder for the fixed-position header-->
                <div class='page-header-space'></div>
            </td>
        </tr>
        </thead>
        <tbody id='main'>");
			switch (printMode)
			{
				case PrintMode.Registrations:
					PrihlaskySb.Append(sb);
					break;
				case PrintMode.Results:
					VysledkySb.Append(sb);
					break;
				case PrintMode.TimeTable:
					CasovyPoradSb.Append(sb);
					break;
				case PrintMode.TotalResults:
					TotalResults.Append(sb);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(printMode), printMode, null);
			}
		}

		#endregion

		#region Vysledky

		public static async Task CreateSaveOpenVysledky(List<DisciplineModel> listDiscipliny, List<SpojkaModel> listSpojky)
		{
			RaceFv race = await RaceProtoExtension.GetActiveRace();
			VysledkySb.Clear();
			CreateUvod(race, "Výsledky", PrintMode.Results);
			CreateTablesVysledky(listDiscipliny, listSpojky);
			CreateEndVysledky();
			await UlozeniAOtevreniHtmlStranky("vysledky", VysledkySb);
			await UpdateDisciplinePrintIcon(listDiscipliny);
		}

		private static void CreateTablesVysledky(List<DisciplineModel> listDiscipliny, List<SpojkaModel> listSpojky)
		{
			string prazdnyRadek = "";
			for (int i = 0; i < listDiscipliny.Count; i++)
			{
				// List<SpojkaModel> vybrani = listSpojky.Where(x => x.DisciplineId == listDiscipliny[i].Id).ToList();
				List<SpojkaModel> selectedCompetitors = listSpojky.Where(x => x.DisciplineId == listDiscipliny[i].Id).OrderByDescending(x => x.Order.HasValue).ThenBy(x => x.Order).ToList();
				string pocetAtletuText = selectedCompetitors.Count > 4
					? $"Celkem {selectedCompetitors.Count} atletů/atletek"
					: $"Celkem {selectedCompetitors.Count} atleti/atletky";
				StringBuilder tabulkaText = new StringBuilder();
				for (int j = 0; j < selectedCompetitors.Count; j++)
				{
					string vykon;
					if (selectedCompetitors[j].PenalizationType != Domain.Enums.PenalizationType.None)
						vykon = selectedCompetitors[j].PenalizationType.ToString();
					else
						vykon = selectedCompetitors[j].Result.ToString() ?? "";
					tabulkaText.Append($@"
                <tr>
                    <td class='td-uzky'>{selectedCompetitors[j].Order}</td>
                    <td class='td-uzky'>{selectedCompetitors[j].Track}</td>
                    <td class='td-uzky'>{selectedCompetitors[j].BIB}</td>
                    <td>{selectedCompetitors[j].FullName}</td>
                    <td>{selectedCompetitors[j].BirthDate}</td>
                    <td>{selectedCompetitors[j].ClubAbbreviation}</td>
                    <td>{vykon}</td>
                    <td class='td-uzky'>{selectedCompetitors[j].Points}</td>
                </tr>");
				}

				string infoODiscipline = $"{listDiscipliny[i].Name} {listDiscipliny[i].Category}";
				if (Extensions.IsRun(listDiscipliny[i].DisciplineType))
					infoODiscipline += $" - Běh: {listDiscipliny[i].Order}";
				if (i < 0)
					prazdnyRadek = "                <p class='prazdny-radek'></p>";

				VysledkySb.Append($@"
                <tr>
            <td class='page'>
        <div>
            <section>
{prazdnyRadek}
            <h5 class='nadpis' style='margin-top: 0.2rem'>{infoODiscipline}</h5>
            <table class='table table-hover'>
                <thead>
                <tr>
                    <th class='th-uzky'>Umís.</th>
                    <th class='th-uzky'>Dráha</th>
                    <th class='th-uzky'>BIB</th>
                    <th>Jméno</th>
                    <th>Narozen</th>
                    <th>Oddíl</th>
                    <th>Výkon</th>
                    <th class='th-uzky'>Body</th>
                </tr>
                </thead>
                <tbody>
                {tabulkaText}
                </tbody>
            </table>
            <hr/>
            <p class='pocet-atletu'>{pocetAtletuText}</p>
            <hr/>
            </section>
        </div>
            </td>
        </tr>");
			}
		}

		private static void CreateEndVysledky()
		{
			VysledkySb.Append(@" <tfoot id='footer'>
        <tr>
            <td>
                <!--place holder for the fixed-position footer-->
                <div class='page-footer-space'></div>
            </td>
        </tr>
        </tfoot>
    </table>
</div>
</body>
</html>");
		}

		#endregion

		#region Prihlasky

		public static async Task CreateSaveOpenPrihlasky(List<DisciplineModel> listDiscipliny, List<SpojkaModel> listSpojky)
		{
			RaceFv race = await RaceProtoExtension.GetActiveRace();
			PrihlaskySb.Clear();
			CreateUvod(race, "Přihlášky", PrintMode.Registrations);
			CreateTablesPrihlasky(listDiscipliny, listSpojky);
			CreateEndPrihlasky();
			await UlozeniAOtevreniHtmlStranky("prihlasky", PrihlaskySb);
			await UpdateDisciplinePrintIcon(listDiscipliny);
		}

		private static void CreateTablesPrihlasky(List<DisciplineModel> listDiscipliny, List<SpojkaModel> listSpojky)
		{
			string prazdnyRadek = "";
			PrihlaskySb.Append(@"
        <tr>
            <td class='page'>");
			for (int i = 0; i < listDiscipliny.Count; i++)
			{
				List<SpojkaModel> vybrani = listSpojky.Where(x => x.DisciplineId == listDiscipliny[i].Id).ToList();
				List<SpojkaModel> listVybrani = vybrani.OrderByDescending(x => x.Track.HasValue).ThenBy(x => x.Track).ToList();
				string pocetAtletuText = listVybrani.Count > 4
					? $"Celkem {listVybrani.Count} atletů/atletek"
					: $"Celkem {listVybrani.Count} atleti/atletky";
				StringBuilder tabulkaText = new StringBuilder();
				string tHead;
				if (Extensions.IsRun(listDiscipliny[i].DisciplineType))
					tHead = @"
                            <thead>
                            <tr>
                                <th class='th-uzky'>Dráha</th>
                                <th class='th-uzky'>BIB</th>
                                <th>Jméno</th>
                                <th>Narozen</th>
                                <th class='th-oddil'>Oddíl</th>
                                <th class='th-uzky'>Pořadí</th>
                                <th class='th-cas'>Čas</th>
                            </tr>
                            </thead>";
				else if (listDiscipliny[i].DisciplineType == Domain.Enums.DisciplineType.LongJump ||
				         listDiscipliny[i].DisciplineType == Domain.Enums.DisciplineType.Throw)
				{
					tHead = $@"
                            <thead>
                            <tr>
                                <th class='th-uzky'>BIB</th>
                                <th style='width: 180px;'>Jméno</th>
                                <th>Narozen</th>
                                <th class='th-oddil' style='width: 200px;'>Oddíl</th>
                                <th class='th-technickyVykon'>Pokus 1</th>
                                <th class='th-technickyVykon'>Pokus 2</th>
                                <th class='th-technickyVykon'>Pokus 3</th>
                                <th class='th-technickyVykon'>Výkon</th>
                            </tr>
                            </thead>";
				}
				else
				{
					StringBuilder sloupce = new StringBuilder();
					for (int j = 0; j < 18; j++)
						sloupce.Append("<th class='th-vyska'></th>");
					tHead = $@"
                                    <thead>
                                    <tr>
                                        <th class='th-uzky'>BIB</th>
                                        <th style='width: 180px;'>Jméno</th>
                                        <th class='th-oddil' style='width: 300px'>Oddíl</th>
                                        {sloupce}
                                    </tr>
                                    </thead>
                            ";
				}

				for (int j = 0; j < listVybrani.Count; j++)
				{
					if (Extensions.IsRun(listDiscipliny[i].DisciplineType))
					{
						tabulkaText.Append($@"
                           <tr>
                                <td class='td-uzky'>{listVybrani[j].Track}</td>
                                <td class='td-uzky'>{listVybrani[j].BIB}</td>
                                <td>{listVybrani[j].FullName}</td>
                                <td>{listVybrani[j].BirthDate}</td>
                                <td>{listVybrani[j].ClubAbbreviation}</td>
                                <td class='td-uzky'></td>
                                <td class='td-cas'></td>
                           </tr>");
					}
					else if (listDiscipliny[i].DisciplineType == Domain.Enums.DisciplineType.LongJump ||
					         listDiscipliny[i].DisciplineType == Domain.Enums.DisciplineType.Throw)
					{
						tabulkaText.Append($@"
                            <tr>
                                <td class='td-uzky'>{listVybrani[j].BIB}</td>
                                <td>{listVybrani[j].FullName}</td>
                                <td>{listVybrani[j].BirthDate}</td>
                                <td>{listVybrani[j].ClubAbbreviation}</td>
                                <td class='td-uzky'></td>
                                <td class='td-uzky'></td>
                                <td class='td-uzky'></td>
                                <td class='td-uzky'></td>
                           </tr>");
					}
					else
					{
						StringBuilder sloupce = new StringBuilder();
						for (int k = 0; k < 18; k++)
							sloupce.Append("<td class='th-vyska'></td>");
						tabulkaText.Append($@"
                            <tr>
                                <td class='td-uzky'>{listVybrani[j].BIB}</td>
                                <td>{listVybrani[j].FullName}</td>
                                <td>{listVybrani[j].ClubAbbreviation}</td>
                                {sloupce}
                           </tr>");
					}
				}

				string infoODiscipline = $"{listDiscipliny[i].Name} {listDiscipliny[i].Category}";
				if (Extensions.IsRun(listDiscipliny[i].DisciplineType))
					infoODiscipline += $" - Běh: {listDiscipliny[i].Order}";
				if (i < 0)
					prazdnyRadek = "<p class='prazdny-radek'></p>";
				if (listDiscipliny[i].DisciplineType != Domain.Enums.DisciplineType.HighJump)
				{
					PrihlaskySb.Append($@" 
                    <div>
                    <section>{prazdnyRadek}
                        <h5 class='nadpis' style='margin-top: 0.2rem; text-align: center'>{infoODiscipline}</h5>
                        <table class='table table-hover table-bordered'>
                        {tHead}
                            <tbody>
                                {tabulkaText}
                            </tbody>
                        </table>
                        <hr/>
                        <p class='pocet-atletu'>{pocetAtletuText}</p>
                        <p class='pocet-atletu'>Start: {listDiscipliny[i].Start.Hours}:{listDiscipliny[i].Start.Minutes:00}</p>
                        <hr/>
                    </section>
                    </div>");
				}
				else
				{
					PrihlaskySb.Append($@" 
                    <div>
                    <section>{prazdnyRadek}
                        <h5 class='nadpis' style='margin-top: 0.2rem; text-align: center'>{infoODiscipline}</h5>
                            <div class='rotation-wrapper-outer' style='height: 1850px'>
                                <div class='rotation-wrapper-inner' style='height: 1200px'>
                                <table class='table table-hover table-bordered element-to-rotate' style='width: 120%'>
                                {tHead}
                                    <tbody>
                                        {tabulkaText}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr/>
                        <p class='pocet-atletu'>{pocetAtletuText}</p>
                        <p class='pocet-atletu'>Start: {listDiscipliny[i].Start.Hours}:{listDiscipliny[i].Start.Minutes:00}</p>
                        <hr/>
                    </section>
                    </div>");
				}
			}
		}

		private static void CreateEndPrihlasky()
		{
			PrihlaskySb.Append(@" </td>
        </tr>
        </tbody>
        <tfoot id='footer'>
        <tr>
            <td>
                <!--place holder for the fixed-position footer-->
                <div class='page-footer-space'></div>
            </td>
        </tr>
        </tfoot>
    </table>
</div>
</body>
</html>");
		}

		#endregion

		#region Casovy_Porad

		public static async Task CreateSaveOpenCasovyPorad(List<DisciplineModel> listDiscipliny)
		{
			RaceFv race = await RaceProtoExtension.GetActiveRace();
			CasovyPoradSb.Clear();
			CreateUvod(race, "Časový pořad", PrintMode.TimeTable);
			CreateTablesCasovyPorad(listDiscipliny);
			await UlozeniAOtevreniHtmlStranky("casovyPorad", CasovyPoradSb);
		}

		private static void CreateTablesCasovyPorad(List<DisciplineModel> listDiscipliny)
		{
			var orderedDisciplines = listDiscipliny.OrderBy(x => x.Start).ToList();
			List<DisciplineModel> disciplines = new List<DisciplineModel>();
			for (int i = 0; i < orderedDisciplines.Count; i++)
			{
				if (Extensions.IsRun(orderedDisciplines[i].DisciplineType) && orderedDisciplines[i].Order == 1)
					disciplines.Add(orderedDisciplines[i]);
				else if (Extensions.IsRun(orderedDisciplines[i].DisciplineType) == false)
					disciplines.Add(orderedDisciplines[i]);
			}

			StringBuilder tabulkaText = new StringBuilder();
			for (int i = 0; i < disciplines.Count; i++)
			{
				if (i > 0)
				{
					if (disciplines[i - 1].Start != disciplines[i].Start)
					{
						tabulkaText.Append($@"  <tr>
                                            <th>{disciplines[i].Start.Hours}:{disciplines[i].Start.Minutes:00}</th>
                                            <th>{disciplines[i].Name} - {disciplines[i].Category}</th>
                                        </tr> ");
					}
					else
					{
						tabulkaText.Append($@"  <tr>
                                            <th></th>
                                            <th>{disciplines[i].Name} - {disciplines[i].Category}</th>
                                        </tr> ");
					}
				}
				else
				{
					tabulkaText.Append($@"  <tr>
                                            <th>{disciplines[i].Start.Hours}:{disciplines[i].Start.Minutes:00}</th>
                                            <th>{disciplines[i].Name} - {disciplines[i].Category}</th>
                                        </tr> ");
				}
			}

			CasovyPoradSb.Append($@"
                <tr>
            <td class='page'>
        <div>
            <section>
            <table class='table table-hover table-bordered'>
                <thead>
                <tr>
                    <th style='width: 8rem; '>Čas startu</th>
                    <th>Disciplína</th>
                </tr>
                </thead>
                <tbody>
                {tabulkaText}
                </tbody>
            </table>
            </section>
        </div>
            </td>
        </tr>
    </body>
</html>");
		}

		#endregion

		#region Declarations

		private static readonly StringBuilder VysledkySb = new StringBuilder();
		private static readonly StringBuilder PrihlaskySb = new StringBuilder();
		private static readonly StringBuilder CasovyPoradSb = new StringBuilder();
		private static readonly StringBuilder TotalResults = new StringBuilder();

		#endregion

		#region Total Results

		public static async Task CreateSaveOpenAllResults(IList<ClubPoints> clubPoints)
		{
			RaceFv race = await RaceProtoExtension.GetActiveRace();
			TotalResults.Clear();
			CreateUvod(race, "Celkové výsledky", PrintMode.TotalResults);
			CreateTablesAllResults(clubPoints);
			await UlozeniAOtevreniHtmlStranky("totalresults", TotalResults);
		}

		private static void CreateTablesAllResults(IList<ClubPoints> clubPoints)
		{
			TotalResults.Append(@"<div>
                    <table class='table table-hover'>
                        <thead>
                            <tr>
                                <th class='td-uzky' style='width: 5rem;'>Umís.</th>
                                <th>Oddíl</th>
                                <th>Body</th>
                            </tr>
                        </thead>
                        <tbody>");
			for (int i = 0; i < clubPoints.Count; i++)
			{
				TotalResults.Append($@"<tr>
                                        <td class='td-uzky'>{clubPoints[i].Order}</td>
                                        <td>{clubPoints[i].ClubName}</td>
                                        <td>{clubPoints[i].Points}</td>
                                    </tr>");
			}

			TotalResults.Append(@"                        </tbody>
                    </table>
                </div>
        </table>
    </div>
</body>
</html>");
		}

		#endregion

		private static async Task UlozeniAOtevreniHtmlStranky(string file, StringBuilder textSb)
		{
			string filePath = $@"{AppDomain.CurrentDomain.BaseDirectory}{file}.html";

			await using (StreamWriter sw = new StreamWriter(filePath))
			{
				await sw.WriteLineAsync(textSb.ToString());
				await sw.FlushAsync();
			}

			string argument = "/select, \"" + filePath;
			ChromeLauncher.OpenLink(argument);
		}

		#region UpdateDisciplinesPrintIcon

		private static async Task UpdateDisciplinePrintIcon(List<DisciplineModel> disciplines)
		{
			await ScheduleProtoExtension.UpdateListDisciplines(disciplines);
		}

		#endregion
	}
}
