﻿namespace AtletickaKancelar.WPF.Infrastructure
{
	public enum PrintMode
	{
		Registrations,
		Results,
		TimeTable,
		TotalResults
	}
}
