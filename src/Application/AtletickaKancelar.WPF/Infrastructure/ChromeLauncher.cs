﻿using System.Diagnostics;
using Microsoft.Win32;

namespace AtletickaKancelar.WPF.Infrastructure
{
	internal static class ChromeLauncher
	{
		private const string ChromeAppKey = @"\Software\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe";

		private static string? ChromeAppFileName
		{
			get
			{
				var chrome = Registry.GetValue("HKEY_LOCAL_MACHINE" + ChromeAppKey, "", null);
				if (chrome == null)
					chrome = Registry.GetValue("HKEY_CURRENT_USER" + ChromeAppKey, "", null);
				return chrome as string;
			}
		}

		public static void OpenLink(string url)
		{
			var chromeAppFileName = ChromeAppFileName;
			if (string.IsNullOrEmpty(chromeAppFileName))
			{
				try
				{
					Process.Start(new ProcessStartInfo(url) { UseShellExecute = true });
				}
				catch
				{
					// ignored
				}
			}
			else
			{
				Process.Start(chromeAppFileName, url);
			}
		}
	}
}
