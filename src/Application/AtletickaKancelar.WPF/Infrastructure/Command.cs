﻿using System;
using System.Windows.Input;

namespace AtletickaKancelar.WPF.Infrastructure
{
	public class Command : ICommand
	{
		private readonly Action _execute;
		private readonly Func<bool> _canExecute;


		public Command(Action execute, Func<bool>? canExecute = null)
		{
			_execute = execute ?? throw new ArgumentNullException(nameof(execute));
			_canExecute = canExecute ?? (() => true);
		}

		public bool CanExecute(object? parameter)
		{
			return _canExecute();
		}

		public void Execute(object? parameter)
		{
			_execute();
		}

		public event EventHandler? CanExecuteChanged
		{
			add => CommandManager.RequerySuggested += value;
			remove => CommandManager.RequerySuggested -= value;
		}
	}

	// ReSharper disable once UnusedTypeParameter
	public class Command<T> : ICommand
	{
		private readonly Action<object> _execute;
		private readonly Func<object, bool> _canExecute;

		public Command(Action<object> execute) : this(execute, x => true)
		{
		}

		public Command(Action<object> execute, Func<object, bool> canExecute)
		{
			_execute = execute ?? throw new ArgumentNullException(nameof(execute));
			_canExecute = canExecute;
		}

		public bool CanExecute(object? parameter)
		{
			return parameter != null && _canExecute(parameter);
		}

		public void Execute(object? parameter)
		{
			if (parameter != null)
				_execute(parameter);
		}

		public event EventHandler? CanExecuteChanged
		{
			add => CommandManager.RequerySuggested += value;
			remove => CommandManager.RequerySuggested -= value;
		}
	}
}
