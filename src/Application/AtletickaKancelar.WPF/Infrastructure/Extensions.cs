﻿using System.Windows.Data;
using AtletickaKancelar.Domain.Enums;

namespace AtletickaKancelar.WPF.Infrastructure
{
	public static class Extensions
	{
		public static bool IsRun(DisciplineType disciplineType)
		{
			return disciplineType switch
			{
				DisciplineType.Sprint => true,
				DisciplineType.MidRun => true,
				DisciplineType.LongRun => true,
				DisciplineType.Relay => true,
				_ => false
			};
		}

		private static object? GetPropertyValue(string propertyName, object? dataItem)
		{
			if (dataItem == null)
				return null;
			object? data = "";

			if (propertyName.Contains('.'))
			{
				string[] propertyNames = propertyName.Split('.');
				// https://stackoverflow.com/questions/43651585/set-variables-from-an-object-using-reflection-doesnt-work
				for (var i = 0; i < propertyNames.Length - 1; i++)
				{
					dataItem = dataItem.GetType().GetProperty(propertyNames[i])?.GetValue(dataItem, null);
					data = dataItem!.GetType().GetProperty(propertyNames[i + 1])?.GetValue(dataItem, null);
				}
			}
			else
			{
				data = dataItem.GetType().GetProperty(propertyName)?.GetValue(dataItem, null);
			}

			return data;
		}

		// https://stackoverflow.com/questions/10342715/validationrule-with-validationstep-updatedvalue-is-called-with-bindingexpressi
		public static object GetBoundValue(object value)
		{
			if (value is BindingExpression binding)
			{
				object dataItem = binding.DataItem;
				string propertyName = binding.ParentBinding.Path.Path;

				var data = GetPropertyValue(propertyName, dataItem);
				return data ?? "";
			}
			else
			{
				return value;
			}
		}
	}
}
