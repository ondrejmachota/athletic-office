﻿namespace AtletickaKancelar.WPF.Infrastructure.Score
{
	public static class ScoreConstants
	{
		// 50 m, 400 m, Dalka, Hod
		internal static readonly double[] A = {58.015, 1.34, 0.34354, 12.33};
		internal static readonly double[] B = {12.5, 120, 180, 5};
		internal static readonly double[] C = {1.62, 1.62, 1.4, 1.1};
		internal static readonly double[] ResultGreater = {6, 60, 180, 5};
		internal static readonly double[] ResultLower = {12.5, 120, 600, 100};
	}
}
