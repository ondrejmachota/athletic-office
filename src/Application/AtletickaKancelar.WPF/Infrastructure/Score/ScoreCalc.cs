﻿using System;
using System.Collections.Generic;
using System.Globalization;
using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.Domain.Enums;
using AtletickaKancelar.WPF.Models;

namespace AtletickaKancelar.WPF.Infrastructure.Score
{
	public class ScoreCalc
	{
		public void CalculatePripravka(Discipline discipline, List<SpojkaMainModel> competitors)
		{
			switch (discipline.DisciplineType)
			{
				case DisciplineType.Sprint: // 50 m
					CalculatePripravkaRun(DisciplineType.Sprint, competitors);
					break;
				case DisciplineType.MidRun: // 400 m
					CalculatePripravkaRun(DisciplineType.MidRun, competitors);
					break;
				case DisciplineType.Throw: // Micek
					CalculatePripravkaTech(DisciplineType.Throw, competitors);
					break;
				case DisciplineType.LongJump: // Dalka
					CalculatePripravkaTech(DisciplineType.LongJump, competitors);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void CalculatePripravkaRun(DisciplineType disciplineType, List<SpojkaMainModel> competitors)
		{
			var index = disciplineType == DisciplineType.Sprint ? 0 : 1;

			double poinst;

			for (var i = 0; i < competitors.Count; i++)
			{
				if (competitors[i].Spojka.Result.HasValue)
				{
					var result = double.Parse(competitors[i].Spojka.Result.ToString()!);
					if (result > ScoreConstants.ResultGreater[index] || result < ScoreConstants.ResultLower[index])
						poinst = Math.Floor(ScoreConstants.A[index] * Math.Pow(ScoreConstants.B[index] - result, ScoreConstants.C[index]));
					else
						poinst = 0;
				}
				else
					poinst = 0;

				competitors[i].Spojka.Points = float.Parse(poinst.ToString(CultureInfo.InvariantCulture));
			}
		}

		private void CalculatePripravkaTech(DisciplineType disciplineType, List<SpojkaMainModel> competitors)
		{
			var index = disciplineType == DisciplineType.LongJump ? 2 : 3;

			double poinst;

			for (var i = 0; i < competitors.Count; i++)
			{
				if (competitors[i].Spojka.Result.HasValue)
				{
					var result = double.Parse(competitors[i].Spojka.Result.ToString()!);
					if (result > ScoreConstants.ResultGreater[index] || result < ScoreConstants.ResultLower[index])
						poinst = Math.Floor(ScoreConstants.A[index] * Math.Pow(result - ScoreConstants.B[index], ScoreConstants.C[index]));
					else
						poinst = 0;
				}
				else
					poinst = 0;

				competitors[i].Spojka.Points = float.Parse(poinst.ToString(CultureInfo.InvariantCulture));
			}
		}
	}
}
