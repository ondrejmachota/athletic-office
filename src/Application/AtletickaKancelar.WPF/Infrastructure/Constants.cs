﻿using System;
using System.Collections.Generic;
using System.IO;
using AtletickaKancelar.Domain.Enums;

namespace AtletickaKancelar.WPF.Infrastructure
{
	public static class Constants
	{
		// "Koule 2kg", "Koule 3kg", "Koule 4kg", "Koule 5kg", "Koule 6kg", "Koule 7.26", "Oštěp", "Disk 0.75kg", "Disk 1kg", "Disk 1.5kg", "Disk 1.75kg", "Disk 2kg",

        public static readonly IList<DisciplineConstant> DisciplineConstants = new List<DisciplineConstant>()
        {
            new DisciplineConstant("50 m", DisciplineType.Sprint),
            new DisciplineConstant("60 m", DisciplineType.Sprint),
            new DisciplineConstant("100 m", DisciplineType.Sprint),
            new DisciplineConstant("150 m", DisciplineType.MidRun),
            new DisciplineConstant("200 m", DisciplineType.MidRun),
            new DisciplineConstant("300 m", DisciplineType.MidRun),
            new DisciplineConstant("400 m", DisciplineType.MidRun),
            new DisciplineConstant("800 m", DisciplineType.LongRun),
            new DisciplineConstant("1500 m", DisciplineType.LongRun),
            new DisciplineConstant("3000 m", DisciplineType.LongRun),
            new DisciplineConstant("50 m př.", DisciplineType.Sprint),
            new DisciplineConstant("60 m př.", DisciplineType.Sprint),
            new DisciplineConstant("100 m př.", DisciplineType.Sprint),
            new DisciplineConstant("110 m př.", DisciplineType.Sprint),
            new DisciplineConstant("200 m př.", DisciplineType.MidRun),
            new DisciplineConstant("300 m př.", DisciplineType.MidRun),
            new DisciplineConstant("400 m př.", DisciplineType.MidRun),
            new DisciplineConstant("Skok daleký", DisciplineType.LongJump),
            new DisciplineConstant("Trojskok", DisciplineType.LongJump),
            new DisciplineConstant("Skok vysový", DisciplineType.HighJump),
            new DisciplineConstant("Skok o tyči", DisciplineType.HighJump),
            new DisciplineConstant("Koule 2kg", DisciplineType.Throw),
            new DisciplineConstant("Koule 3kg", DisciplineType.Throw),
            new DisciplineConstant("Koule 3kg", DisciplineType.Throw),
            new DisciplineConstant("Koule 4kg", DisciplineType.Throw),
            new DisciplineConstant("Koule 5kg", DisciplineType.Throw),
            new DisciplineConstant("Koule 6kg", DisciplineType.Throw),
            new DisciplineConstant("Koule 7.26kg", DisciplineType.Throw),
            new DisciplineConstant("Oštěp", DisciplineType.Throw),
            new DisciplineConstant("Míček", DisciplineType.Throw),
            new DisciplineConstant("Disk", DisciplineType.Throw),
            new DisciplineConstant("4x60m", DisciplineType.Relay),
            new DisciplineConstant("4x100m", DisciplineType.Relay),
            new DisciplineConstant("4x300m", DisciplineType.Relay),
            new DisciplineConstant("4x400m", DisciplineType.Relay),
        };

        public static readonly IList<string> CategoryConstants = new List<string>()
        {
            "Přípravka kluci",
            "Přípravka dívky",
            "Ml. žáci",
            "Ml. žákyně",
            "St. žáci",
            "St. žákyně",
            "Dorostenci",
            "Dorostenky",
            "Junioři",
            "Juniorky",
            "Muži",
            "Ženy",
            "Mix",
        };

        // A directory where the application is installed
        public static readonly string DefaultPath = $"{AppDomain.CurrentDomain.BaseDirectory}";

        // A directory where the setting file and the camera files are saved
        public static readonly string WorkingDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),"Ondřej Machota", "AtletickaKancelar");
	}
}
