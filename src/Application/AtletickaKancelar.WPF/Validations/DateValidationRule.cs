﻿using System;
using System.Globalization;
using System.Windows.Controls;
using AtletickaKancelar.WPF.Infrastructure;

namespace AtletickaKancelar.WPF.Validations
{
    public class DateValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var text = Extensions.GetBoundValue(value);
            var date = text.ToString();
            return DateTime.TryParse(date ?? "", CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out _) ? ValidationResult.ValidResult
                : new ValidationResult(false, "Neplatné datum!");
        }
    }
}
