﻿using System.Globalization;
using System.Windows.Controls;
using AtletickaKancelar.WPF.Infrastructure;

namespace AtletickaKancelar.WPF.Validations
{
	public class NotEmptyValidationRule : ValidationRule
	{
		public override ValidationResult Validate(object value, CultureInfo cultureInfo)
		{
			var text = Extensions.GetBoundValue(value) as string;
			return string.IsNullOrWhiteSpace(text ?? "")
				? new ValidationResult(false, "Pole nesmí být prázdné.")
				: ValidationResult.ValidResult;
		}
		
		// https://stackoverflow.com/questions/10342715/validationrule-with-validationstep-updatedvalue-is-called-with-bindingexpressi
	}
}