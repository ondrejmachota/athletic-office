﻿using System;
using System.Globalization;
using System.Windows.Controls;
using AtletickaKancelar.WPF.Infrastructure;

namespace AtletickaKancelar.WPF.Validations
{
    public class SimpleDateValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var date = Extensions.GetBoundValue(value);
            var dateText = date.ToString();
            return DateTime.TryParse(dateText ?? "",
                CultureInfo.CurrentCulture,
                DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces,
                out _)
                ? ValidationResult.ValidResult
                : new ValidationResult(false, "Neplatný čas");
        }
    }
}
