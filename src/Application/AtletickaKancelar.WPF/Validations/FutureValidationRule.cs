﻿using System;
using System.Globalization;
using System.Windows.Controls;
using AtletickaKancelar.WPF.Infrastructure;

namespace AtletickaKancelar.WPF.Validations
{
	public class FutureValidationRule : ValidationRule
	{
		public override ValidationResult Validate(object value, CultureInfo cultureInfo)
		{
			var dateObject = Extensions.GetBoundValue(value);
			var dateText = dateObject.ToString();
			if (!DateTime.TryParse(dateText ?? "",
				CultureInfo.CurrentCulture,
				DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces,
				out var date)) 
				return new ValidationResult(false, "Neplatné datum");
			return date.Date <= DateTime.Now.Date
				? new ValidationResult(false, "Future date required")
				: ValidationResult.ValidResult;
		}
	}
}
