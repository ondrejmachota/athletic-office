﻿using System;
using AtletickaKancelar.Domain.Enums;
using DynamicData.Binding;

namespace AtletickaKancelar.WPF.Models
{
	public class CompetitorMainModel : AbstractNotifyPropertyChanged
	{
		private bool _isMan;
		private bool _isSelected;

		#region Constructor

		public CompetitorMainModel(CompetitorModel competitorModel)
		{
			Competitor = competitorModel;
		}

		public static CompetitorMainModel CreateDefault()
		{
			return new CompetitorMainModel(new CompetitorModel(0, 0, "", "", Sex.Man, DateTime.Today, "", ""));
		}

		public static CompetitorMainModel Copy(CompetitorModel competitorModel)
		{
			return new CompetitorMainModel(new CompetitorModel(competitorModel.Id, competitorModel.ClubId, competitorModel.FirstName,
				competitorModel.LastName, competitorModel.Sex, competitorModel.BirthDate, competitorModel.ClubName, competitorModel.ClubAbbreviation));
		}

		#endregion

		public CompetitorModel Competitor { get; set; }

		public string FullName => string.Format($"{Competitor.FirstName} {Competitor.LastName}");

		public string VypisInformaci => string.Format($"{Competitor.ClubAbbreviation}            {Competitor.BirthDate.ToShortDateString()}");

		public bool IsMan
		{
			get => _isMan;
			set { SetAndRaise(ref _isMan, value); }
		}

		public bool IsSelected
		{
			get => _isSelected;
			set => SetAndRaise(ref _isSelected, value);
		}
	}
}
