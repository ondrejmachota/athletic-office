﻿using System;
using AtletickaKancelar.Domain.Enums;
using DynamicData.Binding;
using GrpcServer;

namespace AtletickaKancelar.WPF.Models
{
	public class CompetitorModel : AbstractNotifyPropertyChanged
	{
		private string _firstName;
		private string _lastName;
		private Sex _sex;
		private DateTime _birthDate;
		private string _clubName;
		private string _clubAbbreviation;

		public CompetitorModel(int id, int clubId, string firstName, string lastName, Sex sex, DateTime birthDate, string clubName,
		                       string clubAbbreviation)
		{
			Id = id;
			ClubId = clubId;
			_firstName = firstName;
			_lastName = lastName;
			_sex = sex;
			_birthDate = birthDate;
			_clubName = clubName;
			_clubAbbreviation = clubAbbreviation;
		}

		public CompetitorModel(CompetitorDto competitorDto)
		{
			Id = competitorDto.Id;
			ClubId = competitorDto.ClubId;
			_firstName = competitorDto.FirstName;
			_lastName = competitorDto.LastName;
			_sex = (Sex) competitorDto.Sex;
			_birthDate = competitorDto.BirthDate.ToDateTime();
			_clubName = competitorDto.ClubName;
			_clubAbbreviation = competitorDto.ClubAbbreviation;
		}

		public int Id { get; }

		public int ClubId { get; set; }

		public string FirstName
		{
			get => _firstName;
			set => SetAndRaise(ref _firstName, value);
		}

		public string LastName
		{
			get => _lastName;
			set => SetAndRaise(ref _lastName, value);
		}

		public Sex Sex
		{
			get => _sex;
			set => SetAndRaise(ref _sex, value);
		}

		public DateTime BirthDate
		{
			get => _birthDate;
			set => SetAndRaise(ref _birthDate, value);
		}

		public string ClubName
		{
			get => _clubName;
			set => SetAndRaise(ref _clubName, value);
		}

		public string ClubAbbreviation
		{
			get => _clubAbbreviation;
			set => SetAndRaise(ref _clubAbbreviation, value);
		}
	}
}