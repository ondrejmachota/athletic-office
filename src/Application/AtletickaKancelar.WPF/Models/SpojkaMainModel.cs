﻿using AtletickaKancelar.Domain.Enums;
using DynamicData.Binding;

namespace AtletickaKancelar.WPF.Models
{
	public class SpojkaMainModel : AbstractNotifyPropertyChanged
	{
		private int _penalization;
		private bool _isSelected;
		
		public SpojkaModel Spojka { get; set; }
		
		public bool GetsScore { get; set; }
		
		public SpojkaMainModel(SpojkaModel spojka)
		{
			Spojka = spojka;
			Penalization = (int)spojka.PenalizationType;
		}

		public int Penalization
		{
			get => _penalization;
			set
			{
				if (_penalization == value)
					return;
				_penalization = value;
				Spojka.PenalizationType = (PenalizationType)_penalization;
				OnPropertyChanged();
			}
		}

		public bool IsSelected
		{
			get => _isSelected;
			set => SetAndRaise(ref _isSelected, value);
		}
	}
}
