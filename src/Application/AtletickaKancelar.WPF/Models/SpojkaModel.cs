﻿using AtletickaKancelar.Domain.Enums;
using DynamicData.Binding;
using GrpcServer;

namespace AtletickaKancelar.WPF.Models
{
	public class SpojkaModel : AbstractNotifyPropertyChanged
	{
		private int _disciplineId;
		private int? _track;
		private int? _bib;
		private float? _result;
		private int? _order;
		private int? _secondOrder;
		private float? _points;
		private PenalizationType _penalizationtype;
		private int? _mainId;

		public SpojkaModel(SpojkaDto spojkaDto)
		{
			
			Id = spojkaDto.Id;
			CompetitorId = spojkaDto.CompetitorId;
			_disciplineId = spojkaDto.DisciplineId;
			_track = spojkaDto.Track;
			_bib = spojkaDto.BIB;
			_result = spojkaDto.Result;
			_order = spojkaDto.Order;
			_secondOrder = spojkaDto.SecondOrder;
			_points = spojkaDto.Points;
			_penalizationtype = (PenalizationType)spojkaDto.PenalizationType;
			_mainId = spojkaDto.MainId;
			ClubId = spojkaDto.ClubId;
			FullName = spojkaDto.FullName;
			ClubAbbreviation = spojkaDto.ClubAbbreviation;
			BirthDate = spojkaDto.BirthDate;
		}

		/// <summary>
		/// Used only for unit testing
		/// </summary>
		public SpojkaModel(float? result, int? order, int? secondOrder, float? points, PenalizationType penalizationtype)
		{
			_result = result;
			_order = order;
			_secondOrder = secondOrder;
			_points = points;
			_penalizationtype = penalizationtype;
			FullName = "";
			ClubAbbreviation = "";
			BirthDate = "";
		}

		public int Id { get; set; }

		public int CompetitorId { get; set; }

		public int DisciplineId
		{
			get => _disciplineId;
			set => SetAndRaise(ref _disciplineId, value);
		}

		public int? Track
		{
			get => _track;
			set => SetAndRaise(ref _track, value);
		}

		public int? BIB
		{
			get => _bib;
			set => SetAndRaise(ref _bib, value);
		}

		public float? Result
		{
			get => _result;
			set => SetAndRaise(ref _result, value);
		}

		public int? Order
		{
			get => _order;
			set => SetAndRaise(ref _order, value);
		}

		public int? SecondOrder
		{
			get => _secondOrder;
			set => SetAndRaise(ref _secondOrder, value);
		}

		public float? Points
		{
			get => _points;
			set => SetAndRaise(ref _points, value);
		}

		public PenalizationType PenalizationType
		{
			get => _penalizationtype;
			set => SetAndRaise(ref _penalizationtype, value);
		}

		public int? MainId
		{
			get => _mainId;
			set=> SetAndRaise(ref _mainId, value);
		}

		public int ClubId { get; set; }
		public string FullName { get; set; }
		public string ClubAbbreviation { get; set; }
		public string BirthDate { get; set; }
	}
}
