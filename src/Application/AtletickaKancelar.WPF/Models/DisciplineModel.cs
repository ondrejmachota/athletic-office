﻿using System;
using AtletickaKancelar.Domain.Enums;
using DynamicData.Binding;
using GrpcServer;

namespace AtletickaKancelar.WPF.Models
{
	public class DisciplineModel : AbstractNotifyPropertyChanged
	{
		private string _name;
		private string _category;
		private DisciplineType _disciplineType;
		private PhaseType _phaseType;
		private TimeSpan _start;
		private int? _cameraId;
		private int? _order;

		public DisciplineModel(DisciplineDto disciplineDto)
		{
			Id = disciplineDto.Id;
			RaceId = disciplineDto.RaceId;
			_name = disciplineDto.Name;
			_category = disciplineDto.Category;
			_disciplineType = (DisciplineType)disciplineDto.DisciplineType;
			_phaseType = (PhaseType)disciplineDto.PhaseType;
			MainId = disciplineDto.MainId;
			_start = disciplineDto.Start.ToTimeSpan();
			_cameraId = disciplineDto.CameraId;
			_order = disciplineDto.Order;
			IsRegistrationsPrinted = disciplineDto.IsRegistrationsPrinted;
			IsResultsPrinted = disciplineDto.IsResultsPrinted;
			CompetitorsCount = disciplineDto.CompetitorsCount;
		}

		public DisciplineModel(string name, string category, DisciplineType disciplineType, PhaseType disciplinePhase, TimeSpan start, int? order)
		{
			Id = 0;
			RaceId = 0;
			_name = name;
			_category = category;
			_disciplineType = disciplineType;
			_phaseType = disciplinePhase;
			MainId = null;
			_start = start;
			_cameraId = null;
			_order = order;
			IsRegistrationsPrinted = false;
			IsResultsPrinted = false;
			CompetitorsCount = 0;
		}

		public DisciplineModel(DisciplineModel discipline)
		{
			Id = discipline.Id;
			RaceId = discipline.RaceId;
			_name = discipline.Name;
			_category = discipline.Category;
			_disciplineType = discipline.DisciplineType;
			_phaseType = discipline.PhaseType;
			MainId = discipline.MainId;
			_start = discipline.Start;
			_cameraId = discipline.CameraId;
			_order = discipline.Order;
			IsRegistrationsPrinted = discipline.IsRegistrationsPrinted;
			IsResultsPrinted = discipline.IsResultsPrinted;
			CompetitorsCount = discipline.CompetitorsCount;
		}

		public int Id { get;  }

		public int RaceId { get;  }

		public string Name
		{
			get => _name;
			set => SetAndRaise(ref _name, value);
		}

		public string Category
		{
			get => _category;
			set => SetAndRaise(ref _category, value);
		}

		public DisciplineType DisciplineType
		{
			get => _disciplineType;
			set => SetAndRaise(ref _disciplineType, value);
		}

		public PhaseType PhaseType
		{
			get => _phaseType;
			set => SetAndRaise(ref _phaseType, value);
		}

		public int? MainId { get; set; }

		public TimeSpan Start
		{
			get => _start;
			set => SetAndRaise(ref _start, value);
		}

		public int? CameraId
		{
			get => _cameraId;
			set => SetAndRaise(ref _cameraId, value);
		}

		public int? Order
		{
			get => _order;
			set => SetAndRaise(ref _order, value);
		}

		public bool IsRegistrationsPrinted { get; set; }

		public bool IsResultsPrinted { get; set; }

		public int CompetitorsCount { get; }
	}
}
