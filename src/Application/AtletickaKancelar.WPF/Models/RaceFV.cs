﻿using System;
using DynamicData.Binding;
using GrpcServer;

namespace AtletickaKancelar.WPF.Models
{
	public class RaceFv : AbstractNotifyPropertyChanged
	{
		private string _title;
		private bool _isActive;
		private string _place;
		private DateTime _date;
		private string _note;

		public RaceFv(RaceDto raceDto)
		{
			Id = raceDto.Id;
			_title = raceDto.Title;
			_isActive = raceDto.IsActive;
			_place = raceDto.Place;
			_date = raceDto.Date.ToDateTime();
			_note = raceDto.Note;
		}

		public RaceFv(string title, bool isActive, string place, DateTime date, string note)
		{
			_title = title;
			_isActive = isActive;
			_place = place;
			_date = date;
			_note = note;
		}

		public RaceFv(RaceFv raceFv)
		{
			Id = raceFv.Id;
			_title = raceFv.Title;
			_isActive = raceFv.IsActive;
			_place = raceFv.Place;
			_date = raceFv.Date;
			_note = raceFv.Note;
		}

		public static RaceFv CreateDefault()
		{
			return new RaceFv("", false, "", DateTime.Today.AddDays(1), "");
		}

		public int Id { get; set; }

		public string Title
		{
			get => _title;
			set => SetAndRaise(ref _title, value);
		}

		public bool IsActive
		{
			get => _isActive;
			set => SetAndRaise(ref _isActive, value);
		}

		public string Place
		{
			get => _place;
			set => SetAndRaise(ref _place, value);
		}

		public DateTime Date
		{
			get => _date;
			set => SetAndRaise(ref _date, value);
		}

		public string Note
		{
			get => _note;
			set => SetAndRaise(ref _note, value);
		}
	}
}
