﻿using System;
using System.Windows.Media;
using AtletickaKancelar.WPF.Infrastructure;
using DynamicData.Binding;
using Google.Protobuf.WellKnownTypes;
using GrpcServer;

namespace AtletickaKancelar.WPF.Models
{
	public class DisciplineMainModel : AbstractNotifyPropertyChanged
	{
		#region Constructor

		public DisciplineMainModel(DisciplineModel discipline)
		{
			Discipline = discipline;
			ForegroundColorPrihl = discipline.IsRegistrationsPrinted ? Brushes.Green : Brushes.Red;
			ForegroundColorVysl = discipline.IsResultsPrinted ? Brushes.Green : Brushes.Red;
		}

		public static DisciplineMainModel CreateDefault()
		{
			return new DisciplineMainModel(new DisciplineModel(new DisciplineDto
			{
				Id = 0,
				Name = Constants.DisciplineConstants[0].Name,
				Category = Constants.CategoryConstants[0],
				CameraId = null,
				CompetitorsCount = 0,
				DisciplineType = 0,
				IsRegistrationsPrinted = false,
				IsResultsPrinted = false,
				MainId = null,
				Order = null,
				RaceId = 0,
				Start = Duration.FromTimeSpan(new TimeSpan(DateTime.Now.Hour + 2, DateTime.Now.Minute, 0)),
				PhaseType = 0,
			}));
		}

		#endregion

		#region Private properties

		private Brush _foregroundColorPrihl = null!;
		private Brush _foregroundColorVysl = null!;
		private bool _isSelected;
		private DateTime _startTime;

		#endregion

		#region Public properties

		public DisciplineModel Discipline { get; set; }

		public DateTime StartTime
		{
			get => _startTime;
			set => SetAndRaise(ref _startTime, value);
		}

		public string StartTimeString => string.Format($"{Discipline.Start.Hours}:{Discipline.Start.Minutes:00}");

		public bool IsRun => Extensions.IsRun(Discipline.DisciplineType);

		public string DisciplineInfo =>
			IsRun
				? $"{Discipline.Category}, {Discipline.PhaseType}, {StartTimeString}, Běh: {Discipline.Order}"
				: $"{Discipline.Category}, {Discipline.PhaseType}, {StartTimeString}";

		public bool IsSelected
		{
			get => _isSelected;
			set => SetAndRaise(ref _isSelected, value);
		}

		public Brush ForegroundColorPrihl
		{
			get => _foregroundColorPrihl;
			set => SetAndRaise(ref _foregroundColorPrihl, value);
		}

		public Brush ForegroundColorVysl
		{
			get => _foregroundColorVysl;
			set => SetAndRaise(ref _foregroundColorVysl, value);
		}

		#endregion
	}
}
