﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using AtletickaKancelar.WPF.Converters;
using AtletickaKancelar.WPF.Infrastructure;
using AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions;
using AtletickaKancelar.WPF.Models;
using AtletickaKancelar.WPF.Utilities;
using AtletickaKancelar.WPF.Views.Shared;
using AtletickaKancelar.WPF.Views.WindowManagement;
using GrpcServer;

namespace AtletickaKancelar.WPF.Views.RaceWindow
{
	public class RaceViewModel : ViewModelBase
	{
		private bool _isDialogHostOpen;
		private string _searchText = "";
		private EditorMode _editorMode = EditorMode.Create;
		private RaceFv _selectedRace = null!;
		private RaceFv _selectedRaceCache = null!;
		private RaceFv _createRace;
		private ObservableCollection<RaceFv> _cachedRaces = new ObservableCollection<RaceFv>();
		private readonly RaceGrpcProtoService.RaceGrpcProtoServiceClient _client;

		public ObservableCollection<RaceFv> Races { get; private set; } = new ObservableCollection<RaceFv>();

		public ICommand ActiveRace { get; }
		public ICommand RaceCommand { get; }
		public ICommand DeleteRace { get; }
		public ICommand LoadCommand { get; }
		public ICommand OpenDialogHostCm { get; }
		public ICommand CloseDialogHost { get; }

		public RaceViewModel()
		{
#pragma warning disable 4014
			_client = RaceProtoExtension.Create();
			DialogSwitchViewModel = new DialogSwitchViewModel("Založit", "závod");
			_isDialogHostOpen = false;
			_createRace = RaceFv.CreateDefault();
			LoadCommand = new Command(() => GetRacesAsync());
			ActiveRace = new Command(() => Task.Run(() => SetRaceAsActive(SelectedRace)));
			RaceCommand = new Command(() =>
			{
				if (_editorMode == EditorMode.Create)
					CreateRaceAsync();
				else if (_editorMode == EditorMode.Edit)
					EditRaceAsync();
			});
			DeleteRace = new Command(() => DeleteRaceAsync());
			OpenDialogHostCm = new Command<EditorMode>(param =>
			{
				_editorMode = (EditorMode)param;
				if (_editorMode == EditorMode.Edit)
					_selectedRaceCache = new RaceFv(SelectedRace);
				else if (_editorMode == EditorMode.Create)
					_createRace = RaceFv.CreateDefault();
				DialogSwitchViewModel.SetValues(_editorMode);
				IsDialogHostOpen = true;
			});
			CloseDialogHost = new Command(() =>
			{
				IsDialogHostOpen = false;
				if (_editorMode == EditorMode.Edit)
				{
					var index = Races.IndexOf(SelectedRace);
					Races[index] = _selectedRaceCache;
				}
			});
#pragma warning restore 4014
		}

		public DialogSwitchViewModel DialogSwitchViewModel { get; set; }

		public string SearchText
		{
			get => _searchText;
			set
			{
				SetAndRaise(ref _searchText, value);
				Search();
			}
		}

		public bool IsDialogHostOpen
		{
			get => _isDialogHostOpen;
			set => SetAndRaise(ref _isDialogHostOpen, value);
		}

		public RaceFv SelectedRace
		{
			get => _selectedRace;
			set => SetAndRaise(ref _selectedRace, value);
		}

		public RaceFv CreateRace
		{
			get => _createRace;
			private set => SetAndRaise(ref _createRace, value);
		}

		private async Task GetRacesAsync()
		{
			using var call = _client.GetRaceStream(new EmptyRequest());
			await foreach (var race in RaceProtoExtension.DownloadRacesAsync(call))
			{
				Races.Add(race);
			}
		}

		private async Task CreateRaceAsync()
		{
			var race = RaceValidation.ValidateRace(CreateRace.Title, CreateRace.Place, CreateRace.Date, CreateRace.Note);
			if (race != null)
			{
				IsDialogHostOpen = false;
				CreateRace = RaceFv.CreateDefault();
				var newRace = await Task.Run(async () => await _client.CreateRaceAsync(race.ToRaceDto()));
				Races.Add(newRace.ToRaceFv());
			}
		}

		private void EditRaceAsync()
		{
			var race = RaceValidation.ValidateRace(SelectedRace.Title, SelectedRace.Place, DateTime.Today, SelectedRace.Note);
			// User can edit the race although if the date has passed
			if (race != null)
			{
				race.Id = SelectedRace.Id;
				race.Date = SelectedRace.Date;
				race.IsActive = SelectedRace.IsActive;
				IsDialogHostOpen = false;
				Task.Run(() => { _client.EditRaceAsync(race.ToRaceDto()); }).ConfigureAwait(false);
			}
		}

		private async Task SetRaceAsActive(RaceFv? race)
		{
			if (race != null)
			{
				using var call = _client.UpdateRaceStream(new UpdateRaceActivityModel { Id = race.Id });
				Races = await RaceProtoExtension.DownloadRaces(call);
				// TODO : don't refresh collection by downloading races again
				var directoryPath = Path.Combine(Constants.WorkingDirectory, race.Id.ToString());
				if (!Directory.Exists(directoryPath))
					Directory.CreateDirectory(directoryPath);
			}
		}

		private async Task DeleteRaceAsync()
		{
			var result = MessageBox.Show("Are you sure", "Caution", MessageBoxButton.OKCancel);
			if (result == MessageBoxResult.OK)
			{
				await _client.DeleteRaceAsync(SelectedRace.ToRaceDto());
				if (SelectedRace.IsActive)
				{
					await SetRaceAsActive(Races.IndexOf(SelectedRace) != 0 ? Races[0] : Races[1]);
					return;
				}

				// use emptyrequest to inform that the race was deleted successfully
				Races.Remove(SelectedRace);
				// TODO : show a message
			}
		}

		private bool _racesCoppied;

		private void Search()
		{
			if (SearchText.Length > 0)
			{
				if (!_racesCoppied)
				{
					_cachedRaces = new ObservableCollection<RaceFv>(Races);
					_racesCoppied = true;
				}

				Races = new ObservableCollection<RaceFv>(from race in _cachedRaces
				                                         where race.Title.Contains(SearchText) || race.Place.Contains(SearchText) ||
				                                               race.Date.ToShortDateString().Contains(SearchText)
				                                         select race);
			}
			else
			{
				Races = _cachedRaces;
				_racesCoppied = false;
			}
		}
	}
}
