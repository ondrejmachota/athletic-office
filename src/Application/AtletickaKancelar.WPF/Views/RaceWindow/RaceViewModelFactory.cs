﻿using AtletickaKancelar.WPF.Views.WindowManagement.Factories;

namespace AtletickaKancelar.WPF.Views.RaceWindow
{
	public class RaceViewModelFactory : IViewModelFactory<RaceViewModel>
	{
		public RaceViewModel CreateViewModel()
		{
			return new RaceViewModel();
		}
	}
}