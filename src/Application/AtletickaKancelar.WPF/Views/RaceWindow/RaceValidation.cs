﻿using System;
using AtletickaKancelar.WPF.Models;

namespace AtletickaKancelar.WPF.Views.RaceWindow
{
	public static class RaceValidation
	{
		public static RaceFv? ValidateRace(string title, string place, DateTime date, string note)
		{
			if (string.IsNullOrEmpty(title))
				return null;
			if (string.IsNullOrEmpty(place))
				return null;
			if (date.Date < DateTime.Now.Date)
				return null;
			return new RaceFv(title,false, place, date, note);
		}
	}
}
