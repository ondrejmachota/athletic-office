﻿﻿using System.Windows;
using System.Windows.Input;
using AtletickaKancelar.WPF.Infrastructure;
using AtletickaKancelar.WPF.Views.WindowManagement.Navigators;
using DynamicData.Binding;

namespace AtletickaKancelar.WPF.Views.WindowManagement
{
	public class MainWindowViewModel : AbstractNotifyPropertyChanged
	{
		private bool _isInfoOpen;

		public ICommand ExitApplication { get; set; }
		public ICommand OpenInfo { get; set; }
		public ICommand CloseInfo { get; set; }
		public INavigator Navigator { get; set; }

		public MainWindowViewModel(INavigator navigator)
		{
			Navigator = navigator;
			ExitApplication = new Command(() =>  Application.Current.Shutdown());
			OpenInfo = new Command(() => IsInfoOpen = true);
			CloseInfo = new Command(() => IsInfoOpen = false);
			Navigator.UpdateCurrentViewModelCommand.Execute(ViewType.Races); //TODO: on startup switch to window
		}

		public bool IsInfoOpen
		{
			get => _isInfoOpen;
			set => SetAndRaise(ref _isInfoOpen, value);
		}
	}
}