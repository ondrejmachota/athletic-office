﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using AtletickaKancelar.WPF.Views.WindowManagement.Factories;
using AtletickaKancelar.WPF.Views.WindowManagement.Navigators;

namespace AtletickaKancelar.WPF.Views.WindowManagement
{
	public class UpdateCurrentViewModelCommand : ICommand
	{
		public event EventHandler? CanExecuteChanged
		{
			add => CommandManager.RequerySuggested += value;
			remove => CommandManager.RequerySuggested -= value;
		}

	private readonly INavigator _navigator;
		private readonly IRootViewModelFactory _viewModelFactory;

		public UpdateCurrentViewModelCommand(INavigator navigator, IRootViewModelFactory viewModelFactory)
		{
			_navigator = navigator;
			_viewModelFactory = viewModelFactory;
		}

		public bool CanExecute(object? parameter)
		{
			return true;
		}

		public async void Execute(object? parameter)
		{
			if (parameter == null)
				return;
			ViewType viewType;
			if (parameter is object[] values)
			{
				viewType = (ViewType) values[0];
				var width = (double) values[1];
				if (width > 60)
				{
					await Task.Delay(305);
				}
			}
			else
			{
				viewType = (ViewType) parameter;
			}
			
			if (_navigator.CurrentWindowType != viewType)
			{
				_navigator.CurrentViewModel = _viewModelFactory.CreateViewModel(viewType);
				_navigator.CurrentWindowType = viewType;
			}
		}
	}
}
