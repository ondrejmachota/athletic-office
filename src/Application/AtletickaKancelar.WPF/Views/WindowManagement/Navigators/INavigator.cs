﻿using System.Windows.Input;

namespace AtletickaKancelar.WPF.Views.WindowManagement.Navigators
{
	public enum ViewType
	{
		None,
		Races,
		Disciplines,
		Registrations,
		Results,
		Settings
		
	}

	public interface INavigator
	{
		ViewModelBase CurrentViewModel { get; set; }

		ICommand UpdateCurrentViewModelCommand { get; }

		bool IsMenuEnabled { get; set; }

		ViewType CurrentWindowType { get; set; }
	}
}