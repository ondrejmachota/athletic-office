﻿using System.Windows.Input;
using AtletickaKancelar.WPF.Views.WindowManagement.Factories;
using DynamicData.Binding;

namespace AtletickaKancelar.WPF.Views.WindowManagement.Navigators
{
	public class Navigator : AbstractNotifyPropertyChanged, INavigator
	{
		private ViewModelBase _currentViewModel = null!;
		private bool _isMenuEnabled;
		private ViewType _currentWindowType;

		public Navigator(IRootViewModelFactory viewModelFactory)
		{
			UpdateCurrentViewModelCommand = new UpdateCurrentViewModelCommand(this, viewModelFactory);
		}

		public ViewModelBase CurrentViewModel
		{
			get => _currentViewModel;
			set => SetAndRaise(ref _currentViewModel, value);
		}

		public ICommand UpdateCurrentViewModelCommand { get; }

		public bool IsMenuEnabled
		{
			get => _isMenuEnabled;
			set => SetAndRaise(ref _isMenuEnabled, value);
		}

		public ViewType CurrentWindowType
		{
			get => _currentWindowType;
			set => SetAndRaise(ref _currentWindowType, value);
		}
	}
}
