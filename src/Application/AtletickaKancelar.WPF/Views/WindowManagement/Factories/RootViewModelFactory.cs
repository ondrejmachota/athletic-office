﻿using System;
using AtletickaKancelar.WPF.Views.RaceWindow;
using AtletickaKancelar.WPF.Views.RegistrationsWindow;
using AtletickaKancelar.WPF.Views.ResultsWindow;
using AtletickaKancelar.WPF.Views.ScheduleWindow;
using AtletickaKancelar.WPF.Views.SettingsWindow;
using AtletickaKancelar.WPF.Views.WindowManagement.Navigators;

namespace AtletickaKancelar.WPF.Views.WindowManagement.Factories
{
	public class RootViewModelFactory : IRootViewModelFactory
	{
		private readonly IViewModelFactory<RaceViewModel> _raceViewModelFactory;
		private readonly IViewModelFactory<ScheduleViewModel> _scheduleViewModelFactory;
		private readonly IViewModelFactory<RegistrationViewModel> _registrationViewModelFactory;
		private readonly IViewModelFactory<ResultViewModel> _resultViewModelFactory;
		private readonly IViewModelFactory<SettingsViewModel> _settingsviViewModelFactory;
		
		public RootViewModelFactory(IViewModelFactory<RaceViewModel> raceViewModelFactory,
		                            IViewModelFactory<ScheduleViewModel> scheduleViewModelFactory,
		                            IViewModelFactory<RegistrationViewModel> registrationViewModelFactory,
		                            IViewModelFactory<ResultViewModel> resultViewModelFactory,
		                            IViewModelFactory<SettingsViewModel> settingsviViewModelFactory)
		{
			_raceViewModelFactory = raceViewModelFactory;
			_scheduleViewModelFactory = scheduleViewModelFactory;
			_registrationViewModelFactory = registrationViewModelFactory;
			_resultViewModelFactory = resultViewModelFactory;
			_settingsviViewModelFactory = settingsviViewModelFactory;
		}
		
		public ViewModelBase CreateViewModel(ViewType viewType)
		{
			return viewType switch
			{
				ViewType.Races =>  _raceViewModelFactory.CreateViewModel(),
				ViewType.Disciplines => _scheduleViewModelFactory.CreateViewModel(),
				ViewType.Registrations => _registrationViewModelFactory.CreateViewModel(),
				ViewType.Results => _resultViewModelFactory.CreateViewModel(),
				ViewType.Settings => _settingsviViewModelFactory.CreateViewModel(),
				_ => throw new ArgumentException("Bad ViewModel")
			};
		}
	}
}