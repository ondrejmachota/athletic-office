﻿
namespace AtletickaKancelar.WPF.Views.WindowManagement.Factories
{
	public interface IViewModelFactory<T>
	{
		T CreateViewModel();
	}
}