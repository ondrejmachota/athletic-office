﻿using AtletickaKancelar.WPF.Views.WindowManagement.Navigators;

namespace AtletickaKancelar.WPF.Views.WindowManagement.Factories
{
	public interface IRootViewModelFactory
	{
		ViewModelBase CreateViewModel(ViewType viewType);
	}
}