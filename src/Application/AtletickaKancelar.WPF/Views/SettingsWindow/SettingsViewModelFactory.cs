﻿using AtletickaKancelar.Domain.Settings;
using AtletickaKancelar.WPF.Views.WindowManagement.Factories;

namespace AtletickaKancelar.WPF.Views.SettingsWindow
{
	public class SettingsViewModelFactory : IViewModelFactory<SettingsViewModel>
	{
		private readonly ISettingsStore _settingsStore;
		
		public SettingsViewModelFactory(ISettingsStore settingsStore)
		{
			_settingsStore = settingsStore;
		}

		public SettingsViewModel CreateViewModel()
		{
			return new SettingsViewModel(_settingsStore);
		}
	}
}