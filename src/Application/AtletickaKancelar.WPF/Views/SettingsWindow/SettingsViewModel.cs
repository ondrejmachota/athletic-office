﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Threading.Tasks;
using System.Windows.Input;
using AtletickaKancelar.Domain.Models;
using AtletickaKancelar.Domain.Settings;
using AtletickaKancelar.WPF.Infrastructure;
using AtletickaKancelar.WPF.Utilities;
using AtletickaKancelar.WPF.Views.WindowManagement;
using DynamicData.Binding;

namespace AtletickaKancelar.WPF.Views.SettingsWindow
{
	public class SettingsViewModel : ViewModelBase, IDisposable
	{
		private int _rovinka;
		private int _oval;
		private string _grpcServerAddress = null!;
		private bool _isDark;
		private readonly ISettingsStore _settingsStore;
		private readonly IDisposable _cleanUp;

		public ObservableCollection<int> RovinkaTrackCount { get; }
		public ObservableCollection<int> OvalTrackCount { get; }

		public ICommand ToggleBaseCommand { get; }

		public ICommand LoadCommand { get; }

		public SettingsViewModel(ISettingsStore settingsStore)
		{
			_settingsStore = settingsStore;
#pragma warning disable 4014
			LoadCommand = new Command(() => InitializeAsync());
#pragma warning restore 4014
			ToggleBaseCommand = new Command<bool>(o => ThemeSwitcher.ApplyBase((bool) o));

			var writer = this.WhenAnyPropertyChanged()
			                 .Subscribe(model =>
			                 {
				                 _settingsStore.Write(new Options(_rovinka, _oval, _options.IpAddress, _isDark, _grpcServerAddress));
			                 });
			RovinkaTrackCount = new ObservableCollection<int>(Enumerable.Range(6, 5));
			OvalTrackCount = new ObservableCollection<int>(Enumerable.Range(4, 5));
			_cleanUp = new CompositeDisposable(writer);
		}

		private Options _options = null!;

		private async Task InitializeAsync()
		{
			_options = await _settingsStore.LoadAsync();
			Rovinka = _options.Rovinka;
			Oval = _options.Oval;
			GrpcServerAddress = _options.GrpcServer;
			IsDark = _options.IsDark;
		}

		public int Rovinka
		{
			get => _rovinka;
			set => SetAndRaise(ref _rovinka, value);
		}

		public int Oval
		{
			get => _oval;
			set => SetAndRaise(ref _oval, value);
		}

		public string GrpcServerAddress
		{
			get => _grpcServerAddress;
			set => SetAndRaise(ref _grpcServerAddress, value);
		}

		public bool IsDark
		{
			get => _isDark;
			set => SetAndRaise(ref _isDark, value);
		}

		public void Dispose()
		{
			_cleanUp.Dispose();
		}
	}
}
