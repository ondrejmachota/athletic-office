﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using AtletickaKancelar.Domain.Enums;
using AtletickaKancelar.Domain.Models;
using AtletickaKancelar.Domain.Settings;
using AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions;
using AtletickaKancelar.WPF.Models;

namespace AtletickaKancelar.WPF.Views.RegistrationsWindow
{
	public class RegistrationExtension
    {
        private Options _options = null!;

        public static async Task<RegistrationExtension> Create(ISettingsStore settingsStore)
        {
            return new RegistrationExtension {_options = await settingsStore.LoadAsync()};
        }

		public async Task SetCompetitorsToTracks(DisciplineType type, ObservableCollection<SpojkaMainModel> spojky)
        {
            var random = new Random();
            var usedTracks = new List<int>();
            int track;
            var spojkyCount = spojky.Count;
            if (type == DisciplineType.Sprint)
            {
                double pocetDrahNaRovince = Convert.ToDouble(_options.Rovinka);
                if (spojkyCount <= pocetDrahNaRovince)
                    track = (int)Math.Ceiling(pocetDrahNaRovince / 2);
                else
                    track = (int)Math.Ceiling((double)spojkyCount / 2);
            }
            else
            {
                double pocetDrahNaOvale = Convert.ToDouble(_options.Oval);
                if (spojkyCount <= pocetDrahNaOvale)
                    track = (int)Math.Ceiling(pocetDrahNaOvale / 2);
                else
                    track = (int)Math.Ceiling((double)spojkyCount / 2);
            }
            for (int i = 0; i < spojkyCount; i++)
            {
                if (i % 2 == 0)
                    track -= i;
                else
                    track += i;
                int index = random.Next(0, spojkyCount);
                /*if (pouzitaCisla.Contains(index))
                {
                    do
                    {
                        index = random.Next(0, spojkyCount);
                    }
                    while (pouzitaCisla.Contains(index));
                }*/
                while (usedTracks.Contains(index))
                {
                    index = random.Next(0, spojkyCount);
                }
                usedTracks.Add(index);
                spojky[index].Spojka.Track = track;
            }
            await SpojkaProtoExtension.UpdateSpojky(spojky.ToList());
        }
	}
}
