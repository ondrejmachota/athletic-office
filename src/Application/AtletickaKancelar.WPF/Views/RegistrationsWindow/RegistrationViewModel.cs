﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.Domain.Settings;
using AtletickaKancelar.WPF.Infrastructure;
using AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions;
using AtletickaKancelar.WPF.Models;
using AtletickaKancelar.WPF.Utilities;
using AtletickaKancelar.WPF.Views.Shared;
using AtletickaKancelar.WPF.Views.WindowManagement;
using GrpcServer;

namespace AtletickaKancelar.WPF.Views.RegistrationsWindow
{
	public class RegistrationViewModel : ViewModelBase
	{
		private bool _addButtonsEnabled;
		private bool _isTrackButtonEnable;
		private bool _isControlsEnable;
		private bool _isDialogHostOpen;
		private int _selectedCompetitorsCount;
		private string _selectedCompetitorsCountText = "Žádný vybraný závodník";
		private string _searchText;
		private DialogSwitchViewModelExtra _dialogSwitchViewModel;
		private SpojkaMainModel _selectedSpojka = null!;
		private CompetitorMainModel _selectedCompetitor = null!;
		private CompetitorMainModel _selectedCompetitorCache = null!;
		private CompetitorMainModel _createCompetitor;
		private DisciplineMainModel? _selectedDiscipline;
		private List<Club> _clubs = null!;
		private Club _newClub;
		private DisciplinesListViewViewModel _disciplinesListViewViewModel = null!;
		private RegistrationExtension? _registrationExtension;

		public ObservableCollection<CompetitorMainModel> Competitors { get; set; } = new ObservableCollection<CompetitorMainModel>();

		public ObservableCollection<SpojkaMainModel> Spojky { get; set; } = new ObservableCollection<SpojkaMainModel>();

		public ICommand LoadCommand { get; }
		public ICommand ResetCompetitorsCommand { get; }
		public ICommand SelectCompetitorCommand { get; }
		public ICommand DisciplineSelectionChangedCommand { get; }
		public ICommand RegisterCommand { get; }
		public ICommand Print { get; }
		public ICommand CancelPrintCommand { get; }
		public ICommand SetCompetitorTrack { get; }
		public ICommand SaveCommand { get; }
		public ICommand OpenDialohHostCommand { get; }
		public ICommand CloseDialogHOstCommand { get; }
		public ICommand CreateOrEditCommand { get; }
		public ICommand DeleteSpojkaCommand { get; }
		public ICommand MoveCompetitors { get; }

		public RegistrationViewModel(ISettingsStore settingsStore)
		{
			_searchText = "";
			_newClub = new Club();
			_addButtonsEnabled = false;
			_createCompetitor = CompetitorMainModel.CreateDefault();
			_dialogSwitchViewModel = new DialogSwitchViewModelExtra("Vytvořit", "závodníka");
			LoadCommand = new Command(() =>
			{
#pragma warning disable 4014
				DisciplinesListViewViewModel = new DisciplinesListViewViewModel();
				LoadCompetitors();
#pragma warning restore 4014
			});
			ResetCompetitorsCommand = new Command(ResetCompetitors);
			SelectCompetitorCommand = new Command<bool>(param =>
			{
				if ((bool) param)
					_selectedCompetitorsCount++;
				else
					_selectedCompetitorsCount--;

				SetSelectedCompetitorsCountText();
			});
			DisciplineSelectionChangedCommand = new Command(() =>
			{
				if (DisciplinesListViewViewModel.SelectedDiscipline != null)
#pragma warning disable 4014
				{
					LoadSpojkyByDisciplineId(DisciplinesListViewViewModel.SelectedDiscipline.Discipline.Id);
					IsControlsEnable = true;
					IsTrackButtonEnable = DisciplinesListViewViewModel.SelectedDiscipline.IsRun;
				}
				else
				{
					IsControlsEnable = false;
					IsTrackButtonEnable = false;
				}
			});
			RegisterCommand = new Command(() => RegisterCompetitorsToDiscipline());
			Print = new Command(() => PrintMethod());
			CancelPrintCommand = new Command(CancelPrint);
			SetCompetitorTrack = new Command(async () =>
			{
				_registrationExtension ??= await RegistrationExtension.Create(settingsStore).ConfigureAwait(false);
				_registrationExtension.SetCompetitorsToTracks(DisciplinesListViewViewModel.SelectedDiscipline!.Discipline.DisciplineType, Spojky);
			});
			SaveCommand = new Command(() => SpojkaProtoExtension.UpdateSpojky(Spojky.ToList()).ConfigureAwait(false));
			OpenDialohHostCommand = new Command<EditorModeRegistrations>(param => SetDialogHost((EditorModeRegistrations) param));
			CloseDialogHOstCommand = new Command(() =>
			{
				IsDialogHostOpen = false;
				DialogSwitchViewModel.AlertVisibility = Visibility.Collapsed;
				if (DialogSwitchViewModel.EditorMode == EditorModeRegistrations.EditCompetitor)
				{
					var index = Competitors.IndexOf(SelectedCompetitor);
					Competitors[index] = _selectedCompetitorCache;
				}
			});
			CreateOrEditCommand = new Command(() => CreateOrEdit());
			DeleteSpojkaCommand = new Command(() => DeleteSpojka());
			MoveCompetitors = new Command(() => MoveCompetitorsToDiscipline());
#pragma warning restore 4014
		}

		public DisciplinesListViewViewModel DisciplinesListViewViewModel
		{
			get => _disciplinesListViewViewModel;
			set => SetAndRaise(ref _disciplinesListViewViewModel, value);
		}

		public List<Club> Clubs
		{
			get => _clubs;
			set => SetAndRaise(ref _clubs, value);
		}

		public CompetitorMainModel SelectedCompetitor
		{
			get => _selectedCompetitor;
			set => SetAndRaise(ref _selectedCompetitor, value);
		}

		public CompetitorMainModel CreateCompetitor
		{
			get => _createCompetitor;
			set => SetAndRaise(ref _createCompetitor, value);
		}

		public SpojkaMainModel SelectedSpojka
		{
			get => _selectedSpojka;
			set => SetAndRaise(ref _selectedSpojka, value);
		}

		public DialogSwitchViewModelExtra DialogSwitchViewModel
		{
			get => _dialogSwitchViewModel;
			set => SetAndRaise(ref _dialogSwitchViewModel, value);
		}

		public DisciplineMainModel? SelectedDiscipline
		{
			get => _selectedDiscipline;
			set => SetAndRaise(ref _selectedDiscipline, value);
		}

		public Club NewClub
		{
			get => _newClub;
			set => SetAndRaise(ref _newClub, value);
		}

		public bool AddButtonsEnabled
		{
			get => _addButtonsEnabled;
			set => SetAndRaise(ref _addButtonsEnabled, value);
		}

		public bool IsDialogHostOpen
		{
			get => _isDialogHostOpen;
			set => SetAndRaise(ref _isDialogHostOpen, value);
		}

		public bool IsControlsEnable
		{
			get => _isControlsEnable;
			set => SetAndRaise(ref _isControlsEnable, value);
		}

		public bool IsTrackButtonEnable
		{
			get => _isTrackButtonEnable;
			set => SetAndRaise(ref _isTrackButtonEnable, value);
		}

		public string SelectedCompetitorsCountText
		{
			get => _selectedCompetitorsCountText;
			set => SetAndRaise(ref _selectedCompetitorsCountText, value);
		}

		public string SearchText
		{
			get => _searchText;
			set
			{
				SetAndRaise(ref _searchText, value);
				OnSearchTextChanged();
			}
		}

		private async Task SetDialogHost(EditorModeRegistrations editorModeRegistrations)
		{
			IsDialogHostOpen = true;
			Clubs ??= await ClubProtoExtension.DownloadClubs();
			switch (editorModeRegistrations)
			{
				case EditorModeRegistrations.CreateCompetitor:
					_dialogSwitchViewModel.SetValues(EditorModeRegistrations.CreateCompetitor);
					break;
				case EditorModeRegistrations.EditCompetitor:
					_dialogSwitchViewModel.SetValues(EditorModeRegistrations.EditCompetitor);
					SelectedCompetitor = Competitors.FirstOrDefault(x => x.Competitor.Id == SelectedSpojka.Spojka.CompetitorId)!;
					_selectedCompetitorCache = CompetitorMainModel.Copy(SelectedCompetitor.Competitor);
					break;
				case EditorModeRegistrations.CreateClub:
					_dialogSwitchViewModel.SetValues(EditorModeRegistrations.CreateClub);
					break;
			}
		}

		private void SetSelectedCompetitorsCountText()
		{
			SelectedCompetitorsCountText = _selectedCompetitorsCount switch
			{
				0 => "Žádný vybraný závodník",
				1 => "Vybrán 1 závodník",
				{ } n when n < 5 => $"Vybráni {_selectedCompetitorsCount} závodníci",
				_ => $"Vybráno {_selectedCompetitorsCount} závodníků"
			};
			AddButtonsEnabled = _selectedCompetitorsCount != 0;
		}

		private void ResetCompetitors()
		{
			Parallel.For(0, Competitors.Count, i => { Competitors[i].IsSelected = false; });
			_selectedCompetitorsCount = 0;
			SetSelectedCompetitorsCountText();
		}

		private async Task LoadCompetitors()
		{
			var client = CompetitorProtoExtension.Create();
			using var call = client.GetCompetitorStream(new EmptyCompetitorRequest());
			await foreach (var competitor in CompetitorProtoExtension.DownloadCompetitorsAsync(call))
			{
				Competitors.Add(competitor);
			}
		}

		private async Task LoadSpojkyByDisciplineId(int id)
		{
			var client = SpojkaProtoExtension.Create();
			Spojky.Clear();
			using var call = client.GetSpojkaByDisciplineIdSteam(new DisciplineIdRequest {Id = id});
			await foreach (var spojka in SpojkaProtoExtension.DownloadSpojkyAsync(call))
			{
				Spojky.Add(spojka);
			}
		}

		private bool _isCoppied;
		private List<CompetitorMainModel> _cachedCompetitors = new List<CompetitorMainModel>();

		private void OnSearchTextChanged()
		{
			if (SearchText.Length > 0)
			{
				if (_isCoppied == false)
				{
					_isCoppied = true;
					_cachedCompetitors = new List<CompetitorMainModel>(Competitors);
				}

				Competitors =
					new ObservableCollection<CompetitorMainModel>(
						_cachedCompetitors.Where(x => x.FullName.Contains(SearchText)));
			}
			else
			{
				Competitors = new ObservableCollection<CompetitorMainModel>(_cachedCompetitors);
				_isCoppied = false;
			}
		}

		private async Task RegisterCompetitorsToDiscipline()
		{
			var discipline = DisciplinesListViewViewModel.SelectedDiscipline;
			if (discipline != null)
			{
				AddButtonsEnabled = false;
				var selectedCompetitors = Competitors.Where(x => x.IsSelected).ToList();

				var spojkyGrpc = await SpojkaProtoExtension.CheckSpojky(selectedCompetitors,
					discipline.Discipline.Id, discipline.IsRun, discipline.Discipline.MainId);

				if (spojkyGrpc.Count > 0)
				{
					var spojky = await SpojkaProtoExtension.Add(spojkyGrpc);
					for (int i = 0; i < spojky.Count; i++)
					{
						if (Spojky.FirstOrDefault(x => x.Spojka.Id == spojky[i].Spojka.Id) == null)
							Spojky.Add(spojky[i]);
					}
				}

				ResetCompetitors();
			}
		}

		private async Task CreateOrEdit()
		{
			Club? club;
			IsDialogHostOpen = false;
			switch (DialogSwitchViewModel.EditorMode)
			{
				case EditorModeRegistrations.CreateCompetitor:
					club = Clubs.FirstOrDefault(x => x.Name == CreateCompetitor.Competitor.ClubName);
					if (club != null)
					{
						CreateCompetitor.Competitor.ClubId = club.Id;
						CreateCompetitor.Competitor.ClubName = club.Name;
						CreateCompetitor.Competitor.ClubAbbreviation = club.Abbreviation;
					}

					// TODO : add validation
					var newCompetitor = await CompetitorProtoExtension.CreateCompetitor(CreateCompetitor);
					Competitors.Add(newCompetitor);
					break;
				case EditorModeRegistrations.EditCompetitor:
					club = Clubs.FirstOrDefault(x => x.Name == SelectedCompetitor.Competitor.ClubName);
					if (club != null)
					{
						SelectedCompetitor.Competitor.ClubId = club.Id;
						SelectedCompetitor.Competitor.ClubName = club.Name;
						SelectedCompetitor.Competitor.ClubAbbreviation = club.Abbreviation;
						await CompetitorProtoExtension.UpdateCompetitor(SelectedCompetitor);
						var index = Competitors.IndexOf(SelectedCompetitor);
						Competitors[index] = CompetitorMainModel.Copy(SelectedCompetitor.Competitor);
					}

					// TODO : update collections
					break;
				case EditorModeRegistrations.CreateClub:
					var isUnique = await ClubProtoExtension.IsUnique(NewClub.Name, NewClub.Abbreviation).ConfigureAwait(false);
					if (isUnique)
					{
						Clubs.Add(await ClubProtoExtension.CreateClub(NewClub));
						NewClub = new Club();
						DialogSwitchViewModel.AlertVisibility = Visibility.Collapsed;
					}
					else
					{
						DialogSwitchViewModel.AlertVisibility = Visibility.Visible;
					}

					break;
			}
		}

		private async Task DeleteSpojka()
		{
			await SpojkaProtoExtension.DeleteSpojka(SelectedSpojka);
			Spojky.Remove(SelectedSpojka);
		}

		private async Task PrintMethod()
		{
			await Printer.PrintRegOrResl(DisciplinesListViewViewModel.Disciplines, true);
		}

		private void CancelPrint()
		{
			for (int i = 0; i < DisciplinesListViewViewModel.Disciplines.Count; i++)
			{
				DisciplinesListViewViewModel.Disciplines[i].IsSelected = false;
			}
		}

		private async Task MoveCompetitorsToDiscipline()
		{
			var selectedSpojky = Spojky.Where(x => x.IsSelected);
			if (SelectedDiscipline != null && DisciplinesListViewViewModel.SelectedDiscipline != null)
			{
				if (DisciplinesListViewViewModel.SelectedDiscipline.Discipline.Id != SelectedDiscipline.Discipline.Id)
				{
					List<SpojkaMainModel> spojky;
					var isRun = Extensions.IsRun(SelectedDiscipline.Discipline.DisciplineType);
					if (isRun && SelectedDiscipline.Discipline.MainId.HasValue)
					{
						spojky = await SpojkaProtoExtension.GetSpojkyById(true,
							SelectedDiscipline.Discipline.MainId.Value); // fetch spojky where a maidId equals to selected discipline mainId
					}
					else
					{
						spojky = await SpojkaProtoExtension.GetSpojkyById(false,
							SelectedDiscipline.Discipline.Id); // fetch spojky where an Id equals to selected discipline
					}

					int spojkyCount = spojky.Count;
					List<SpojkaMainModel> spojkaModels = new List<SpojkaMainModel>();
					bool reload = false;
					foreach (SpojkaMainModel spojka in selectedSpojky)
					{
						bool isRegistered = false;
						for (int i = 0; i < spojkyCount; i++)
						{
							if (spojky[i].Spojka.CompetitorId == spojka.Spojka.CompetitorId)
							{
								// TODO : isRegistered = true; If are the selected competitors in downloaded spojky, it means that they are actually registered in that discipline
								// Moving fixed, but clean this code

								if (isRun)
								{
									// moving between discipline childs, TODO : replace with actual discipline.MainId, in which is the spojka registered
									isRegistered = SelectedDiscipline.Discipline.MainId !=
									               DisciplinesListViewViewModel.SelectedDiscipline.Discipline.MainId;
								}

								break;
							}
							else
							{
								isRegistered = false;
							}
						}

						if (isRegistered == false)
						{
							reload = true;
							Spojky.Where((x) => x.Spojka.Id == spojka.Spojka.Id).ToList().ForEach((s) =>
							{
								s.Spojka.DisciplineId = SelectedDiscipline.Discipline.Id;
								s.Spojka.MainId = SelectedDiscipline.Discipline.MainId;
							});

							if (isRun == false)
							{
								spojka.Spojka.Track = null;
							}

							spojkaModels.Add(spojka);
						}
					}

					if (reload)
					{
						await SpojkaProtoExtension.UpdateSpojky(spojkaModels);
						//DiscIdPresunZavodnikListView.SelectedIndex = -1;
						for (int i = 0; i < spojkaModels.Count; i++)
						{
							Spojky.Remove(spojkaModels[i]);
						}
					}
				}

				for (int i = 0; i < Spojky.Count; i++)
				{
					Spojky[i].IsSelected = false;
				}

				SelectedDiscipline = null;
			}
		}
	}
}
