﻿using AtletickaKancelar.Domain.Settings;
using AtletickaKancelar.WPF.Views.WindowManagement.Factories;

namespace AtletickaKancelar.WPF.Views.RegistrationsWindow
{
	public class RegistrationViewModelFactory : IViewModelFactory<RegistrationViewModel>
	{
		private readonly ISettingsStore _settingsStore;
		
		public RegistrationViewModelFactory(ISettingsStore settingsStore)
		{
			_settingsStore = settingsStore;
		}
		
		public RegistrationViewModel CreateViewModel()
		{
			return new RegistrationViewModel(_settingsStore);
		}
	}
}