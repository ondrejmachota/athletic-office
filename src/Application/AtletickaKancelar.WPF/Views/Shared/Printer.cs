﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Media;
using AtletickaKancelar.WPF.Infrastructure;
using AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions;
using AtletickaKancelar.WPF.Models;

namespace AtletickaKancelar.WPF.Views.Shared
{
	public static class Printer
	{
		public static async Task PrintRegOrResl(ObservableCollection<DisciplineMainModel> disciplines, bool registrations)
		{
			if (disciplines.Count != 0)
			{
				List<DisciplineModel> printList = new List<DisciplineModel>();
				int i = 0;
				foreach (DisciplineMainModel disciplina in disciplines)
				{
					if (disciplina.IsSelected)
					{
						if (registrations)
						{
							disciplina.ForegroundColorPrihl = Brushes.Green;
							disciplina.Discipline.IsRegistrationsPrinted = true;
						}
						else
						{
							disciplina.ForegroundColorVysl = Brushes.Green;
							disciplina.Discipline.IsResultsPrinted = true;
						}
						i++;
						printList.Add(disciplina.Discipline);
						disciplina.IsSelected = false;
					}
				}
				if (i != 0)
				{
					if (registrations) 
						await HtmlCreator.CreateSaveOpenPrihlasky(printList, await SpojkaProtoExtension.GetAll());
					else
						await HtmlCreator.CreateSaveOpenVysledky(printList, await SpojkaProtoExtension.GetAll());
				}
			}
		}

		public static async Task PrintSchedule(List<DisciplineMainModel> disciplines)
		{
			var disciplineModels = new List<DisciplineModel>();
			for (var i = 0; i < disciplines.Count; i++)
			{
				disciplineModels.Add(disciplines[i].Discipline);
			}
			await HtmlCreator.CreateSaveOpenCasovyPorad(disciplineModels);
		}
	}
}