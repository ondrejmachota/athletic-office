﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions;
using AtletickaKancelar.WPF.Models;
using DynamicData.Binding;
using GrpcServer;

namespace AtletickaKancelar.WPF.Views.Shared
{
	public class DisciplinesListViewViewModel : AbstractNotifyPropertyChanged
	{
		private DisciplineMainModel? _selectedDiscipline;
		public ObservableCollection<DisciplineMainModel> Disciplines { get; set; } = new ObservableCollection<DisciplineMainModel>();
		private readonly DisciplineGrpcProtoService.DisciplineGrpcProtoServiceClient _client;

		public DisciplinesListViewViewModel()
		{
			_client = ScheduleProtoExtension.Create();
#pragma warning disable 4014
			FetchDisciplines().ConfigureAwait(false);
#pragma warning restore 4014
		}

		public DisciplineMainModel? SelectedDiscipline
		{
			get => _selectedDiscipline;
			set => SetAndRaise(ref _selectedDiscipline, value);
		}

		private async Task FetchDisciplines()
		{
			using var call = _client.GetDisciplineStream(new EmptyDisciplineRequest());
			await foreach (var discipline in ScheduleProtoExtension.DownloadDisciplinesAsync(call))
			{
				Disciplines.Add(discipline);
			}
		}
	}
}
