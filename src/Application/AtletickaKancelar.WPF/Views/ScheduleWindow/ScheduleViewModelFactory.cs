﻿using AtletickaKancelar.WPF.Views.WindowManagement.Factories;

namespace AtletickaKancelar.WPF.Views.ScheduleWindow
{
	public class ScheduleViewModelFactory : IViewModelFactory<ScheduleViewModel>
	{
		public ScheduleViewModel CreateViewModel()
		{
			return new ScheduleViewModel();
		}
	}
}