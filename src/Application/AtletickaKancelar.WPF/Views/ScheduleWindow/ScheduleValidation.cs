﻿namespace AtletickaKancelar.WPF.Views.ScheduleWindow
{
	public class ScheduleValidation
	{
		public string CreateDisciplineSelection { get; set; }
		public string CreateCategorySelection { get; set; }
		public string CreateDisciplineStartTime { get; set; }
		public string CreateDisciplinePhase { get; set; }
		
		public string EditDisciplineSelection { get; set; }
		public string EditCategorySelection { get; set; }
		public string EditDisciplineStartTime { get; set; }
		public string EditDisciplinePhase { get; set; }

		public ScheduleValidation()
		{
			CreateDisciplineSelection = "";
			CreateCategorySelection = "";
			CreateDisciplineStartTime = "";
			CreateDisciplinePhase = "";

			EditDisciplineSelection = "";
			EditCategorySelection = "";
			EditDisciplineStartTime = "";
			EditDisciplinePhase = "";
		}
	}
}