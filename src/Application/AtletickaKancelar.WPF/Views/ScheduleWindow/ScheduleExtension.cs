﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using AtletickaKancelar.WPF.Converters;
using AtletickaKancelar.WPF.Infrastructure;
using AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions;
using AtletickaKancelar.WPF.Models;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using GrpcServer;

namespace AtletickaKancelar.WPF.Views.ScheduleWindow
{
	public static class ScheduleExtension
	{
		public static async Task<DisciplineMainModel> CreateDiscipline(DisciplineConstant disciplineConstant, DisciplineModel selectedDiscipline, List<DisciplineMainModel> disciplines, DisciplineGrpcProtoService.DisciplineGrpcProtoServiceClient client)
		{
			var assignMainId = false;
			selectedDiscipline.MainId = null;
			selectedDiscipline.Name = disciplineConstant.Name;
			selectedDiscipline.DisciplineType = disciplineConstant.DisciplineType;
			if (Extensions.IsRun(disciplineConstant.DisciplineType))
			{
				var isDisciplineSimilar = await client.GetSimilarDisciplineAsync(new GetSimilarDisciplineGrpcModel
				{
					Category = selectedDiscipline.Category,
					Name = disciplineConstant.Name,
					PhaseType = (int) selectedDiscipline.PhaseType,
					Start = Duration.FromTimeSpan(selectedDiscipline.Start)
				});
				selectedDiscipline.CameraId = GenerateCameraId(disciplines);
				if (isDisciplineSimilar.Id == 0) // there isnt any similar discipline => create a new one with unique mainId
				{
					// discipline = new Discipline( ); // set order to 1
					selectedDiscipline.Order ??= 1;
					assignMainId = true;
				}
				else //we found a similar discipline, so we just create a child discipline
				{
					selectedDiscipline.MainId = isDisciplineSimilar.MainId;
					selectedDiscipline.Order = await GenerateOrder(selectedDiscipline ,selectedDiscipline.Order);
				}
			}
			var newDiscipline = await client.CreateDisciplineAsync(new CreateDisciplineGrpcModel
			{
				AssignMainId = assignMainId,
				DisciplineModel = selectedDiscipline.ToDisciplineDto(),
			});
			return new DisciplineMainModel(newDiscipline.ToDisciplineModel());
		}

		public static async Task<DisciplineMainModel> CreateChildDiscipline(DisciplineMainModel discipline, List<DisciplineMainModel> disciplines, DisciplineGrpcProtoService.DisciplineGrpcProtoServiceClient client)
		{
			var order = await GenerateOrder(discipline.Discipline, null);
			var cameraId = GenerateCameraId(disciplines);

			var newDiscipline = await client.CreateChildDisciplineAsync(new CreateChildDisciplineGrpcModel
			{
				DisciplineModel = discipline.Discipline.ToDisciplineDto(),
				Order = order,
				CameraId = cameraId,
			});

			return new DisciplineMainModel(newDiscipline.ToDisciplineModel());
		}

		/// <summary>
		/// Přidá disciplíně pořadí běhu
		/// </summary>
		/// <returns>pořadí běhu</returns>
		private static async Task<int> GenerateOrder(DisciplineModel? discipline, int? order) // DisciplineId to look for child disciplines
		{
			if (order != null)
			{
				return order.Value;
			}
			if (discipline == null) // maybe the discipline is never null
				return 1;

			var client = ScheduleProtoExtension.Create();
			var orders = new List<int?>();
			using (var call = client.GetChildDisciplinesOrderStream(new ChildDisciplineOrderModel { Value = discipline.MainId}))
			{
				while (await call.ResponseStream.MoveNext())
				{
					orders.Add(call.ResponseStream.Current.Value);
				}
			}

			orders = orders.Where(x => x.HasValue).ToList();
			orders.Sort();
			var max = orders.Count != 0 ? orders.Max()!.Value : 1;
			if (orders.Count <= max)
			{
				for (int i = 0; i < orders.Count; i++)
				{
					if (orders[i] != i + 1)
					{
						return i + 1;
					}
				}
			}
			else
			{
				return max + 1;
			}
			return max + 1;
		}

		/// <summary>
		/// Přidá disciplíně KameraId
		/// </summary>
		/// <returns>KameraId</returns>
		private static int GenerateCameraId(List<DisciplineMainModel> disciplines)
		{
			List<int> used = new List<int>();
			var cislo = 1;
			List<DisciplineMainModel> pocetDisciplinList = disciplines.FindAll(x => x.IsRun);
			if (pocetDisciplinList.Count > 0)
			{
				for (int i = 0; i < pocetDisciplinList.Count; i++)
				{
					var id = pocetDisciplinList[i].Discipline.CameraId;
					if (id.HasValue)
						used.Add(id.Value);
				}
				used.Sort();
				if (pocetDisciplinList.Count != used.Max())
				{
					for (int i = 0; i < pocetDisciplinList.Count; i++)
					{
						if (used[i] != i + 1)
						{
							return i + 1;
						}
					}
				}
				else
					cislo = pocetDisciplinList.Count + 1;
			}
			return cislo;
		}

		public static void FindDisciplines(ObservableCollection<DisciplineMainModel> disciplines, List<DisciplineMainModel> cachedDisciplines, string name, string category)
		{
			var disciplineModels = new List<DisciplineMainModel>();

			if (string.IsNullOrEmpty(name))
			{
				disciplineModels = (from discipline in cachedDisciplines
				                    where discipline.Discipline.Category == category
				                    select discipline).ToList();
			}

			if (string.IsNullOrEmpty(category))
			{
				disciplineModels = (from discipline in cachedDisciplines
				                    where discipline.Discipline.Name == name
				                    select discipline).ToList();
			}

			if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(category))
			{
				disciplineModels = (from discipline in cachedDisciplines
				                    where discipline.Discipline.Name == name && discipline.Discipline.Category == category
				                    select discipline).ToList();
			}

			disciplines.Clear();
			for (int i = 0; i < disciplineModels.Count; i++)
			{
				disciplines.Add(disciplineModels[i]);
			}
		}

		public static async void ChangeCameraIds(ObservableCollection<DisciplineMainModel> disciplines)
		{
			Random random = new Random();
			List<int> used = new List<int>();
			List<DisciplineMainModel> runDisciplines = disciplines.ToList().FindAll(x => x.IsRun);
			int newCamId;
			if (runDisciplines.Count != 0)
			{
				for (int i = 0; i < runDisciplines.Count; i++)
				{
					do
					{
						newCamId = random.Next(1, runDisciplines.Count + 1);
					}
					while (used.Contains(newCamId));
					runDisciplines[i].Discipline.CameraId = newCamId;
					disciplines.FirstOrDefault(x => x.Discipline.Id == runDisciplines[i].Discipline.Id)!.Discipline.CameraId = newCamId;
					used.Add(newCamId);
				}

				await ScheduleProtoExtension.UpdateListDisciplines(runDisciplines.Select(x => x.Discipline).ToList());
			}
		}
	}
}
