﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using AtletickaKancelar.Domain.Enums;
using AtletickaKancelar.WPF.Converters;
using AtletickaKancelar.WPF.Infrastructure;
using AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions;
using AtletickaKancelar.WPF.Models;
using AtletickaKancelar.WPF.Utilities;
using AtletickaKancelar.WPF.Views.Shared;
using AtletickaKancelar.WPF.Views.WindowManagement;
using GrpcServer;

namespace AtletickaKancelar.WPF.Views.ScheduleWindow
{
	public class ScheduleViewModel : ViewModelBase
	{
		private bool _orderIsEnabled;
		private bool _isDialogHostOpen;
		private EditorMode _editorMode = EditorMode.Create;
		private DisciplineMainModel _selectedDiscipline = null!;
		private DisciplineMainModel _selectedDisciplineCache = null!;
		private DisciplineMainModel _createDiscipline;
		private DisciplineConstant? _disciplineComboBoxSelectedItem;
		private DisciplineConstant? _createdisciplineComboBoxSelectedItem;
		private string _comboSearchDisciplineConstant = null!;
		private string _selectedCategory = null!;
		private List<DisciplineMainModel> _cachedDisciplines = new List<DisciplineMainModel>();
		private readonly DisciplineGrpcProtoService.DisciplineGrpcProtoServiceClient _client;

		public ObservableCollection<DisciplineMainModel> Disciplines { get; set; } = new ObservableCollection<DisciplineMainModel>();

		public ICommand LoadCommand { get; }
		public ICommand DisciplineSelectionChangedCommand { get; }
		public ICommand OpenDialogHostCm { get; }
		public ICommand CloseDialogHost { get; }
		public ICommand CopyDiscipline { get; }
		public ICommand DeleteDisciplineCommand { get; }
		public ICommand DisciplineCommand { get; }
		public ICommand PrintCommand { get; }

		public ICommand ChangeCameraIds { get; }

		public ScheduleViewModel()
		{
			_isDialogHostOpen = false;
			_createDiscipline = DisciplineMainModel.CreateDefault();
			_client = ScheduleProtoExtension.Create();
			DialogSwitchViewModel = new DialogSwitchViewModel("Vytvořit", "disciplínu");
			// BindingOperations.EnableCollectionSynchronization(Disciplines, SyncLock);
#pragma warning disable 4014
			LoadCommand = new Command(() => LoadDisciplinesAsync());
			DisciplineSelectionChangedCommand = new Command<EditorMode>(param => OnDisciplineSelectionChanged((EditorMode) param));
			OpenDialogHostCm = new Command<EditorMode>(param =>
			{
				_editorMode = (EditorMode) param;
				if (_editorMode == EditorMode.Edit)
				{
					_selectedDisciplineCache = new DisciplineMainModel(new DisciplineModel(SelectedDiscipline.Discipline));
					SelectedDiscipline.StartTime = new DateTime(SelectedDiscipline.Discipline.Start.Ticks);
					DisciplineComboBoxSelectedItem = DisciplineConstants.FirstOrDefault(x => x.Name == SelectedDiscipline.Discipline.Name)!;
				}
				else if (_editorMode == EditorMode.Create)
				{
					CreateDiscipline = DisciplineMainModel.CreateDefault();
					CreateDiscipline.StartTime = new DateTime(CreateDiscipline.Discipline.Start.Ticks);
					CreateDisciplineComboBoxSelectedItem = DisciplineConstants.FirstOrDefault(x => x.Name == CreateDiscipline.Discipline.Name)!;
				}

				DialogSwitchViewModel.SetValues(_editorMode);
				IsDialogHostOpen = true;
			});
			DisciplineCommand = new Command(() =>
			{
				if (_editorMode == EditorMode.Create)
					CreateDisciplineAsync();
				else if (_editorMode == EditorMode.Edit)
					EditDisciplineAsync();
			});
			CloseDialogHost = new Command(() =>
			{
				IsDialogHostOpen = false;
				if (_editorMode == EditorMode.Edit)
				{
					var index = Disciplines.IndexOf(SelectedDiscipline);
					Disciplines[index] = _selectedDisciplineCache;
				}
			});
			CopyDiscipline = new Command(() => CopyDisciplineAsync());
			DeleteDisciplineCommand = new Command(() => DeleteDisciplineAsync());
			PrintCommand = new Command(() => Printer.PrintSchedule(Disciplines.ToList()));
			ChangeCameraIds = new Command(() => ScheduleExtension.ChangeCameraIds(Disciplines));
#pragma warning restore 4014
		}

		public DialogSwitchViewModel DialogSwitchViewModel { get; set; }

		public ObservableCollection<DisciplineConstant> DisciplineConstants { get; } =
			new ObservableCollection<DisciplineConstant>(Constants.DisciplineConstants);

		public ObservableCollection<string> Categories { get; } =
			new ObservableCollection<string>(Constants.CategoryConstants);

		public ObservableCollection<PhaseType> Phases { get; } =
			new ObservableCollection<PhaseType>(Enum.GetValues(typeof(PhaseType)).Cast<PhaseType>());

		public DisciplineConstant? DisciplineComboBoxSelectedItem
		{
			get => _disciplineComboBoxSelectedItem;
			set => SetAndRaise(ref _disciplineComboBoxSelectedItem, value);
		}

		public DisciplineConstant? CreateDisciplineComboBoxSelectedItem
		{
			get => _createdisciplineComboBoxSelectedItem;
			set => SetAndRaise(ref _createdisciplineComboBoxSelectedItem, value);
		}

		public DisciplineMainModel SelectedDiscipline
		{
			get => _selectedDiscipline;
			set => SetAndRaise(ref _selectedDiscipline, value);
		}

		public DisciplineMainModel CreateDiscipline
		{
			get => _createDiscipline;
			set => SetAndRaise(ref _createDiscipline, value);
		}

		public bool OrderIsEnabled
		{
			get => _orderIsEnabled;
			set => SetAndRaise(ref _orderIsEnabled, value);
		}

		public bool IsDialogHostOpen
		{
			get => _isDialogHostOpen;
			set => SetAndRaise(ref _isDialogHostOpen, value);
		}

		public string ComboSearchDisciplineConstant
		{
			get => _comboSearchDisciplineConstant;
			set
			{
				SetAndRaise(ref _comboSearchDisciplineConstant, value);
				SearchInDisicplines();
			}
		}

		public string SelectedCategory
		{
			get => _selectedCategory;
			set
			{
				SetAndRaise(ref _selectedCategory, value);
				SearchInDisicplines();
			}
		}

		private bool _isColletionCached;

		private void SearchInDisicplines()
		{
			if (!string.IsNullOrEmpty(SelectedCategory) || !string.IsNullOrEmpty(ComboSearchDisciplineConstant) ||
			    (!string.IsNullOrEmpty(SelectedCategory) && !string.IsNullOrEmpty(ComboSearchDisciplineConstant)))
			{
				if (!_isColletionCached)
				{
					_isColletionCached = true;
					_cachedDisciplines = Disciplines.ToList();
				}

				ScheduleExtension.FindDisciplines(Disciplines, _cachedDisciplines, ComboSearchDisciplineConstant, SelectedCategory);
			}
			else
			{
				_isColletionCached = false;
				Disciplines.Clear();
				for (int i = 0; i < _cachedDisciplines.Count; i++)
				{
					Disciplines.Add(_cachedDisciplines[i]);
				}
			}
		}

		private async Task CreateDisciplineAsync()
		{
			if (CreateDisciplineComboBoxSelectedItem == null)
				return;
			IsDialogHostOpen = false;
			var disciplineConstant = Constants.DisciplineConstants[DisciplineConstants.IndexOf(CreateDisciplineComboBoxSelectedItem)];
			var disciplines = Disciplines.ToList();
			CreateDiscipline.Discipline.Start = TimeSpan.FromTicks(CreateDiscipline.StartTime.Ticks);
			var newDiscipline = await ScheduleExtension.CreateDiscipline(disciplineConstant, CreateDiscipline.Discipline, disciplines, _client);
			CreateDiscipline = DisciplineMainModel.CreateDefault();
			Disciplines.Add(newDiscipline);
		}

		private async Task CopyDisciplineAsync()
		{
			if (Extensions.IsRun(SelectedDiscipline.Discipline.DisciplineType))
			{
				var disciplines = Disciplines.ToList();
				var newDiscipline = await ScheduleExtension.CreateChildDiscipline(SelectedDiscipline, disciplines, _client);
				Disciplines.Add(newDiscipline);
			}
		}

		private void OnDisciplineSelectionChanged(EditorMode editorMode)
		{
			if (editorMode == EditorMode.Create)
			{
				if (CreateDisciplineComboBoxSelectedItem == null)
					return;
				DisciplineConstant disciplineConstantCreate =
					Constants.DisciplineConstants[DisciplineConstants.IndexOf(CreateDisciplineComboBoxSelectedItem)];
				var isRunCreate = Extensions.IsRun(disciplineConstantCreate.DisciplineType);
				OrderIsEnabled = isRunCreate;
				if (OrderIsEnabled == false)
					CreateDiscipline.Discipline.Order = null;
			}
			else if (editorMode == EditorMode.Edit)
			{
				if (DisciplineComboBoxSelectedItem == null)
					return;
				DisciplineConstant disciplineConstant = Constants.DisciplineConstants[DisciplineConstants.IndexOf(DisciplineComboBoxSelectedItem)];
				var isRun = Extensions.IsRun(disciplineConstant.DisciplineType);
				OrderIsEnabled = isRun;
				if (OrderIsEnabled == false)
					SelectedDiscipline.Discipline.Order = null;
			}
		}

		private async Task LoadDisciplinesAsync()
		{
			Disciplines.Clear();
			using var call = _client.GetDisciplineStream(new EmptyDisciplineRequest());
			await foreach (var discipline in ScheduleProtoExtension.DownloadDisciplinesAsync(call))
			{
				Disciplines.Add(discipline);
			}

			// await Task.Run(async () =>
			// {
			// 	await foreach (var discipline in ScheduleProtoExtension.DownloadDisciplinesAsync(call))
			// 	{
			// 			Disciplines.Add(discipline);
			// 	}
			// });
		}

		private async Task DeleteDisciplineAsync()
		{
			if (SelectedDiscipline.Discipline.CompetitorsCount == 0)
			{
				var result = MessageBox.Show("Chcete tuto disciplínu smazat?", "Info", MessageBoxButton.YesNo, MessageBoxImage.Warning);
				if (result == MessageBoxResult.Yes)
				{
					if (SelectedDiscipline.Discipline.Id == SelectedDiscipline.Discipline.MainId)
					{
						if (await ScheduleProtoExtension.ChildDisciplinesCount(SelectedDiscipline.Discipline.MainId) != 1)
						{
							MessageBox.Show("Nejdříve smažte ostatní běhy.", "Info", MessageBoxButton.OK, MessageBoxImage.Error);
							return;
						}
					}

					await _client.DeleteDisciplineAsync(SelectedDiscipline.Discipline.ToDisciplineDto());
					// https://entityframeworkcore.com/knowledge-base/50310892/how-to-delete-row-with-1-1-relation-to-the-same-table-
					Disciplines.Remove(SelectedDiscipline);
				}
			}
			else
			{
				MessageBox.Show("Disciplínu nelze smazat, protože obsahuje závodníky.", "Info", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		private async Task EditDisciplineAsync()
		{
			IsDialogHostOpen = false;
			DisciplineConstant disciplineConstant = Constants.DisciplineConstants[DisciplineConstants.IndexOf(DisciplineComboBoxSelectedItem!)];
			SelectedDiscipline.Discipline.Name = disciplineConstant.Name;
			SelectedDiscipline.Discipline.DisciplineType = disciplineConstant.DisciplineType;
			SelectedDiscipline.Discipline.Start = TimeSpan.FromTicks(SelectedDiscipline.StartTime.Ticks);
			await _client.EditDisciplineAsync(SelectedDiscipline.Discipline.ToDisciplineDto());
		}
	}
}
