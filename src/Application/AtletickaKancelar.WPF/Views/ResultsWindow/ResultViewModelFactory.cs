﻿using AtletickaKancelar.WPF.Views.WindowManagement.Factories;

namespace AtletickaKancelar.WPF.Views.ResultsWindow
{
	public class ResultViewModelFactory : IViewModelFactory<ResultViewModel>
	{
		public ResultViewModel CreateViewModel()
		{
			return new ResultViewModel();
		}
	}
}