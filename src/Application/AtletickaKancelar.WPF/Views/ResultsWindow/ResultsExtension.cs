﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.Domain.Enums;
using AtletickaKancelar.Domain.Models;
using AtletickaKancelar.WPF.Infrastructure;
using AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions;
using AtletickaKancelar.WPF.Models;

namespace AtletickaKancelar.WPF.Views.ResultsWindow
{
	public class ResultsExtension
	{
		private int _maxBody;
		private List<float> averages = new List<float>();
		private List<int> averagesUsed = new List<int>();

		public async Task<ObservableCollection<SpojkaMainModel>> ComputeResults(List<SpojkaMainModel> spojkyForResults,
		                                                                        DisciplineMainModel selectedDiscipline)
		{
			var spojky = new List<SpojkaMainModel>();
			SetMaxBody(selectedDiscipline);
			try
			{
				var spojkyForPoints = await Task.Run(() => UrciPoradiAsync(spojkyForResults, selectedDiscipline));
				spojky = await Task.Run(() => UdelBody(spojkyForPoints)); // TODO : only Where(GetsScore)
				await SpojkaProtoExtension.UpdateSpojky(spojky);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message + "\nZkontrolujte pomocné pořadí");
			}

			return new ObservableCollection<SpojkaMainModel>(spojky.Where(x => x.Spojka.DisciplineId == selectedDiscipline.Discipline.Id)
			                                                       .OrderByDescending(x => x.Spojka.Order.HasValue).ThenBy(x => x.Spojka.Order));
		}

		private void SetMaxBody(DisciplineMainModel selectedDiscipline)
		{
			string[] kategorie = {"Dorostenci", "Dorostenky", "Junioři", "Juniorky", "Muži", "Ženy"};
			for (int y = 0; y < 6; y++)
			{
				if (selectedDiscipline.Discipline.Category.Contains(kategorie[y]))
				{
					_maxBody = 13;
					break;
				}

				_maxBody = 10;
			}
		}

		private async Task<List<SpojkaMainModel>> UrciPoradiAsync(List<SpojkaMainModel> spojkyForResults, DisciplineMainModel selectedDiscipline)
		{
			List<SpojkaMainModel> spojkyForOrder;

			#region Spojeni s ostatnimi disciplinami

			List<int> activeClubs;
			if (selectedDiscipline.IsRun)
			{
				activeClubs = await ClubProtoExtension.GetActiveClubsId(selectedDiscipline.IsRun,
					selectedDiscipline.Discipline.MainId!.Value); // replace for aktivnioddily

				//copy spojky from actual discipline and child disciplines
				var downloadSpojkyByMainId =
					await SpojkaProtoExtension.DownloadSpojkyByMainIdWithoudDiscipline((int) selectedDiscipline.Discipline.MainId,
						selectedDiscipline.Discipline.Id);
				spojkyForOrder = downloadSpojkyByMainId.Select(x => new SpojkaMainModel(x)).ToList();
				spojkyForOrder.AddRange(spojkyForResults);
			}
			else
			{
				activeClubs = await ClubProtoExtension.GetActiveClubsId(false, selectedDiscipline.Discipline.Id); // replace for aktivnioddily
				//copy spojky from actual discipline
				spojkyForOrder = spojkyForResults;
			}

			#endregion

			#region Vynulovani predeslych dat

			for (var i = 0; i < spojkyForOrder.Count; i++)
			{
				spojkyForOrder[i].Spojka.Points = null;
				spojkyForOrder[i].Spojka.Order = null;
				if (spojkyForOrder[i].Spojka.PenalizationType == PenalizationType.None && spojkyForOrder[i].Spojka.Result != null)
					spojkyForOrder[i].GetsScore = true;
				else
					spojkyForOrder[i].GetsScore = false;
			}

			#endregion

			#region Odhozeni zavodniku

			int pocetBodovanych = 2; // bodovani zavodnici
			if (selectedDiscipline.Discipline.DisciplineType == DisciplineType.Relay)
				pocetBodovanych = 1;
			for (int i = 0; i < activeClubs.Count; i++)
			{
				List<SpojkaMainModel> vyber = spojkyForOrder.FindAll(x => x.Spojka.ClubId == activeClubs[i] && x.GetsScore);
				for (int j = 0; j < vyber.Count; j++)
				{
					if (vyber.Count > pocetBodovanych)
					{
						List<SpojkaMainModel> sortedVyber;
						if (!selectedDiscipline.IsRun)
							sortedVyber = vyber.OrderBy(n => n.Spojka.Result).ThenByDescending(x => x.Spojka.SecondOrder).Reverse().ToList();
						else
							sortedVyber = vyber.OrderBy(n => n.Spojka.Result).ThenByDescending(x => x.Spojka.SecondOrder).ToList();
						spojkyForOrder.Where(x => x.Spojka.Id == sortedVyber[^1].Spojka.Id).ToList().ForEach(s => s.GetsScore = false);
						vyber.Remove(sortedVyber[^1]);
						if (vyber.Count == pocetBodovanych)
							break;
					}
					else
						break;
				}
			}

			#endregion

			#region Poradi

			List<SpojkaMainModel> sorter = spojkyForOrder.OrderByDescending(n => n.Spojka.Result.HasValue).ThenBy(n => n.Spojka.Result).ToList();
			spojkyForOrder = sorter;
			if (selectedDiscipline.IsRun == false)
				spojkyForOrder.Reverse();
			List<float?> listVykonu = new List<float?>(); //vyhodnocené výkony
			List<int> poziceI = new List<int>(); //počítání průměru
			var place = 0;
			var counter = 0;
			var iJeMax = false;
			for (int i = 0; i < spojkyForOrder.Count; i++)
			{
				if (spojkyForOrder[i].GetsScore)
				{
					if (spojkyForOrder[i].Spojka.Result.HasValue && !listVykonu.Contains(spojkyForOrder[i].Spojka.Result))
					{
						place = place + 1 + counter;
						var secOrder = spojkyForOrder[i].Spojka.SecondOrder;
						if (secOrder.HasValue && secOrder.Value != 0)
						{
							var pplace = place + secOrder.Value - 1;
							spojkyForOrder.Where(x => x.Spojka.Id == spojkyForOrder[i].Spojka.Id && x.GetsScore).ToList()
							              .ForEach(s => s.Spojka.Order = pplace);
						}
						else
						{
							var spojkaOrder = place;
							spojkyForOrder.Where(x => x.Spojka.Id == spojkyForOrder[i].Spojka.Id && x.GetsScore).ToList()
							              .ForEach(s => s.Spojka.Order = spojkaOrder);
						}

						listVykonu.Add(spojkyForOrder[i].Spojka.Result);
						counter = 0;
						if (poziceI.Count > 0)
						{
							CalculateAverage(poziceI);
							poziceI.Clear();
						}
					}
					else if (spojkyForOrder[i].Spojka.Result.ToString() != "")
					{
						var secOrder = spojkyForOrder[i].Spojka.SecondOrder;
						if (secOrder.HasValue && secOrder.Value != 0)
						{
							var pplace = place + secOrder.Value - 1;
							spojkyForOrder.Where(x => x.Spojka.Id == spojkyForOrder[i].Spojka.Id && x.GetsScore).ToList()
							              .ForEach(s => s.Spojka.Order = pplace);
						}
						else
						{
							var spojkaOrder = place;
							spojkyForOrder.Where(x => x.Spojka.Id == spojkyForOrder[i].Spojka.Id && x.GetsScore).ToList()
							              .ForEach(s => s.Spojka.Order = spojkaOrder);
						}

						if (spojkyForOrder[i].Spojka.SecondOrder == 0) //brzda průměru, když mají závodníci pomocné pořadí
						{
							if (counter == 0)
							{
								poziceI.Add(place);
								poziceI.Add(place + 1);
								if (i == spojkyForOrder.Count - 1)
									iJeMax = true;
							}
							else
							{
								poziceI.Add(place + counter + 1);
								if (i == spojkyForOrder.Count - 1)
									iJeMax = true;
							}
						}

						counter++;
					}
				}
				else if (poziceI.Count > 0)
				{
					CalculateAverage(poziceI);
					poziceI.Clear();
				}
			}

			if (iJeMax)
			{
				CalculateAverage(poziceI);
				poziceI.Clear();
			}

			return spojkyForOrder;

			#endregion
		}

		private void CalculateAverage(List<int> poziceI)
		{
			float bodovyPrumer = 0;
			averagesUsed.Add(poziceI.Count);
			for (int y = 0; y < poziceI.Count; ++y)
			{
				if (poziceI[y] == 1)
					bodovyPrumer += _maxBody;
				else
					bodovyPrumer += _maxBody - poziceI[y];
			}

			bodovyPrumer /= poziceI.Count;
			averages.Add(bodovyPrumer);
		}

		private List<SpojkaMainModel> UdelBody(List<SpojkaMainModel> spojkyForPoints)
		{
			// TODO : compute points without spojky where GetsScore is false
			int pozicePrumeru = 0;
			for (int i = 0; i < spojkyForPoints.Count; i++)
			{
				int? place = spojkyForPoints[i].Spojka.Order;
				int placeMinus;
				if (i - 1 >= 0)
					if (spojkyForPoints[i - 1].GetsScore)
						placeMinus = (int) spojkyForPoints[i - 1].Spojka.Order!; // Hodnota byla jiz urcena
					else
						placeMinus = 0;
				else
					placeMinus = 0;
				int placePlus;
				if (i + 1 <= spojkyForPoints.Count - 1)
					if (spojkyForPoints[i + 1].GetsScore)
						placePlus = (int) spojkyForPoints[i + 1].Spojka.Order!;
					else
						placePlus = 0;
				else
					placePlus = 0;
				if (place == placeMinus || place == placePlus)
				{
					if (place == 1)
					{
						var index = pozicePrumeru;
						spojkyForPoints.Where(x => x.Spojka.Id == spojkyForPoints[i].Spojka.Id).ToList()
						               .ForEach(s => s.Spojka.Points = averages[index]);
					}
					else if (place >= _maxBody)
						spojkyForPoints.Where(x => x.Spojka.Id == spojkyForPoints[i].Spojka.Id).ToList().ForEach(s => s.Spojka.Points = 0);
					else
					{
						var index = pozicePrumeru;
						spojkyForPoints.Where(x => x.Spojka.Id == spojkyForPoints[i].Spojka.Id).ToList()
						               .ForEach(s => s.Spojka.Points = averages[index]);
					}

					if (averagesUsed[0] != 1)
					{
						averagesUsed[0] -= 1;
					}
					else
					{
						averagesUsed.RemoveAt(0);
						pozicePrumeru++;
					}
				}
				else // správně
				{
					if (place == 1)
						spojkyForPoints.Where(x => x.Spojka.Id == spojkyForPoints[i].Spojka.Id).ToList().ForEach(s => s.Spojka.Points = _maxBody);
					else if (place >= _maxBody)
						spojkyForPoints.Where(x => x.Spojka.Id == spojkyForPoints[i].Spojka.Id).ToList().ForEach(s => s.Spojka.Points = 0);
					else
					{
						var index = i;
						spojkyForPoints.Where(x => x.Spojka.Id == spojkyForPoints[i].Spojka.Id).ToList()
						               .ForEach(s => s.Spojka.Points = _maxBody - spojkyForPoints[index].Spojka.Order);
					}
				}
			}

			averagesUsed.Clear();
			averages.Clear();
			return spojkyForPoints;
		}

		public async Task<ObservableCollection<SpojkaMainModel>> ImportResultFromCameraAndComputeThem(
			List<SpojkaMainModel> spojkyForResults, DisciplineMainModel selectedDiscipline)
		{
			var activeRace = await RaceProtoExtension.GetActiveRace();
			var path = $@"{Constants.WorkingDirectory}\{activeRace.Id}\{selectedDiscipline.Discipline.CameraId}.txt";
			var importedResults = await ImportResults(path);
			foreach (var cameraResult in importedResults)
			{
				spojkyForResults.Where(x => x.Spojka.DisciplineId == selectedDiscipline.Discipline.Id && x.Spojka.Track == cameraResult.Lane)
				                .ToList().ForEach(s => s.Spojka.Result = cameraResult.Time);
			}

			List<SpojkaMainModel> spojky = new List<SpojkaMainModel>();

			SetMaxBody(selectedDiscipline);
			try
			{
				var spojkyForPoints = await Task.Run(() => UrciPoradiAsync(spojkyForResults, selectedDiscipline));
				spojky = await Task.Run(() => UdelBody(spojkyForPoints));
				await SpojkaProtoExtension.UpdateSpojky(spojky);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

			return new ObservableCollection<SpojkaMainModel>(spojky.Where(x => x.Spojka.DisciplineId == selectedDiscipline.Discipline.Id)
			                                                       .OrderByDescending(x => x.Spojka.Order.HasValue).ThenBy(x => x.Spojka.Order));
		}

		private static async Task<List<CameraResult>> ImportResults(string path)
		{
			var results = new List<CameraResult>();
			const NumberStyles style = NumberStyles.Number;
			var culture = CultureInfo.CreateSpecificCulture("en-US");
			await Task.Run(() =>
			{
				try
				{
					var readedLines = File.ReadAllLines(path);
					var lines = readedLines.ToList();
					for (var a = 2; a < lines.Count; a++)
					{
						var drtic = lines[a].Split('\t');
						// int.TryParse(drtic[0], out _);
						int.TryParse(drtic[1], out var lane);
						float.TryParse(drtic[2], style, culture, out var time); // There was float? before, check possible camera outputs
						results.Add(new CameraResult(lane, time));
					}
				}
				catch
				{
					// ignored
				}
			});
			return results;
		}

		public async Task<List<ClubPoints>> GetClubPoints(int raceId)
		{
			var clubPointsSpojky = await ClubProtoExtension.GetPoints(raceId);
			var clubPoints = new List<ClubPoints>();
			var clubIds = new List<int>();
			for (int i = 0; i < clubPointsSpojky.Count; i++) // Get Clubs
			{
				if (!clubIds.Contains(clubPointsSpojky[i].ClubId))
				{
					clubIds.Add(clubPointsSpojky[i].ClubId);
					clubPoints.Add(new ClubPoints(clubPointsSpojky[i].ClubId, clubPointsSpojky[i].ClubName, 0, 0));
				}
			}

			for (int i = 0; i < clubPoints.Count; i++) // Get points
			{
				List<ClubPoints> selectedByClubId = clubPointsSpojky.Where(x => x.ClubId == clubPoints[i].ClubId).ToList();
				for (int j = 0; j < selectedByClubId.Count; j++)
				{
					clubPoints[i].Points += selectedByClubId[j].Points;
				}
			}

			return SortClubPoints(clubPoints);
		}

		public List<ClubPoints> SortClubPoints(List<ClubPoints> clubPoints)
		{
			int place = 0;
			int counter = 0;
			clubPoints = clubPoints.OrderByDescending(x => x.Points).ToList();
			List<float?> points = new List<float?>();
			for (int i = 0; i < clubPoints.Count; i++)
			{
				if (!points.Contains(clubPoints[i].Points))
				{
					place = place + 1 + counter;
					points.Add(clubPoints[i].Points);
					clubPoints[i].Order = place;
					counter = 0;
				}
				else
				{
					counter++;
					clubPoints[i].Order = place;
				}
			}

			return clubPoints;
		}
	}
}
