﻿using System.Linq;
using System.Windows.Controls;
using AtletickaKancelar.Domain.Entities;

namespace AtletickaKancelar.WPF.Views.ResultsWindow
{
	public partial class ResultView : UserControl
	{
		public ResultView()
		{
			InitializeComponent();
			_resultViewModel = (ResultViewModel) DataContext;
		}

		private readonly ResultViewModel _resultViewModel;

		private void ClubPointsListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			_resultViewModel.SelectedClubPoints = ClubPointsListView.SelectedItems.Cast<ClubPoints>().ToList();
		}
	}
}
