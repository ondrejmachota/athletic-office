﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.Domain.Enums;
using AtletickaKancelar.WPF.Infrastructure;
using AtletickaKancelar.WPF.Infrastructure.GrpcProtoExtensions;
using AtletickaKancelar.WPF.Models;
using AtletickaKancelar.WPF.Views.Shared;
using AtletickaKancelar.WPF.Views.WindowManagement;
using GrpcServer;

namespace AtletickaKancelar.WPF.Views.ResultsWindow
{
	public class ResultViewModel : ViewModelBase
	{
		// http://www.shujaat.net/2011/02/wpf-binding-converter-parameter.html
		private bool _isResultsButtonsEnable;
		private bool _isCameraLoadEnable;
		private string _cameraIdText = "";
		private SpojkaMainModel _selectedSpojka = null!;
		private DisciplinesListViewViewModel _disciplinesListViewViewModel = null!;
		private readonly ResultsExtension _resultsExtension = new ResultsExtension();

		public ObservableCollection<SpojkaMainModel> Spojky { get; set; } = new ObservableCollection<SpojkaMainModel>();

		public ObservableCollection<ClubPoints> ClubPoints { get; set; } = new ObservableCollection<ClubPoints>();

		public ICommand LoadCommand { get; }
		public ICommand DisciplineSelectionChangedCommand { get; }
		public ICommand SendResultsToDb { get; }
		public ICommand ComputeResults { get; }
		public ICommand ImportResultsFromCamera { get; }
		public ICommand Print { get; }
		public ICommand CancelPrintCommand { get; }
		public ICommand PenalizationChanged { get; }
		public ICommand GetPointsCommand { get; }
		public ICommand PrintClubPointsCommand { get; }

		public ResultViewModel()
		{
			LoadCommand = new Command(() => DisciplinesListViewViewModel = new DisciplinesListViewViewModel());
#pragma warning disable 4014
			DisciplineSelectionChangedCommand = new Command(() => LoadSpojky());
			SendResultsToDb = new Command(() => SpojkaProtoExtension.UpdateSpojky(Spojky.ToList()));
			ComputeResults = new Command(() => ComputeResultsMethod());
			ImportResultsFromCamera = new Command(() => ImportAndComputeResultsMethod());
			Print = new Command(() => PrintMethod());
			CancelPrintCommand = new Command(CancelPrint);
			PenalizationChanged = new Command<PenalizationType>(param => Penalize((PenalizationType) param));
			GetPointsCommand = new Command(() => LoadClubPoints());
			PrintClubPointsCommand = new Command(() => PrintClubPoints());
#pragma warning restore 4014
		}

		public DisciplinesListViewViewModel DisciplinesListViewViewModel
		{
			get => _disciplinesListViewViewModel;
			set => SetAndRaise(ref _disciplinesListViewViewModel, value);
		}

		public List<ClubPoints> SelectedClubPoints = new List<ClubPoints>();

		public SpojkaMainModel SelectedSpojka
		{
			get => _selectedSpojka;
			set => SetAndRaise(ref _selectedSpojka, value);
		}

		public bool IsResultsButtonsEnable
		{
			get => _isResultsButtonsEnable;
			set => SetAndRaise(ref _isResultsButtonsEnable, value);
		}

		public bool IsCameraLoadEnable
		{
			get => _isCameraLoadEnable;
			set => SetAndRaise(ref _isCameraLoadEnable, value);
		}

		public string CameraIdText
		{
			get => _cameraIdText;
			set => SetAndRaise(ref _cameraIdText, value);
		}

		private async Task LoadSpojky()
		{
			if (DisciplinesListViewViewModel.SelectedDiscipline != null)
			{
				IsResultsButtonsEnable = true;
				IsCameraLoadEnable = DisciplinesListViewViewModel.SelectedDiscipline.IsRun;
				CameraIdText = (IsCameraLoadEnable ? DisciplinesListViewViewModel.SelectedDiscipline.Discipline.CameraId.ToString() : "")!;
				await LoadSpojkyByDisciplineId(DisciplinesListViewViewModel.SelectedDiscipline.Discipline.Id).ConfigureAwait(false);
			}
			else
			{
				IsResultsButtonsEnable = false;
				IsCameraLoadEnable = false;
				CameraIdText = "";
			}
		}

		private async Task LoadSpojkyByDisciplineId(int id)
		{
			var client = SpojkaProtoExtension.Create();
			Spojky.Clear();
			using var call = client.GetSpojkaByDisciplineIdSteam(new DisciplineIdRequest {Id = id});
			await foreach (var spojka in SpojkaProtoExtension.DownloadSpojkyAsync(call))
			{
				Spojky.Add(spojka);
			}
		}

		private async Task ComputeResultsMethod()
		{
			Spojky = await Task.Run(() => _resultsExtension.ComputeResults(Spojky.ToList(), DisciplinesListViewViewModel.SelectedDiscipline!));
		}

		private async Task ImportAndComputeResultsMethod()
		{
			Spojky = await _resultsExtension.ImportResultFromCameraAndComputeThem(Spojky.ToList(), DisciplinesListViewViewModel.SelectedDiscipline!);
		}

		private async Task PrintMethod()
		{
			await Printer.PrintRegOrResl(DisciplinesListViewViewModel.Disciplines, false);
		}

		private async Task PrintClubPoints()
		{
			if (!SelectedClubPoints.Any())
				SelectedClubPoints = ClubPoints.ToList();
			else
				_resultsExtension.SortClubPoints(SelectedClubPoints);
			await HtmlCreator.CreateSaveOpenAllResults(SelectedClubPoints);
		}

		private void CancelPrint()
		{
			for (int i = 0; i < DisciplinesListViewViewModel.Disciplines.Count; i++)
			{
				DisciplinesListViewViewModel.Disciplines[i].IsSelected = false;
			}
		}

		private async Task Penalize(PenalizationType penalization)
		{
			SelectedSpojka.Spojka.PenalizationType = penalization;
			if (SelectedSpojka.Spojka.PenalizationType != PenalizationType.None)
			{
				SelectedSpojka.Spojka.Order = null;
				SelectedSpojka.Spojka.Points = null;
				SelectedSpojka.Spojka.Result = null;
			}
			await SpojkaProtoExtension.UpdateSpojka(SelectedSpojka);
		}

		private async Task LoadClubPoints()
		{
			var activeRace = await RaceProtoExtension.GetActiveRace();
			ClubPoints = new ObservableCollection<ClubPoints>(await _resultsExtension.GetClubPoints(activeRace.Id));
		}
	}
}
