﻿using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.WPF.Models;
using Google.Protobuf.WellKnownTypes;
using GrpcServer;

namespace AtletickaKancelar.WPF.Converters
{
	public static class AkConverter
	{
		public static RaceDto ToRaceDto(this RaceFv race)
		{
			return new RaceDto
			{
				Id = race.Id,
				Title = race.Title,
				IsActive = race.IsActive,
				Place = race.Place,
				Date = Timestamp.FromDateTime(race.Date.AddHours(2).ToUniversalTime()),
				Note = race.Note
			};
		}

		public static RaceFv ToRaceFv(this RaceDto raceDto)
		{
			return new RaceFv(raceDto);
		}

		public static DisciplineModel ToDisciplineModel(this DisciplineDto disciplineDto)
		{
			return new DisciplineModel(disciplineDto);
		}

		public static DisciplineDto ToDisciplineDto(this DisciplineModel discipline)
		{
			return new DisciplineDto
			{
				Id = discipline.Id,
				RaceId = discipline.RaceId,
				Name = discipline.Name,
				Category = discipline.Category,
				DisciplineType = (int) discipline.DisciplineType,
				PhaseType = (int) discipline.PhaseType,
				MainId = discipline.MainId,
				Start = Duration.FromTimeSpan(discipline.Start),
				CameraId = discipline.CameraId,
				Order = discipline.Order,
				IsRegistrationsPrinted = discipline.IsRegistrationsPrinted,
				IsResultsPrinted = discipline.IsResultsPrinted,
				CompetitorsCount = discipline.CompetitorsCount
			};
		}

		public static CompetitorModel ToCompetitorModel(this CompetitorDto competitorDto)
		{
			return new CompetitorModel(competitorDto);
		}

		public static CompetitorDto ToCompetitorDto(this CompetitorModel competitor)
		{
			return new CompetitorDto
			{
				Id = competitor.Id,
				ClubId = competitor.ClubId,
				FirstName = competitor.FirstName,
				LastName = competitor.LastName,
				Sex = (int) competitor.Sex,
				BirthDate = Timestamp.FromDateTime(competitor.BirthDate.AddHours(2).ToUniversalTime()),
				ClubName = competitor.ClubName,
				ClubAbbreviation = competitor.ClubAbbreviation,
			};
		}

		public static SpojkaModel ToSpojkaModel(this SpojkaDto spojkaDto)
		{
			return new SpojkaModel(spojkaDto);
		}

		public static SpojkaDto ToSpojkaDto(this Spojka spojka)
		{
			return new SpojkaDto
			{
				Id = spojka.Id,
				CompetitorId = spojka.CompetitorId,
				DisciplineId = spojka.DisciplineId,
				Track = spojka.Track,
				BIB = spojka.BIB,
				Result = spojka.Result,
				Order = spojka.Order,
				SecondOrder = spojka.SecondOrder,
				Points = spojka.Points,
				PenalizationType = (int) spojka.PenalizationType,
				MainId = spojka.MainId,
				ClubId = spojka.ClubId,
				FullName = spojka.FullName,
				ClubAbbreviation = spojka.ClubAbbreviation,
				BirthDate = spojka.BirthDate,
			};
		}

		public static SpojkaDto ToSpojkaDto(this SpojkaModel spojka)
		{
			return new SpojkaDto
			{
				Id = spojka.Id,
				CompetitorId = spojka.CompetitorId,
				DisciplineId = spojka.DisciplineId,
				Track = spojka.Track,
				BIB = spojka.BIB,
				Result = spojka.Result,
				Order = spojka.Order,
				SecondOrder = spojka.SecondOrder,
				Points = spojka.Points,
				PenalizationType = (int) spojka.PenalizationType,
				MainId = spojka.MainId,
				ClubId = spojka.ClubId,
				FullName = spojka.FullName,
				ClubAbbreviation = spojka.ClubAbbreviation,
				BirthDate = spojka.BirthDate,
			};
		}

		public static Club ToClub(this ClubDto clubDto)
		{
			return new Club
			{
				Id = clubDto.Id,
				Name = clubDto.Name,
				Abbreviation = clubDto.Abbreviation,
			};
		}

		public static ClubDto ToClubDto(this Club club)
		{
			return new ClubDto
			{
				Id = club.Id,
				Name = club.Name,
				Abbreviation = club.Abbreviation,
			};
		}

		public static ClubPoints ToClubPoints(this GetPointsResponse clubPoints)
		{
			return new ClubPoints
			{
				ClubId = clubPoints.ClubId,
				ClubName = clubPoints.ClubName,
				Points = clubPoints.Points,
				Order = 0
			};
		}
	}
}
