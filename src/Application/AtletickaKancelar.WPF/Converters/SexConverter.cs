﻿using System;
using System.Globalization;
using System.Windows.Data;
using AtletickaKancelar.Domain.Enums;

namespace AtletickaKancelar.WPF.Converters
{
	public class SexConverter : IValueConverter
	{
		public object Convert(object? value, Type targetType, object parameter, CultureInfo culture)
		{
			return value != null && (Sex) value != Sex.Woman;
		}

		public object ConvertBack(object? value, Type targetType, object parameter, CultureInfo culture)
		{
			return value != null && (bool) value ? Sex.Man : Sex.Woman;
		}
	}
}
