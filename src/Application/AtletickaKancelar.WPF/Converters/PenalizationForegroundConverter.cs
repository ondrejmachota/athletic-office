﻿﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using AtletickaKancelar.Domain.Enums;

namespace AtletickaKancelar.WPF.Converters
{
	public class PenalizationForegroundConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var penalization = (PenalizationType) value;
			return penalization switch
			{
				PenalizationType.None => new SolidColorBrush(Color.FromRgb(87, 245, 51)),
				PenalizationType.DNS => new SolidColorBrush(Color.FromRgb(255, 155, 17)),
				PenalizationType.DNF => new SolidColorBrush(Color.FromRgb(255, 155, 17)),
				PenalizationType.DQ => new SolidColorBrush(Color.FromRgb(255, 46, 33)),
				PenalizationType.NM => new SolidColorBrush(Color.FromRgb(255, 46, 33)),
				_ => new SolidColorBrush(Color.FromRgb(255, 46, 33))
			};
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException(nameof(ConvertBack));
		}
	}
}