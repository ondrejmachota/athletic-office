﻿using System;
using System.Globalization;
using System.Windows.Data;
using AtletickaKancelar.Domain.Enums;

namespace AtletickaKancelar.WPF.Converters
{
	public class ResultValueConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var disciplineType = (DisciplineType)parameter;
			return disciplineType switch
			{
				DisciplineType.Sprint => $"{value:0.000}",
				DisciplineType.MidRun => $"{value:0.00}",
				DisciplineType.LongRun => $"{value:0.00}",
				DisciplineType.Throw => $"{value:0.00}",
				DisciplineType.LongJump => $"{value:0.00}",
				DisciplineType.HighJump => $"{value:0.00}",
				DisciplineType.Relay => $"{value:0.00}",
				DisciplineType.Other => $"{value:0.00}",
				_ => $"{value:0.00}"
			};
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return "AHOJ";
		}
	}
}