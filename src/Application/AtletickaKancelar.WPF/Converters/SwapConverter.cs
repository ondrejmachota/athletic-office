﻿using System;
using System.Globalization;
using System.Windows.Data;
using AtletickaKancelar.Domain.Enums;

namespace AtletickaKancelar.WPF.Converters
{
	public class SwapConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var sex = (Sex) value;
			var newValue = sex == Sex.Man;
			return !newValue;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var newValue = (bool) value;
			return newValue ? Sex.Woman : Sex.Man;
		}
	}
}