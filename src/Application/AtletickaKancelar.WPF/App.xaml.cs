﻿using System;
using System.Windows;
using AtletickaKancelar.Domain.Models;
using AtletickaKancelar.Domain.Settings;
using AtletickaKancelar.WPF.Utilities;
using AtletickaKancelar.WPF.Views.RaceWindow;
using AtletickaKancelar.WPF.Views.RegistrationsWindow;
using AtletickaKancelar.WPF.Views.ResultsWindow;
using AtletickaKancelar.WPF.Views.ScheduleWindow;
using AtletickaKancelar.WPF.Views.SettingsWindow;
using AtletickaKancelar.WPF.Views.WindowManagement;
using AtletickaKancelar.WPF.Views.WindowManagement.Factories;
using AtletickaKancelar.WPF.Views.WindowManagement.Navigators;
using GrpcServer;
using Microsoft.Extensions.DependencyInjection;

namespace AtletickaKancelar.WPF
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		protected override async void OnStartup(StartupEventArgs e)
		{
			var serviceProvider = CreateServiceProvider();
			Options options = await serviceProvider.GetRequiredService<ISettingsStore>().LoadAsync();
			// TODO: Try to connect to database
			ThemeSwitcher.ApplyBase(options.IsDark);
			Window window = serviceProvider.GetRequiredService<MainWindow>();
			window.Show();

			base.OnStartup(e);
		}

		private static IServiceProvider CreateServiceProvider()
		{
			IServiceCollection services = new ServiceCollection();

			services.AddScoped<INavigator, Navigator>();
			// User controls
			services.AddSingleton<IRootViewModelFactory, RootViewModelFactory>();
			services.AddSingleton<IViewModelFactory<RaceViewModel>, RaceViewModelFactory>();
			services.AddSingleton<IViewModelFactory<ScheduleViewModel>, ScheduleViewModelFactory>();
			services.AddSingleton<IViewModelFactory<RegistrationViewModel>, RegistrationViewModelFactory>();
			services.AddSingleton<IViewModelFactory<ResultViewModel>, ResultViewModelFactory>();
			services.AddSingleton<IViewModelFactory<SettingsViewModel>, SettingsViewModelFactory>();
			services.AddScoped<MainWindowViewModel>();
			services.AddScoped(s => new MainWindow(s.GetRequiredService<MainWindowViewModel>()));

			services.AddSingleton<ISettingsStore, SettingsStore>();

			services.AddSingleton<RaceGrpcProtoService.RaceGrpcProtoServiceClient>();
			services.AddSingleton<DisciplineGrpcProtoService.DisciplineGrpcProtoServiceClient>();
			services.AddSingleton<CompetitorGrpcProtoService.CompetitorGrpcProtoServiceClient>();
			services.AddSingleton<ClubGrpcProtoService.ClubGrpcProtoServiceClient>();
			services.AddSingleton<SpojkaGrpcProtoService.SpojkaGrpcProtoServiceClient>();

			return services.BuildServiceProvider();
		}
	}
}
