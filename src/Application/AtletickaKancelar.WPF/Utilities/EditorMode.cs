﻿namespace AtletickaKancelar.WPF.Utilities
{
	public enum EditorMode
	{
		Create,
		Edit
	}
	
	public enum EditorModeRegistrations
	{
		CreateCompetitor,
		EditCompetitor,
		CreateClub
	}
}