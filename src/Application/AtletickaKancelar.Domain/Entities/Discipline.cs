﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using AtletickaKancelar.Domain.Enums;

namespace AtletickaKancelar.Domain.Entities
{
	public class Discipline : DomainObject
	{
		#region Contructors
		public Discipline()
		{

		}

		public Discipline(Discipline discipline, int competitorsCount)
		{
			Id = discipline.Id;
			RaceId = discipline.RaceId;
			Name = discipline.Name;
			Category = discipline.Category;
			DisciplineType = discipline.DisciplineType;
			PhaseType = discipline.PhaseType;
			MainId = discipline.MainId;
			Start = discipline.Start;
			CameraId = discipline.CameraId;
			Order = discipline.Order;
			IsRegistrationsPrinted = discipline.IsRegistrationsPrinted;
			IsResultsPrinted = discipline.IsResultsPrinted;
			CompetitorsCount = competitorsCount;
		}

		public Discipline(int raceId, string name, string category, DisciplineType disciplineType, PhaseType phaseType, TimeSpan start)
		{
			RaceId = raceId;
			Name = name;
			Category = category;
			DisciplineType = disciplineType;
			PhaseType = phaseType;
			Start = start;
		}

		public Discipline(int raceId, string name, string category, DisciplineType disciplineType, PhaseType phaseType, TimeSpan start, int? cameraId, int? order)
		{
			RaceId = raceId;
			Name = name;
			Category = category;
			DisciplineType = disciplineType;
			PhaseType = phaseType;
			Start = start;
			CameraId = cameraId;
			Order = order;
		}

		// Constructor for child discipline
		public Discipline(int raceId, string name, string category, DisciplineType disciplineType, PhaseType phaseType, int? mainId, TimeSpan start, int? cameraId, int? order)
		{
			RaceId = raceId;
			Name = name;
			Category = category;
			DisciplineType = disciplineType;
			PhaseType = phaseType;
			MainId = mainId;
			Start = start;
			CameraId = cameraId;
			Order = order;
		}
		#endregion
		
		public int RaceId { get; set; }

		public Race Race { get; set; } = null!;

		public string Name { get; set; } = null!;

		public string Category { get; set; } = null!;

		public DisciplineType DisciplineType { get; set; }

		public PhaseType PhaseType { get; set; }

		public int? MainId { get; set; }
		public Discipline MainDiscipline { get; set; } = null!;

		public ICollection<Discipline> ChildDisciplies { get; set; } = null!;

		public ICollection<Spojka> Spojky { get; set; } = null!;

		public TimeSpan Start { get; set; }

		public int? CameraId { get; set; }

		public int? Order { get; set; }

		public bool IsRegistrationsPrinted { get; set; }

		public bool IsResultsPrinted { get; set; }
		
		[NotMapped] 
		public int CompetitorsCount { get; set; }
	}
}
