﻿using System;
using System.Collections.Generic;
using AtletickaKancelar.Domain.Enums;

namespace AtletickaKancelar.Domain.Entities
{
	public class Competitor : DomainObject
	{
		public int ClubId { get; set; }

		public Club Club { get; set; } = null!;

		public string FirstName { get; set; } = null!;

		public string LastName { get; set; } = null!;

		public Sex Sex { get; set; }

		public DateTime BirthDate { get; set; }

		public string ClubName { get; set; } = null!;

		public string ClubAbbreviation { get; set; } = null!;

		public ICollection<Spojka> Spojky { get; set; } = null!;
	}
}
