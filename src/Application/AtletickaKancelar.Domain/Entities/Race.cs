﻿using System;
using System.Collections.Generic;

namespace AtletickaKancelar.Domain.Entities
{
	public class Race : DomainObject
	{
		public string Title { get; set; } = null!;

		public ICollection<Discipline> Disciplines { get; set; } = null!;

		public bool IsActive { get; set; }

		public string Place { get; set; } = null!;

		public DateTime Date { get; set; }

		public string Note { get; set; } = null!;
}
}
