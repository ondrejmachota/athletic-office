﻿using System.Collections.Generic;

namespace AtletickaKancelar.Domain.Entities
{
	public class Club : DomainObject
	{
		public string Name { get; set; } = null!;

		public string Abbreviation { get; set; } = null!;

		public ICollection<Competitor> Competitors { get; set; } = null!;

		public override string ToString()
		{
			return Name;
		}
	}
}
