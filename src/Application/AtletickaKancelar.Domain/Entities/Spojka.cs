﻿using System.ComponentModel.DataAnnotations.Schema;
using AtletickaKancelar.Domain.Enums;

namespace AtletickaKancelar.Domain.Entities
{
	public class Spojka : DomainObject
	{
		public int CompetitorId { get; set; }

		public Competitor Competitor { get; set; } = null!;

		public int DisciplineId { get; set; }

		public Discipline Discipline { get; set; } = null!;

		public int? Track { get; set; }

		public int? BIB { get; set; }

		public float? Result { get; set; }

		public int? Order { get; set; }

		public int? SecondOrder { get; set; }

		public float? Points { get; set; }

		public PenalizationType PenalizationType { get; set; }

		public int? MainId { get; set; }

		#region NOT MAPPED
		[NotMapped]
		public int ClubId { get; set; }
		[NotMapped]
		public string FullName { get; set; } = null!;
		[NotMapped]
		public string ClubAbbreviation { get; set; } = null!;
		[NotMapped]
		public string BirthDate { get; set; } = null!;
		#endregion
	}
}
