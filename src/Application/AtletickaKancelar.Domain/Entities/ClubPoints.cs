﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AtletickaKancelar.Domain.Entities
{
	public class ClubPoints
	{
		public int ClubId { get; set; }
		[NotMapped] public string ClubName { get; set; } = null!;
		public int Order { get; set; }
		public float? Points { get; set; }

		public ClubPoints(int clubId, string clubName, int order, float? points)
		{
			ClubId = clubId;
			ClubName = clubName;
			Order = order;
			Points = points;
		}

		public ClubPoints()
		{
		}
	}
}
