﻿namespace AtletickaKancelar.Domain.Models
{
    public class CameraResult
    {
        public readonly int Lane;
        public readonly float Time;

        public CameraResult(int lane, float time)
        {
            Lane = lane;
            Time = time;
        }
    }
}
