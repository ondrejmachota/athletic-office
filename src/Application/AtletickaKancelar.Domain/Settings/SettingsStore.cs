﻿using System.IO;
using System.Threading.Tasks;
using AtletickaKancelar.Domain.Extensions;
using AtletickaKancelar.Domain.Models;
using Newtonsoft.Json;

namespace AtletickaKancelar.Domain.Settings
{
	public class SettingsStore : ISettingsStore
	{
		public SettingsStore()
		{
			var directory = new DirectoryInfo(Constants.WorkingDirectory);
			if (!directory.Exists)
				directory.Create();
		}

		public async Task Write(Options options)
		{
			var filepath = Path.Combine(Constants.WorkingDirectory, "appsettings.json");
			var fileText = JsonConvert.SerializeObject(options);
			await File.WriteAllTextAsync(filepath, fileText).ConfigureAwait(false);
		}

		public Options Load()
		{
			var filePath = Path.Combine(Constants.WorkingDirectory, "appsettings.json");
			var file = new FileInfo(filePath);

			if (!file.Exists)
			{
				Write(Constants.Options).ConfigureAwait(false);
				return Constants.Options;
			}

			var fileText = File.ReadAllText(filePath);

			var options = JsonConvert.DeserializeObject<Options>(fileText);
			if (options == null) throw new JsonSerializationException($"Cannot deserialize options from file: {filePath}");
			return options;
		}

		public async Task<Options> LoadAsync()
		{
			var filePath = Path.Combine(Constants.WorkingDirectory, "appsettings.json");
			var file = new FileInfo(filePath);

			if (!file.Exists)
			{
				await Write(Constants.Options).ConfigureAwait(false);
				return Constants.Options;
			}

			var fileText = await File.ReadAllTextAsync(filePath).ConfigureAwait(false);
			
			var options = JsonConvert.DeserializeObject<Options>(fileText);
			if (options == null) throw new JsonSerializationException($"Cannot deserialize options from file: {filePath}");
			return options;
		}
	}
}
