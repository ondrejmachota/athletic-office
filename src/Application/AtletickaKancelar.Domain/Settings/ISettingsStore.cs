﻿using System.Threading.Tasks;
using AtletickaKancelar.Domain.Models;

namespace AtletickaKancelar.Domain.Settings
{
	public interface ISettingsStore
	{
		Task Write(Options options);

		Options Load();

		Task<Options> LoadAsync();
	}
}