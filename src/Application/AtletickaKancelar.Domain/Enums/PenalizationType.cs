﻿namespace AtletickaKancelar.Domain.Enums
{
    public enum PenalizationType
    {
        None = -1,
        DNS,
        DNF,
        DQ,
        NM
    }
}
