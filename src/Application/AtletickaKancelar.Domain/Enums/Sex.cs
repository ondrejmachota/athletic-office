﻿namespace AtletickaKancelar.Domain.Enums
{
    public enum Sex
    {
        Man,
        Woman
    }
}
