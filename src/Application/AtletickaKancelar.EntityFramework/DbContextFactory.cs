﻿using AtletickaKancelar.Domain.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace AtletickaKancelar.EntityFramework
{
    public class DbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext CreateDbContext(string[]? args = null)
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>();
            // TODO : Options not updated, pass shared options service to dbcontext
            options.UseSqlite($"Data Source={Constants.DatabasePath}");

            return new ApplicationDbContext(options.Options);
        }

        public DbContextFactory()
        {
            CreateDbContext().Database.EnsureCreated();
        }
    }
}
