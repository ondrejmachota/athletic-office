﻿using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.Domain.Enums;
using AtletickaKancelar.Domain.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace AtletickaKancelar.EntityFramework
{
	public class ApplicationDbContext : DbContext
	{
		#region DbSets

		public DbSet<Race> Races { get; set; } = null!;

		public DbSet<Discipline> Disciplines { get; set; } = null!;

		public DbSet<Club> Clubs { get; set; } = null!;

		public DbSet<Competitor> Competitors { get; set; } = null!;

		public DbSet<Spojka> Spojky { get; set; } = null!;

		#endregion

		public ApplicationDbContext(DbContextOptions options) : base(options)
		{
		}

		public ApplicationDbContext()
		{
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
			optionsBuilder.UseSqlite($"Data Source={Constants.DatabasePath}");

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);
			builder.Entity<Race>(entity =>
			{
				entity.Property(x => x.Title).IsRequired().HasMaxLength(125);
				entity.Property(x => x.Date).IsRequired();
				entity.Property(x => x.Place).IsRequired().HasMaxLength(125);
				entity.Property(x => x.Note).HasMaxLength(750).IsRequired(false);
				entity.Property(x => x.IsActive).IsRequired().HasDefaultValue(false); // TODO : Change from int32 to tinyint(1) in db 
			});
			EnumToNumberConverter<DisciplineType, int> disciplineTypeConverter = new EnumToNumberConverter<DisciplineType, int>();
			EnumToNumberConverter<PhaseType, int> phaseTypeConverter = new EnumToNumberConverter<PhaseType, int>();
			builder.Entity<Discipline>(entity =>
			{
				entity.HasOne(r => r.Race).WithMany(d => d.Disciplines).HasForeignKey(k => k.RaceId);
				entity.Property(x => x.MainId).IsRequired(false);
				entity.HasMany(x => x.ChildDisciplies).WithOne(x => x.MainDiscipline).HasForeignKey(k => k.MainId).OnDelete(DeleteBehavior.SetNull);
				entity.Property(x => x.Name).HasMaxLength(50).IsRequired();
				entity.Property(x => x.Category).HasMaxLength(50).IsRequired();
				entity.Property(x => x.DisciplineType).HasConversion(disciplineTypeConverter).IsRequired();
				entity.Property(x => x.PhaseType).HasConversion(phaseTypeConverter).IsRequired().HasDefaultValue(PhaseType.None);
				entity.Property(x => x.Start).IsRequired();
				entity.Property(x => x.CameraId).IsRequired(false);
				entity.Property(x => x.Order).IsRequired(false);
				entity.Property(x => x.IsRegistrationsPrinted).HasDefaultValue(false);
				entity.Property(x => x.IsResultsPrinted).HasDefaultValue(false);
			});
			builder.Entity<Club>(entity =>
			{
				entity.Property(x => x.Name).HasMaxLength(100).IsRequired();
				entity.Property(x => x.Abbreviation).HasMaxLength(10).IsRequired();
			});
			builder.Entity<Competitor>(entity =>
			{
				entity.HasOne(c => c.Club).WithMany(c => c.Competitors).HasForeignKey(k => k.ClubId);
				entity.Property(x => x.FirstName).HasMaxLength(40).IsRequired();
				entity.Property(x => x.LastName).HasMaxLength(60).IsRequired();
				entity.Property(x => x.BirthDate).IsRequired();
				entity.Property(x => x.ClubName).HasMaxLength(100);
				entity.Property(x => x.ClubAbbreviation).HasMaxLength(10);
			});
			EnumToNumberConverter<PenalizationType, int> penalizationTypeConverter = new EnumToNumberConverter<PenalizationType, int>();
			builder.Entity<Spojka>(entity =>
			{
				entity.HasOne(c => c.Competitor).WithMany(c => c.Spojky).HasForeignKey(k => k.CompetitorId);
				entity.HasOne(c => c.Discipline).WithMany(c => c.Spojky).HasForeignKey(k => k.DisciplineId);
				entity.Property(x => x.Track).IsRequired(false);
				entity.Property(x => x.BIB).IsRequired(false);
				entity.Property(x => x.Result).IsRequired(false);
				entity.Property(x => x.Order).IsRequired(false);
				entity.Property(x => x.SecondOrder).IsRequired(false).HasDefaultValue(0);
				entity.Property(x => x.Points).IsRequired(false);
				entity.Property(x => x.PenalizationType).HasConversion(penalizationTypeConverter).IsRequired().HasDefaultValue(PenalizationType.None);
			});
		}
	}
}
