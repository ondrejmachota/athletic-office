﻿using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.Domain.Enums;
using AtletickaKancelar.EntityFramework.Services.IServices;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace AtletickaKancelar.EntityFramework.Services
{
	public class DisciplineService : IDisciplineService
	{
		private readonly DbContextFactory _dbContextFactory;
		private readonly IRaceService _raceService;

		public DisciplineService(DbContextFactory dbContextFactory, IRaceService raceService)
		{
			_dbContextFactory = dbContextFactory;
			_raceService = raceService;
		}

		public async Task<Discipline> Create(Discipline discipline, bool assignMainId = false)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				EntityEntry<Discipline> createdDiscipline = await context.Disciplines.AddAsync(discipline);
				Debug.Write(createdDiscipline.Entity.Id);
				await context.SaveChangesAsync();
				if (assignMainId)
				{
					createdDiscipline.Entity.MainId = createdDiscipline.Entity.Id;
					context.Disciplines.Update(createdDiscipline.Entity);
					await context.SaveChangesAsync();
				}

				return createdDiscipline.Entity;
			}
		}

		public async Task<Discipline> CreateChildDiscipline(Discipline discipline, int cameraId, int order)
		{
			var childDiscipline = new Discipline(discipline.RaceId, discipline.Name, discipline.Category, discipline.DisciplineType,
				discipline.PhaseType, discipline.MainId, discipline.Start, cameraId, order);
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				await context.Disciplines.AddAsync(childDiscipline);
				await context.SaveChangesAsync();
			}

			return childDiscipline;
		}

		public async Task Remove(Discipline discipline)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				context.Disciplines.Remove(discipline);
				await context.SaveChangesAsync();
			}
		}

		public async Task<List<Discipline>> GetAll()
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				var activeRace = await _raceService.GetActiveRace();
				return await context.Disciplines.Where(x => x.RaceId == activeRace.Id)
				                    .Select(x => new Discipline(x, x.Spojky.Count)).ToListAsync();
			}
		}

		public async Task<List<int?>> GetChildDisciplinesOrders(int? mainId, int raceId)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				return await context.Disciplines.Where(d => d.RaceId == raceId && d.MainId == mainId)
				                    .Select(x => x.Order).ToListAsync();
			}
		}

		public async Task<int> ChildDisciplinesCount(int? mainId)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				return await context.Disciplines.Where(d => d.MainId == mainId).CountAsync();
			}
		}

		public async Task<Discipline?> GetSimilar(string name, string category, PhaseType phaseType, TimeSpan start)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				var activeRace = await _raceService.GetActiveRace();
				return await context.Disciplines.Where(d =>
					                    d.RaceId == activeRace.Id && d.Name == name && d.Category == category &&
					                    d.PhaseType == phaseType && d.Start == start && d.MainId != null)
				                    .FirstOrDefaultAsync();
			}
		}

		public async Task Update(Discipline discipline)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				context.Disciplines.Update(discipline);
				await context.SaveChangesAsync();
			}
		}

		public async Task UpdateList(List<Discipline> disciplines)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				for (var i = 0; i < disciplines.Count; i++)
				{
					context.Disciplines.Update(disciplines[i]);
				}

				await context.SaveChangesAsync();
			}
		}

		public async Task<List<int>> GetDisciplineIds(int? mainId)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				var ids = await context.Disciplines.Where(x => x.MainId == mainId).Select(x => x.Id)
				                       .ToListAsync();
				return ids;
			}
		}
	}
}
