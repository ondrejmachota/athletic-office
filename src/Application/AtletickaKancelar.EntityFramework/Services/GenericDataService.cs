﻿using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.EntityFramework.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AtletickaKancelar.EntityFramework.Services
{
    public class GenericDataService<T> : IGenericDataService<T> where T : DomainObject
    {
        private readonly DbContextFactory _dbContextFactory;

        public GenericDataService(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<T> Create(T entity)
        {
            await using (var context = _dbContextFactory.CreateDbContext())
            {
                var createdEntity = await context.Set<T>().AddAsync(entity);
                await context.SaveChangesAsync();

                return createdEntity.Entity;
            }
        }

        public async Task<bool> Delete(int id)
        {
            await using (var context = _dbContextFactory.CreateDbContext())
            {
                var entity = await context.Set<T>().FirstOrDefaultAsync(e => e.Id == id);
                if (entity == null) return false;
                context.Set<T>().Remove(entity);
                await context.SaveChangesAsync();

                return true;
            }
        }

        public async Task<T?> Get(int id)
        {
            await using (var context = _dbContextFactory.CreateDbContext())
            {
                return await context.Set<T>().FirstOrDefaultAsync(e => e.Id == id);
            }
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            await using (var context = _dbContextFactory.CreateDbContext())
            {
                return await context.Set<T>().ToListAsync();
            }
        }

        public async Task<T> Update(int id, T entity)
        {
            await using (var context = _dbContextFactory.CreateDbContext())
            {
                entity.Id = id;
                context.Set<T>().Update(entity);
                await context.SaveChangesAsync();

                return entity;
            }
        }
    }
}
