﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AtletickaKancelar.EntityFramework.Services.IServices
{
    public interface IClubService
    {
        Task<bool> IsUnique(string name, string abbreviation);
        Task<List<int>> GetActiveClubIdByDisciplineId(int disciplineId);
        Task<List<int>> GetActiveClubIdByMainId(int? mainId);
    }
}
