﻿using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AtletickaKancelar.EntityFramework.Services.IServices
{
    public interface IDisciplineService
    {
        Task<Discipline> Create(Discipline discipline, bool assignMainId = false);
        Task<Discipline> CreateChildDiscipline(Discipline discipline, int cameraId, int order);
        Task Update(Discipline discipline);
        Task UpdateList(List<Discipline> disciplines);
        Task Remove(Discipline discipline);
        Task<List<int?>> GetChildDisciplinesOrders(int? mainId, int raceId);
        Task<Discipline?> GetSimilar(string name, string category, PhaseType phaseType, TimeSpan start);
        Task<List<Discipline>> GetAll();
        Task<int> ChildDisciplinesCount(int? mainId);
        Task<List<int>> GetDisciplineIds(int? mainId);
    }
}
