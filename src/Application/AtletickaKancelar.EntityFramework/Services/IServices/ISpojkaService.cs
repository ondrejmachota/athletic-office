﻿using AtletickaKancelar.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AtletickaKancelar.EntityFramework.Services.IServices
{
    public interface ISpojkaService
    {
        Task<int> GetSpojkyCount(int disciplineId);
        Task<IEnumerable<Spojka>> GetSpojkyByDisciplineId(int disciplineId);
        Task<IEnumerable<Spojka>> GetSpojkyByMainId(int maindId);
        Task<IEnumerable<Spojka>> GetSpojkyByMainIdWithouDisciplineId(int maindId, int disciplineId);
        Task<List<Spojka>> GetAll();
        Task Update(List<Spojka> spojky);
        Task UpdateOne(Spojka spojka);
        Task Add(List<Spojka> spojky);
        Task Remove(Spojka spojka);
        IAsyncEnumerable<ClubPoints> GetPoints(int raceId);
    }
}
