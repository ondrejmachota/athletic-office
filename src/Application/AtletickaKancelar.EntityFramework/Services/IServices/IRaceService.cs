﻿using System.Collections.Generic;
using AtletickaKancelar.Domain.Entities;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace AtletickaKancelar.EntityFramework.Services.IServices
{
    public interface IRaceService
    {
        Task Update(Race race);
        Task<List<Race>> UpdateIsActive(int id);
        Task Delete(Race race);
        Task<ObservableCollection<Race>> Find(string search);

        Task<Race> GetActiveRace();
    }
}
