﻿﻿using AtletickaKancelar.EntityFramework.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AtletickaKancelar.EntityFramework.Services
{
    public class ClubService : IClubService
    {
        private readonly DbContextFactory _dbContextFactory;

        public ClubService(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<bool> IsUnique(string name, string abbreviation)
        {
            int count;
            await using (ApplicationDbContext context = _dbContextFactory.CreateDbContext())
            {
                count = await context.Clubs.Where(x => x.Name == name || x.Abbreviation == abbreviation || (x.Name == name && x.Abbreviation == abbreviation)).CountAsync();
            }
            return (count == 0);
        }

        public async Task<List<int>> GetActiveClubIdByDisciplineId(int disciplineId)
        {
            await using (ApplicationDbContext context = _dbContextFactory.CreateDbContext())
            {
                List<int> ids = new List<int>();
                var querry = await (from spojka in context.Spojky
                                    join competitor in context.Competitors on spojka.CompetitorId equals competitor.Id
                                    join club in context.Clubs on competitor.ClubId equals club.Id
                                    where spojka.DisciplineId == disciplineId
                                    select new
                                    {
                                        ClubId = club.Id
                                    }).ToListAsync();
                foreach (var id in querry)
                {
                    ids.Add(id.ClubId);
                }
                return ids.Distinct().ToList();
            }
        }

        public async Task<List<int>> GetActiveClubIdByMainId(int? mainId)
        {
            await using (ApplicationDbContext context = _dbContextFactory.CreateDbContext())
            {
                List<int> ids = new List<int>();
                var querry = await (from spojka in context.Spojky
                                    join competitor in context.Competitors on spojka.CompetitorId equals competitor.Id
                                    join club in context.Clubs on competitor.ClubId equals club.Id
                                    where spojka.MainId == mainId
                                    select new
                                    {
                                        ClubId = club.Id
                                    }).ToListAsync();
                foreach (var id in querry)
                {
                    ids.Add(id.ClubId);
                }
                return ids.Distinct().ToList();
            }
        }
    }
}
