﻿using System;
using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.EntityFramework.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace AtletickaKancelar.EntityFramework.Services
{
    public class RaceService : IRaceService
    {
        private readonly DbContextFactory _dbContextFactory;

        public RaceService(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task Update(Race race)
        {
            await using (var context = _dbContextFactory.CreateDbContext())
            {
                context.Races.Update(race);
                await context.SaveChangesAsync();
            }
        }

        public async Task<List<Race>> UpdateIsActive(int id)
        {
            await using (var context = _dbContextFactory.CreateDbContext())
            {
                await context.Races.ForEachAsync(x =>
                {
                    if (x.Id == id)
                        x.IsActive = true;
                    else
                        x.IsActive = false;
                });
                await context.SaveChangesAsync();

                return await context.Races.ToListAsync();
            }
        }

        public async Task Delete(Race race) // TODO : delete
        {
            await using (var context = _dbContextFactory.CreateDbContext())
            {
                context.Races.Remove(race);
                await context.SaveChangesAsync();
            }
        }

        // not working
        public async Task<ObservableCollection<Race>> Find(string search)
        {
            List<Race> result;

            await using (var context = _dbContextFactory.CreateDbContext())
            {
                result = await context.Races.Where(s => s.Title == search).ToListAsync();
            }

            return new ObservableCollection<Race>(result);
        }

        public async Task<Race> GetActiveRace()
        {
            await using (var context = _dbContextFactory.CreateDbContext())
            {
                
                var race = await context.Races.FirstOrDefaultAsync(s => s.IsActive);
                if (race == null) throw new NullReferenceException("No active race selected!");
                return race;
            }
        }
    }
}
