﻿using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.EntityFramework.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AtletickaKancelar.EntityFramework.Services
{
	public class SpojkaService : ISpojkaService
	{
		private readonly DbContextFactory _dbContextFactory;

		public SpojkaService(DbContextFactory dbContextFactory)
		{
			_dbContextFactory = dbContextFactory;
		}

		public async Task<int> GetSpojkyCount(int disciplineId)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				return await context.Spojky.Where(d => d.DisciplineId == disciplineId).CountAsync();
			}
		}

		public async Task<IEnumerable<Spojka>> GetSpojkyByDisciplineId(int disciplineId)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				var query = await (from spojka in context.Spojky
				                   where spojka.DisciplineId == disciplineId
				                   join competitor in context.Competitors on spojka.CompetitorId equals competitor.Id
				                   select new
				                   {
					                   Spojka = spojka,
					                   competitor.ClubId,
					                   FullName = $"{competitor.FirstName} {competitor.LastName}",
					                   competitor.ClubAbbreviation,
					                   BirthDate = competitor.BirthDate.ToShortDateString(),
				                   }).ToListAsync();
				List<Spojka> spojky = new List<Spojka>();
				foreach (var obj in query)
				{
					obj.Spojka.ClubId = obj.ClubId;
					obj.Spojka.FullName = obj.FullName;
					obj.Spojka.ClubAbbreviation = obj.ClubAbbreviation;
					obj.Spojka.BirthDate = obj.BirthDate;
					spojky.Add(obj.Spojka);
				}

				return spojky;
			}
		}

		public async Task<IEnumerable<Spojka>> GetSpojkyByMainId(int maindId)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				var query = await (from spojka in context.Spojky
				                   join competitor in context.Competitors on spojka.CompetitorId equals competitor.Id
				                   join discipline in context.Disciplines on spojka.DisciplineId equals discipline.Id
				                   where discipline.MainId == maindId
				                   select new
				                   {
					                   Spojka = spojka,
					                   competitor.ClubId,
					                   FullName = $"{competitor.FirstName} {competitor.LastName}",
					                   competitor.ClubAbbreviation,
					                   BirthDate = competitor.BirthDate.ToShortDateString(),
				                   }).ToListAsync();
				List<Spojka> spojky = new List<Spojka>();
				foreach (var obj in query)
				{
					obj.Spojka.ClubId = obj.ClubId;
					obj.Spojka.FullName = obj.FullName;
					obj.Spojka.ClubAbbreviation = obj.ClubAbbreviation;
					obj.Spojka.BirthDate = obj.BirthDate;
					spojky.Add(obj.Spojka);
				}

				return spojky;
			}
		}

		public async Task<IEnumerable<Spojka>> GetSpojkyByMainIdWithouDisciplineId(int maindId, int disciplineId)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				var query = await (from spojka in context.Spojky
				                   where spojka.DisciplineId != disciplineId
				                   join competitor in context.Competitors on spojka.CompetitorId equals competitor.Id
				                   join discipline in context.Disciplines on spojka.DisciplineId equals discipline.Id
				                   where discipline.MainId == maindId
				                   select new
				                   {
					                   Spojka = spojka,
					                   competitor.ClubId,
					                   FullName = $"{competitor.FirstName} {competitor.LastName}",
					                   competitor.ClubAbbreviation,
					                   BirthDate = competitor.BirthDate.ToShortDateString(),
				                   }).ToListAsync();
				List<Spojka> spojky = new List<Spojka>();
				foreach (var obj in query)
				{
					obj.Spojka.ClubId = obj.ClubId;
					obj.Spojka.FullName = obj.FullName;
					obj.Spojka.ClubAbbreviation = obj.ClubAbbreviation;
					obj.Spojka.BirthDate = obj.BirthDate;
					spojky.Add(obj.Spojka);
				}

				return spojky;
			}
		}

		public async Task<List<Spojka>> GetAll()
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				var query = await (from spojka in context.Spojky
				                   join competitor in context.Competitors on spojka.CompetitorId equals competitor.Id
				                   select new
				                   {
					                   Spojka = spojka,
					                   competitor.ClubId,
					                   FullName = $"{competitor.FirstName} {competitor.LastName}",
					                   competitor.ClubAbbreviation,
					                   BirthDate = competitor.BirthDate.ToShortDateString(),
				                   }).ToListAsync();
				List<Spojka> spojky = new List<Spojka>();
				foreach (var obj in query)
				{
					obj.Spojka.ClubId = obj.ClubId;
					obj.Spojka.FullName = obj.FullName;
					obj.Spojka.ClubAbbreviation = obj.ClubAbbreviation;
					obj.Spojka.BirthDate = obj.BirthDate;
					spojky.Add(obj.Spojka);
				}

				return spojky;
			}
		}

		public async Task Update(List<Spojka> spojky)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				for (int i = 0; i < spojky.Count; i++)
				{
					context.Spojky.Update(spojky[i]);
				}

				await context.SaveChangesAsync();
			}
		}

		public async Task UpdateOne(Spojka spojka)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				context.Spojky.Update(spojka);
				await context.SaveChangesAsync();
			}
		}

		public async Task Add(List<Spojka> spojky)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				for (var i = 0; i < spojky.Count; i++)
				{
					await context.Spojky.AddAsync(spojky[i]);
				}

				await context.SaveChangesAsync();
			}
		}

		public async Task Remove(Spojka spojka)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				context.Spojky.Remove(spojka);
				await context.SaveChangesAsync();
			}
		}

		public async IAsyncEnumerable<ClubPoints> GetPoints(int raceId)
		{
			await using (var context = _dbContextFactory.CreateDbContext())
			{
				var query = await (from spojka in context.Spojky
				                   join competitor in context.Competitors on spojka.CompetitorId equals competitor.Id
				                   join club in context.Clubs on competitor.ClubId equals club.Id
				                   join discipline in context.Disciplines on spojka.DisciplineId equals discipline.Id
				                   where spojka.Points != null && discipline.RaceId == raceId
				                   select new ClubPoints(club.Id, club.Name, 0, spojka.Points)).ToListAsync();
				foreach (var item in query)
				{
					yield return item;
				}
			}
		}
	}
}
