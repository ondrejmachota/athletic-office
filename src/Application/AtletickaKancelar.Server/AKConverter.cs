﻿using AtletickaKancelar.Domain.Entities;
using Google.Protobuf.WellKnownTypes;
using AtletickaKancelar.Domain.Enums;

namespace AtletickaKancelar.Server
{
	public static class AkConverter
	{
		public static Race ToRace(this RaceDto raceDto)
		{
			var race = new Race
			{
				Id = raceDto.Id,
				Title = raceDto.Title,
				IsActive = raceDto.IsActive,
				Place = raceDto.Place,
				Date = raceDto.Date.ToDateTime(),
				Note = raceDto.Note
			};
			return race;
		}

		public static RaceDto ToRaceDto(this Race race)
		{
			return new RaceDto
			{
				Id = race.Id,
				Title = race.Title,
				IsActive = race.IsActive,
				Place = race.Place,
				Date = Timestamp.FromDateTime(race.Date.AddHours(2).ToUniversalTime()),
				Note = race.Note
			};
		}

		public static Discipline ToDiscipline(this DisciplineDto disciplineDto)
		{
			return new Discipline
			{
				Id = disciplineDto.Id,
				RaceId = disciplineDto.RaceId,
				Name = disciplineDto.Name,
				Category = disciplineDto.Category,
				DisciplineType = (DisciplineType) disciplineDto.DisciplineType,
				PhaseType = (PhaseType) disciplineDto.PhaseType,
				MainId = disciplineDto.MainId,
				Start = disciplineDto.Start.ToTimeSpan(),
				CameraId = disciplineDto.CameraId,
				Order = disciplineDto.Order,
				IsRegistrationsPrinted = disciplineDto.IsRegistrationsPrinted,
				IsResultsPrinted = disciplineDto.IsResultsPrinted,
				CompetitorsCount = disciplineDto.CompetitorsCount // Don't send
			};
		}

		public static DisciplineDto ToDisciplineDto(this Discipline discipline)
		{
			return new DisciplineDto
			{
				Id = discipline.Id,
				RaceId = discipline.RaceId,
				Name = discipline.Name,
				Category = discipline.Category,
				DisciplineType = (int) discipline.DisciplineType,
				PhaseType = (int) discipline.PhaseType,
				MainId = discipline.MainId,
				Start = Duration.FromTimeSpan(discipline.Start),
				CameraId = discipline.CameraId,
				Order = discipline.Order,
				IsRegistrationsPrinted = discipline.IsRegistrationsPrinted,
				IsResultsPrinted = discipline.IsResultsPrinted,
				CompetitorsCount = discipline.CompetitorsCount
			};
		}

		public static Club ToClub(this ClubDto clubDto)
		{
			return new Club
			{
				Id = clubDto.Id,
				Name = clubDto.Name,
				Abbreviation = clubDto.Abbreviation,
			};
		}

		public static ClubDto ToClubDto(this Club club)
		{
			return new ClubDto
			{
				Id = club.Id,
				Name = club.Name,
				Abbreviation = club.Abbreviation,
			};
		}

		public static Competitor ToCompetitor(this CompetitorDto competitorDto)
		{
			return new Competitor
			{
				Id = competitorDto.Id,
				ClubId = competitorDto.ClubId,
				FirstName = competitorDto.FirstName,
				LastName = competitorDto.LastName,
				Sex = (Sex) competitorDto.Sex,
				BirthDate = competitorDto.BirthDate.ToDateTime(),
				ClubName = competitorDto.ClubName,
				ClubAbbreviation = competitorDto.ClubAbbreviation,
			};
		}

		public static CompetitorDto ToCompetitorDto(this Competitor competitor)
		{
			return new CompetitorDto
			{
				Id = competitor.Id,
				ClubId = competitor.ClubId,
				FirstName = competitor.FirstName,
				LastName = competitor.LastName,
				Sex = (int) competitor.Sex,
				BirthDate = Timestamp.FromDateTime(competitor.BirthDate.AddHours(2).ToUniversalTime()),
				ClubName = competitor.ClubName,
				ClubAbbreviation = competitor.ClubAbbreviation,
			};
		}

		public static Spojka ToSpojka(this SpojkaDto spojkaDto)
		{
			return new Spojka
			{
				Id = spojkaDto.Id,
				CompetitorId = spojkaDto.CompetitorId,
				DisciplineId = spojkaDto.DisciplineId,
				Track = spojkaDto.Track,
				BIB = spojkaDto.BIB,
				Result = spojkaDto.Result,
				Order = spojkaDto.Order,
				SecondOrder = spojkaDto.SecondOrder,
				Points = spojkaDto.Points,
				PenalizationType = (PenalizationType)spojkaDto.PenalizationType,
				MainId = spojkaDto.MainId,
				ClubId = spojkaDto.ClubId,
				FullName = spojkaDto.FullName,
				ClubAbbreviation = spojkaDto.ClubAbbreviation,
				BirthDate = spojkaDto.BirthDate,
			};
		}
		
		public static SpojkaDto ToSpojkaDto(this Spojka spojka)
		{
			return new SpojkaDto
			{
				Id = spojka.Id,
				CompetitorId = spojka.CompetitorId,
				DisciplineId = spojka.DisciplineId,
				Track = spojka.Track,
				BIB = spojka.BIB,
				Result = spojka.Result,
				Order = spojka.Order,
				SecondOrder = spojka.SecondOrder,
				Points = spojka.Points,
				PenalizationType = (int)spojka.PenalizationType,
				MainId = spojka.MainId,
				ClubId = spojka.ClubId,
				FullName = spojka.FullName,
				ClubAbbreviation = spojka.ClubAbbreviation,
				BirthDate = spojka.BirthDate,
			};
		}

		public static GetPointsResponse ToGetPointsResponse(this ClubPoints clubPoints)
		{
			return new GetPointsResponse
			{
				ClubId = clubPoints.ClubId,
				ClubName = clubPoints.ClubName,
				Points = clubPoints.Points
			};
		}
	}
}
