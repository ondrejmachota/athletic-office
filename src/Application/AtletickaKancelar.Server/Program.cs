using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.Domain.Settings;
using AtletickaKancelar.EntityFramework;
using AtletickaKancelar.EntityFramework.Services;
using AtletickaKancelar.EntityFramework.Services.IServices;
using AtletickaKancelar.Server.Services;
using Microsoft.AspNetCore.Server.Kestrel.Core;

var builder = WebApplication.CreateBuilder(args);

// Additional configuration is required to successfully run gRPC on macOS.
// For instructions on how to configure Kestrel and gRPC clients on macOS, visit https://go.microsoft.com/fwlink/?linkid=2099682

// Add services to the container.
builder.Services.AddGrpc();

builder.Services.AddSingleton<ISettingsStore, SettingsStore>();
builder.Services.AddSingleton(_ => new DbContextFactory());
builder.Services.AddSingleton<IGenericDataService<Race>, GenericDataService<Race>>();
builder.Services.AddSingleton<IRaceService, RaceService>();
builder.Services.AddSingleton<IGenericDataService<Discipline>, GenericDataService<Discipline>>();
builder.Services.AddSingleton<IDisciplineService, DisciplineService>();
builder.Services.AddSingleton<IGenericDataService<Club>, GenericDataService<Club>>();
builder.Services.AddSingleton<IClubService, ClubService>();
builder.Services.AddSingleton<IGenericDataService<Competitor>, GenericDataService<Competitor>>();
builder.Services.AddSingleton<ISpojkaService, SpojkaService>();

builder.WebHost.UseUrls("http://0.0.0.0:5000", "https://0.0.0.0:5001");
builder.WebHost.ConfigureKestrel(options =>
{
	options.ConfigureEndpointDefaults(listenOptions =>
	{
		listenOptions.Protocols = HttpProtocols.Http2;
		listenOptions.UseHttps();
	});
});

var app = builder.Build();

// Configure the HTTP request pipeline.
app.MapGrpcService<RaceGrpcService>();
app.MapGrpcService<DisciplineGrpcService>();
app.MapGrpcService<ClubGrpcService>();
app.MapGrpcService<CompetitorGrpcService>();
app.MapGrpcService<SpojkaGrpcService>();
app.MapGet("/",
	() =>
		"Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

app.Run();
