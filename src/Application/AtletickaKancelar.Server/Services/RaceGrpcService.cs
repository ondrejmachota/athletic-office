﻿using AtletickaKancelar.Domain.Entities;
using AtletickaKancelar.EntityFramework.Services.IServices;
using Grpc.Core;

namespace AtletickaKancelar.Server.Services
{
	public class RaceGrpcService : RaceGrpcProtoService.RaceGrpcProtoServiceBase
	{
		private readonly IGenericDataService<Race> _genericDataService;
		private readonly IRaceService _raceService;

		public RaceGrpcService(IGenericDataService<Race> genericDataService,
		                       IRaceService raceService)
		{
			_genericDataService = genericDataService;
			_raceService = raceService;
		}

		public override async Task GetRaceStream(EmptyRequest request, IServerStreamWriter<RaceDto> responseStream,
		                                         ServerCallContext context)
		{
			var races = new List<Race>(await _genericDataService.GetAll());

			foreach (var race in races)
			{
				await responseStream.WriteAsync(race.ToRaceDto());
			}
		}

		public override async Task<RaceDto> CreateRace(RaceDto request, ServerCallContext context)
		{
			var race = await _genericDataService.Create(request.ToRace());
			return await Task.FromResult(race.ToRaceDto());
		}

		public override async Task UpdateRaceStream(UpdateRaceActivityModel request,
		                                            IServerStreamWriter<RaceDto> responseStream,
		                                            ServerCallContext context)
		{
			var races = await _raceService.UpdateIsActive(request.Id);

			for (var i = 0; i < races.Count; i++)
			{
				await responseStream.WriteAsync((races[i].ToRaceDto()));
			}
		}

		public override async Task<EmptyRequest> EditRace(RaceDto request, ServerCallContext context)
		{
			var race = request.ToRace();
			await _raceService.Update(race);
			return await Task.FromResult(new EmptyRequest());
		}

		public override async Task<EmptyRequest> DeleteRace(RaceDto request, ServerCallContext context)
		{
			await _genericDataService.Delete(request.ToRace().Id);
			return await Task.FromResult(new EmptyRequest());
		}

		public override async Task<RaceDto> GetActiveRace(EmptyRequest request, ServerCallContext context)
		{
			var race = await _raceService.GetActiveRace();
			return race.ToRaceDto();
		}
	}
}
